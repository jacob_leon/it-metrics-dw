USE [master]
GO
/****** Object:  Database [IT_Metrics]    Script Date: 3/13/2015 4:53:21 PM ******/
CREATE DATABASE [IT_Metrics]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'IT_Metrics', FILENAME = N'F:\MSSQL\Data\IT_Metrics.mdf' , SIZE = 8656896KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'IT_Metrics_log', FILENAME = N'E:\MSSQL\Logs\IT_Metrics_log.ldf' , SIZE = 10028800KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [IT_Metrics] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [IT_Metrics].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [IT_Metrics] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [IT_Metrics] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [IT_Metrics] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [IT_Metrics] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [IT_Metrics] SET ARITHABORT OFF 
GO
ALTER DATABASE [IT_Metrics] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [IT_Metrics] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [IT_Metrics] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [IT_Metrics] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [IT_Metrics] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [IT_Metrics] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [IT_Metrics] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [IT_Metrics] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [IT_Metrics] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [IT_Metrics] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [IT_Metrics] SET  DISABLE_BROKER 
GO
ALTER DATABASE [IT_Metrics] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [IT_Metrics] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [IT_Metrics] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [IT_Metrics] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [IT_Metrics] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [IT_Metrics] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [IT_Metrics] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [IT_Metrics] SET RECOVERY FULL 
GO
ALTER DATABASE [IT_Metrics] SET  MULTI_USER 
GO
ALTER DATABASE [IT_Metrics] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [IT_Metrics] SET DB_CHAINING OFF 
GO
ALTER DATABASE [IT_Metrics] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [IT_Metrics] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [IT_Metrics]
GO
/****** Object:  User [test]    Script Date: 3/13/2015 4:53:21 PM ******/
CREATE USER [test] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [IT_Metrics_Admin]    Script Date: 3/13/2015 4:53:21 PM ******/
CREATE USER [IT_Metrics_Admin] FOR LOGIN [IT_Metrics_Admin] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [BCGCOM\VND Leon Jacob]    Script Date: 3/13/2015 4:53:21 PM ******/
CREATE USER [BCGCOM\VND Leon Jacob] FOR LOGIN [BCGCOM\VND Leon Jacob] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [BCGCOM\Vivian Victoria]    Script Date: 3/13/2015 4:53:22 PM ******/
CREATE USER [BCGCOM\Vivian Victoria] FOR LOGIN [BCGCOM\Vivian Victoria] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [BCGCOM\srv_Tableau_Server]    Script Date: 3/13/2015 4:53:22 PM ******/
CREATE USER [BCGCOM\srv_Tableau_Server] FOR LOGIN [BCGCOM\srv_Tableau_Server] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [BCGCOM\IT Metrics DB Admin]    Script Date: 3/13/2015 4:53:22 PM ******/
CREATE USER [BCGCOM\IT Metrics DB Admin] FOR LOGIN [BCGCOM\IT Metrics DB Admin]
GO
/****** Object:  User [BCGCOM\Hecksher Derek]    Script Date: 3/13/2015 4:53:22 PM ******/
CREATE USER [BCGCOM\Hecksher Derek] FOR LOGIN [BCGCOM\Hecksher Derek] WITH DEFAULT_SCHEMA=[BDW]
GO
/****** Object:  User [BCGCOM\Carbon Santiago]    Script Date: 3/13/2015 4:53:22 PM ******/
CREATE USER [BCGCOM\Carbon Santiago] FOR LOGIN [BCGCOM\Carbon Santiago] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  DatabaseRole [BCG_db_owner]    Script Date: 3/13/2015 4:53:22 PM ******/
CREATE ROLE [BCG_db_owner]
GO
ALTER ROLE [BCG_db_owner] ADD MEMBER [IT_Metrics_Admin]
GO
ALTER ROLE [db_owner] ADD MEMBER [BCGCOM\VND Leon Jacob]
GO
ALTER ROLE [BCG_db_owner] ADD MEMBER [BCGCOM\Vivian Victoria]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BCGCOM\srv_Tableau_Server]
GO
ALTER ROLE [BCG_db_owner] ADD MEMBER [BCGCOM\IT Metrics DB Admin]
GO
ALTER ROLE [db_datareader] ADD MEMBER [BCGCOM\Hecksher Derek]
GO
ALTER ROLE [BCG_db_owner] ADD MEMBER [BCGCOM\Carbon Santiago]
GO
/****** Object:  Schema [BDW]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SCHEMA [BDW]
GO
/****** Object:  Schema [STG]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SCHEMA [STG]
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[APPLICATION_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[APPLICATION_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[AUDIT_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[AUDIT_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[COMP_HARDWARE_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[COMP_HARDWARE_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[MOBILE_DEVICE_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[MOBILE_DEVICE_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[MOBILE_DEVICE_JNK_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[MOBILE_DEVICE_JNK_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[OFFICE_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[OFFICE_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[PARTICIPANT_IDN_S]    Script Date: 3/13/2015 4:53:23 PM ******/
CREATE SEQUENCE [BDW].[PARTICIPANT_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[PUBLISHER_IDN_S]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE SEQUENCE [BDW].[PUBLISHER_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[RUN_AUDIT_IDN_S]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE SEQUENCE [BDW].[RUN_AUDIT_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[SERVICE_USAGE_JNK_IDN_S]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE SEQUENCE [BDW].[SERVICE_USAGE_JNK_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
USE [IT_Metrics]
GO
/****** Object:  Sequence [BDW].[SERVICE_VENDOR_IDN_S]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE SEQUENCE [BDW].[SERVICE_VENDOR_IDN_S] 
 AS [bigint]
 START WITH 1
 INCREMENT BY 1
 MINVALUE -9223372036854775808
 MAXVALUE 9223372036854775807
 CACHE 
GO
/****** Object:  StoredProcedure [BDW].[APP_METERING_FAC_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[APP_METERING_FAC_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: APP_METERING_FAC
//		Date: Feb 25, 2015
//		Description: This proc creates the fact details around application usage (metering)
//
*/

  INSERT INTO BDW.APP_METERING_FAC(MONTH_IDN,
									COMP_HARDWARE_IDN,
									PARTICIPANT_IDN,
									OFFICE_IDN,
									PUBLISHER_IDN,
									LAST_USAGE_DATE_IDN,
									TOTAL_USAGES,
									AVG_DAILY_USAGES,
									TOTAL_DURATION_MIN,
									AVG_DURATION_MIN,
									AVG_DAILY_DURATION_MIN,
									LAST_USAGE_DATETIME,
									AUDIT_CREATE_IDN,
									AUDIT_UPDATE_IDN)
					Select  MD.MONTH_IDN,
							CHD.COMP_HARDWARE_IDN,
							coalesce(PD.PARTICIPANT_IDN,0),
							BDW.GET_OFFICE_IDN(PD.OFFICE_COD) as OFFICE_IDN,
							PUB.PUBLISHER_IDN,
							LU.DATE_IDN as LAST_USAGE_DATE_IDN,							
							[Total Usages] AS TOTAL_USAGES,
							[Average Usages per Day] as AVG_DAILY_USAGES,
							[Total Duration (min)] as TOTAL_DURATION_MIN,
							[Average Duration of Use (min)] as AVG_DURATION_MIN,
							[Average Duration per Day (min)] as AVG_DAILY_DURATION_MIN,
							convert(datetime,[Last Usage],105) AS LAST_USAGE_DATETIME,
							@AUDIT_IDN,
							@AUDIT_IDN				
					  FROM
							 STG.App_Metering_STG AM
				   LEFT JOIN stg.tb_cache_daily_details_stg DD	
	 					  ON am.[NetBIOS Name] = DD.build_computer_name
				   LEFT JOIN BDW.COMP_HARDWARE_DIM CHD
						  ON am.[NetBIOS Name] = CHD.NAME
				  LEFT JOIN BDW.PARTICIPANT_DIM PD
						  ON DD.user_employee_id = PD.EMPLOYEE_ID
						 AND PD.REC_END_DATE = '12-31-9999'
				  INNER JOIN BDW.PUBLISHER_DIM PUB
						  ON AM.Application = PUB.PUBLISHER_NAM
				  INNER JOIN BDW.DATE_DIM LU
						  ON convert(date,[Last Usage],105) = LU.DATE
				  INNER JOIN BDW.MONTH_DIM MD
						  ON AM.[MONTH] = MD.MONTH_YEAR
				;

	update statistics BDW.APP_METERING_FAC WITH SAMPLE 50 PERCENT, ALL;




GO
/****** Object:  StoredProcedure [BDW].[APPLICATION_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[APPLICATION_DIM_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: APPLICATION_DIM_M
//		Date: Feb 16, 2015
//		Description: This is the mapping that loads the application dimension.  The source data from the 
//					 applicaiton staging table is inserted/updated into the application dimension. The procedure
//					 needs an integer passed to it for auditing purposes
//					 
//
*/


			/*
			Grab all the data from the office staging table.  If the office code is already in the 
			table, then update the rest of the rows.  If it is not in there, insert the row.
			*/ 
			   MERGE 
				INTO BDW.APPLICATION_DIM AD
			   USING (SELECT APPLICATION_NAM,
							  PD.PUBLISHER_NAM,
							  PD.PUBLISHER_IDN,
							  VERSION_COD
						 FROM [STG].[APPLICATION_STG] AP
				   INNER JOIN BDW.PUBLISHER_DIM PD
						   ON AP.PUBLISHER_NAM = PD.PUBLISHER_NAM) AST
				ON
					AD.APPLICATION_NAM = AST.APPLICATION_NAM AND
--					AD.PUBLISHER_NAM = AST.PUBLISHER_NAM AND
					AD.VERSION_COD = AST.VERSION_COD
/*
			WHEN MATCHED THEN
				UPDATE SET AD.PUBLISHER_NAM = AST.PUBLISHER_NAM,
						   AD.AUDIT_UPDATE_IDN = @AUDIT_IDN
*/	
			WHEN NOT MATCHED THEN
					INSERT( APPLICATION_NAM,
							PUBLISHER_IDN,
							VERSION_COD, 
							AUDIT_UPDATE_IDN, 
							AUDIT_CREATE_IDN)		
					VALUES(AST.APPLICATION_NAM, 
							AST.PUBLISHER_IDN, 
							VERSION_COD,
							@AUDIT_IDN,
							@AUDIT_IDN)
;


  update statistics BDW.APPLICATION_DIM WITH SAMPLE 50 PERCENT, ALL;





GO
/****** Object:  StoredProcedure [BDW].[COMP_HARDWARE_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[COMP_HARDWARE_DIM_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: COMP_HARDWARE_DIM_M
//		Date: Jan 21, 2015
//		Description: This is the mapping that loads the Computer dimension.  The source data from the 
//					 Computer staging table is inserted/updated into the Computer dimension. The procedure
//					 needs an integer passed to it for auditing purposes
//					 
//
*/

			/*
			Grab all the data from the computer staging table.  If the office code is already in the 
			table, then update the rest of the rows.  If it is not in there, insert the row.
			*/ 
			MERGE 
				INTO BDW.COMP_HARDWARE_DIM CD
				USING (
						SELECT [IMAGE_MODEL_DESC]
							  ,[HD_MODEL_DESC]
							  ,[COMP_MODEL_DESC]
							  ,[WINDOWS_ARCH_DESC]
							  ,[PROCESSOR_MODEL_DESC]
							  ,[VIDEO_MODEL_DESC]
							  ,[WIRED_NIC_MODEL_DESC]
							  ,[WIRELESS_NIC_MODEL_DESC]
							  ,NAME
							  ,[STATUS]
						  FROM [STG].[COMP_HARDWARE_STG]
  												  ) CS
				ON
					CD.NAME = CS.NAME
			WHEN MATCHED THEN 
					UPDATE SET cd.[IMAGE_MODEL_DESC] = CS.[IMAGE_MODEL_DESC]
								,cd.[HD_MODEL_DESC] = CS.[HD_MODEL_DESC]
								,cd.[COMP_MODEL_DESC] = CS.[COMP_MODEL_DESC]
								,cd.[WINDOWS_ARCH_DESC] = cs.[WINDOWS_ARCH_DESC]
								,cd.[PROCESSOR_MODEL_DESC] = cs.[PROCESSOR_MODEL_DESC]
								,cd.[VIDEO_MODEL_DESC] = cs.[VIDEO_MODEL_DESC]
								,cd.[WIRED_NIC_MODEL_DESC]= cs.[WIRED_NIC_MODEL_DESC]
								,cd.[WIRELESS_NIC_MODEL_DESC] = cs.[WIRELESS_NIC_MODEL_DESC]
								,cd.AUDIT_UPDATE_IDN = @AUDIT_IDN
								,cd.[STATUS] = cs.[STATUS]									
			WHEN NOT MATCHED THEN
					INSERT( [IMAGE_MODEL_DESC]
							,[HD_MODEL_DESC]
							,[COMP_MODEL_DESC]
							,[WINDOWS_ARCH_DESC]
							,[PROCESSOR_MODEL_DESC]
							,[VIDEO_MODEL_DESC]
							,[WIRED_NIC_MODEL_DESC]
							,[WIRELESS_NIC_MODEL_DESC]
							,AUDIT_CREATE_IDN
							,AUDIT_UPDATE_IDN
							,NAME
							,[STATUS]
							)		
					VALUES( CS.[IMAGE_MODEL_DESC]
							,CS.[HD_MODEL_DESC]
							,CS.[COMP_MODEL_DESC]
							,CS.[WINDOWS_ARCH_DESC]
							,CS.[PROCESSOR_MODEL_DESC]
							,CS.[VIDEO_MODEL_DESC]
							,CS.[WIRED_NIC_MODEL_DESC]
							,CS.[WIRELESS_NIC_MODEL_DESC]
							,@AUDIT_IDN
							,@AUDIT_IDN
							,CS.NAME
							,CS.[STATUS])
						;

  update statistics BDW.COMP_HARDWARE_DIM WITH SAMPLE 50 PERCENT, ALL;



GO
/****** Object:  StoredProcedure [BDW].[COMPUTER_APPLICATION_FAC_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[COMPUTER_APPLICATION_FAC_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: COMPUTER_APPLICATION_FAC_M
//		Date: Jan 21, 2015
//		Description: This is the fact table that is used to create the association between the Application, Computer
//					 and User.
//					 
*/

					INSERT INTO BDW.COMPUTER_APPLICATION_FAC(	APPLICATION_IDN,
																COMP_HARDWARE_IDN,
																INSTALL_DATE_IDN,
																USER_IDN,
																BUILD_DATE_IDN,
																PURCHASE_DATE_IDN,
																AUDIT_CREATE_IDN,
																AUDIT_UPDATE_IDN,
																OFFICE_IDN,
																SCCM_CAPTURED_DATE)
						SELECT  
								coalesce(AD.APPLICATION_IDN,0) as APPLICATION_IDN,
								cd.COMP_HARDWARE_IDN,
								coalesce(instl.DATE_IDN,0) as INSTALL_DATE_IDN,
								coalesce(PD.PARTICIPANT_IDN,0) as USER_IDN,
								coalesce(bld.DATE_IDN,0) as BUILD_DATE_IDN,
								coalesce(pur.DATE_IDN,0) as PURCHASE_DATE_IDN,
								@AUDIT_IDN,
								@AUDIT_IDN,
								BDW.GET_OFFICE_IDN(PD.OFFICE_COD) as OFFICE_IDN,
								coalesce(app.CapturedDate,'12-31-9999')
						  FROM STG.tb_CACHE_Daily_Details_STG h
						LEFT JOIN
								 (Select cast(coalesce(InstallDate,'99991231') as Datetime) InstallDate,
										 DisplayName,
										 Publisher,
										 ComputerName,
										 CAST(Version as VARCHAR(20)) as [Version],
										 CapturedDate
									from STG.BCG_SCCM_STG) app
								ON h.build_computer_name = app.computerName
					     LEFT JOIN (	SELECT  APPLICATION_IDN,
												APPLICATION_NAM,
												VERSION_COD,
												PUBLISHER_NAM
									      FROM BDW.APPLICATION_DIM AD
									INNER JOIN BDW.PUBLISHER_DIM PD
											ON AD.PUBLISHER_IDN = PD.PUBLISHER_IDN) AD
								ON app.DisplayName = AD.Application_nam
							   AND app.Version = AD.VERSION_COD
							   AND app.Publisher = AD.PUBLISHER_NAM
						 LEFT JOIN BDW.PARTICIPANT_DIM PD
								on h.user_employee_id = pd.EMPLOYEE_ID
							   AND PD.REC_END_DATE = '12-31-9999'
						 LEFT JOIN BDW.DATE_DIM instl
								ON app.InstallDate = instl.DATE
						 LEFT JOIN BDW.DATE_DIM BLD
								ON h.build_bcg_build_date = bld.date
						 LEFT JOIN BDW.DATE_DIM PUR
						        ON h.asset_purchase_date = pur.date
						INNER JOIN BDW.COMP_HARDWARE_DIM cd
								ON  h.Build_computer_name = cd.NAME;





GO
/****** Object:  StoredProcedure [BDW].[LOAD_BDW_PF]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[LOAD_BDW_PF](@RUN_COMMENTS VARCHAR(2000) = NULL) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: LOAD_BDW_PF
//		Date: Jan 21, 2015
//		Description: This is the main process flow for building the data mart.  It coordinates table extracts 
//					 from linked servers as well as staging loads and target loads.  This procedure also has
//					 error handling and audit logging.
//
*/



	BEGIN TRY
		BEGIN TRANSACTION

			DECLARE @AUDIT_IDN INT = NEXT VALUE FOR BDW.RUN_AUDIT_IDN_S;


			--Create an audit record of this run
			INSERT INTO BDW.RUN_AUDIT(RUN_AUDIT_IDN, START_DATETIME, COMMENTS)
			  VALUES(@AUDIT_IDN, GETDATE(),@RUN_COMMENTS);

			--REMOVE CONSTRAINTS----------------------------------------------------------
/*
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK];
	

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK;

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK];
*/
			--END REMOVE CONSTRAINTS--------------------------------------------------------




			--STAGING RUNS---------------------------------------------
			/*
				These procedures pull data from other tables on linked servers they are pulled
				locally to optimize for performance.
			*/

			exec STG.tb_Ref_Cohort_STG_M;
			exec STG.tb_Working_Data_User_Header_STG_M;
			exec STG.tb_Ref_Country_STG_M;
			exec STG.tb_Ref_Office_STG_M;
			exec STG.tb_Ref_Office_system_STG_M;
			exec STG.tb_Ref_Region_New_STG_M;
			exec STG.tb_Ref_Region_STG_M;
			exec STG.tb_CACHE_Daily_Details_STG_M;
			exec STG.vw_BCG_Tableau_Data_STG_M;
			exec stg.SessionDetailsView_STG_M;


			/*
				The data is then joined to resemble the corresponding dimension tables.  Only new
				rows and rows that have been updated in the source make it to the staging tables.
			*/
			exec STG.PARTICIPANT_STG_M;
			exec STG.SERVICE_USAGE_JNK_STG_M;
			exec STG.SERVICE_VENDOR_STG_M;
			exec STG.OFFICE_STG_M;
			exec stg.COMP_HARDWARE_STG_M;
			exec stg.APPLICATION_STG_M;
			exec stg.MOBILE_DEVICE_JNK_STG_M;
			exec stg.MOBILE_DEVICE_STG_M;
			/*
				The dimension tables are where all the descriptive data rests.  Each row has an audit column
				that you can trace back to a run that created or updated the row.
			*/
			--DIM RUNS

			exec BDW.MOBILE_DEVICE_DIM_M @AUDIT_IDN;
			exec BDW.MOBILE_DEVICE_JNK_DIM_M @AUDIT_IDN;
			exec BDW.PARTICIPANT_DIM_M @AUDIT_IDN;
			exec BDW.SERVICE_USAGE_JNK_DIM_M @AUDIT_IDN;
			exec BDW.SERVICE_VENDOR_DIM_M @AUDIT_IDN;
			exec BDW.OFFICE_DIM_M @AUDIT_IDN;
			exec BDW.COMP_HARDWARE_DIM_M @AUDIT_IDN;
			exec BDW.PUBLISHER_DIM_M @AUDIT_IDN;
			exec BDW.APPLICATION_DIM_M @AUDIT_IDN;

			/*
				The fact table is where all the numeric data lies.  The table has participant level granularity
				so it is easy to identify who is on each conference
			*/
			--FACT RUN
			exec BDW.SERVICE_USAGE_FAC_M @AUDIT_IDN;
			exec BDW.COMPUTER_APPLICATION_FAC_M @AUDIT_IDN;
			exec BDW.APP_METERING_FAC_M @AUDIT_IDN;
			exec BDW.MOBILE_DEVICE_APP_FAC_M @AUDIT_IDN;
			--Update audit row to indicate success
			UPDATE BDW.RUN_AUDIT
			   SET END_DATETIME = GETDATE(),
			       [STATUS] = 'S'
			 WHERE RUN_AUDIT_IDN = @AUDIT_IDN;


			--REENABLE CONSTRAINTS------------------------------------------------------------
/*
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK] FOREIGN KEY([PARTICIPANT_IDN])
			REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
			
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK]

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK] FOREIGN KEY([SERVICE_USAGE_JNK_IDN])
			REFERENCES [BDW].[SERVICE_USAGE_JNK_DIM] ([SERVICE_USAGE_JNK_IDN])
			
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK]
			
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK] FOREIGN KEY([OFFICE_IDN])
			REFERENCES [BDW].OFFICE_DIM (OFFICE_IDN)

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK];
*/
			--END REENABLE CONSTRAINTS--------------------------------------------------------------




			COMMIT;
	END TRY
	BEGIN CATCH		
			ROLLBACK;

			--Create the variables to hold the error data
		    DECLARE @ErrorMessage NVARCHAR(4000);
			DECLARE @ErrorSeverity INT;
			DECLARE @ErrorState INT;

			--Load the error data into variables
			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();

			--Update the audit row to indicate Fatal Error
			UPDATE BDW.RUN_AUDIT
			   SET END_DATETIME = GETDATE(),
				   [STATUS] = 'FE',
				   [ERROR_MESSAGE] = @ErrorMessage
			 WHERE RUN_AUDIT_IDN = @AUDIT_IDN;


			 --Throw an error to the session
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);


	END CATCH

	


GO
/****** Object:  StoredProcedure [BDW].[MOBILE_DEVICE_APP_FAC_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[MOBILE_DEVICE_APP_FAC_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: MOBILE_DEVICE_APP_FAC_M
//		Date: Jan 21, 2015
//		Description: This is the fact table that is used to create the association between the Application, Mobile Device
//					 and User.
//					 
*/

				INSERT INTO [BDW].[MOBILE_DEVICE_APP_FAC]
						   ([MOBILE_DEVICE_IDN]
						   ,[MOBILE_DEVICE_JNK_IDN]
						   ,[EMPLOYEE_IDN]
						   ,[APPLICATION_IDN]
						   ,[DEVICE_LAST_SEEN_DATE]
						   ,[DEVICE_LAST_ENROLLED_DATE]
						   ,[DEVICE_LAST_COMPLIANCE_CHK_DATE]
						   ,[DEVICE_LAST_COMPROMISED_CHK_DATE]
						   ,[SNPSHOT_CAPTURE_DATE]
						   ,[MAC_ADDRESS]
						   ,[IMEI]
						   ,[AUDIT_CREATE_IDN]
						   ,[AUDIT_UPDATE_IDN])
   					Select MDD.MOBILE_DEVICE_IDN,
						   MDJD.MOBILE_DEVICE_JNK_IDN,
						   PD.PARTICIPANT_IDN,
						   coalesce(AD.APPLICATION_IDN,0),
						   coalesce(try_convert(datetime,a.LastSeen,103),'12-31-9999'),
						   convert(datetime,a.LastEnrolledOn,103),
						   coalesce(try_convert(datetime,a.LastComplianceCheckOn,103),'12-31-9999'),
						   coalesce(try_convert(datetime,a.LastCompromisedCheckOn,103),'12-31-9999'),
						   coalesce(try_convert(datetime,a.start_date,103),'12-31-9999'),
						   coalesce(a.macaddress,'Undefined') as MAC_ADDRESS,
						   coalesce(a.imei, 'Undefined') AS IMEI,
						   @AUDIT_IDN,
						   @AUDIT_IDN
					  from STG.Airwatch_STG a 
				INNER JOIN dbo.airwatchApps aa
						on a.udid = aa.udid
				INNER JOIN BDW.MOBILE_DEVICE_DIM MDD
						ON	a.model=MDD.MODEL_NAM AND
							a.[Device Model]=MDD.DEVICE_NAM AND
							a.OperatingSystem=MDD.OS_VERSION_NAM
				INNER JOIN BDW.MOBILE_DEVICE_JNK_DIM MDJD
						ON a.Ownership = MDJD.DEVICE_OWNERSHIP AND
						   a.EnrollmentStatus = MDJD.ENROLLMENT_STATUS AND
						   a.ComplianceStatus = MDJD.COMPLIANCE_STATUS AND
						   a.CompromisedStatus = MDJD.COMPROMISED_STATUS AND
						   coalesce(aa.IsManaged,'Undefined') = MDJD.APP_ISMANAGED
				INNER JOIN BDW.PARTICIPANT_DIM PD
						on aa.user_employee_id = pd.employee_id
					   AND pd.rec_end_date = '12-31-9999'
				LEFT JOIN BDW.APPLICATION_DIM AD
						ON aa.ApplicationName = AD.APPLICATION_NAM
					   AND coalesce(Version,'Undefined') = AD.VERSION_COD





GO
/****** Object:  StoredProcedure [BDW].[MOBILE_DEVICE_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [BDW].[MOBILE_DEVICE_DIM_M](@AUDIT_IDN INT) AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: [MOBILE_DEVICE_DIM_M]
//		Date: Jan 21, 2015
//		Description: This table contains all the details around the mobile devices from Airwatch. 
//					
//
*/

			/*
			Insert all new rows from the service vendor staging table.  If there is a match on the company 
			name then update the rest of the columns.  If there is no match, insert the row.
			*/
			MERGE 
			INTO BDW.MOBILE_DEVICE_DIM MDD
			USING  (
					SELECT [PLATFORM_NAM]
						  ,[MODEL_NAM]
						  ,[DEVICE_NAM]
						  ,[OS_VERSION_NAM]
					  FROM [STG].[MOBILE_DEVICE_STG]
					) MDS
			ON 
			   MDD.[MODEL_NAM] = MDS.[MODEL_NAM] AND
			   MDD.[DEVICE_NAM] = MDS.[DEVICE_NAM] AND
			   MDD.[OS_VERSION_NAM] = MDS.[OS_VERSION_NAM]
			WHEN NOT MATCHED THEN
				INSERT ([PLATFORM_NAM],
						[MODEL_NAM],
						[DEVICE_NAM],
						[OS_VERSION_NAM],
						AUDIT_UPDATE_IDN,
						AUDIT_CREATE_IDN)
				VALUES(	MDS.[PLATFORM_NAM],
						MDS.[MODEL_NAM],
						MDS.[DEVICE_NAM],
						MDS.[OS_VERSION_NAM],
						@AUDIT_IDN,
						@AUDIT_IDN)
			WHEN MATCHED THEN
					UPDATE
						SET [PLATFORM_NAM] = MDS.[PLATFORM_NAM],
							AUDIT_UPDATE_IDN = @AUDIT_IDN
					;

			update statistics BDW.MOBILE_DEVICE_DIM WITH SAMPLE 50 PERCENT, ALL;



GO
/****** Object:  StoredProcedure [BDW].[MOBILE_DEVICE_JNK_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[MOBILE_DEVICE_JNK_DIM_M](@AUDIT_IDN INT) AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: [MOBILE_DEVICE_JNK_DIM_M]
//		Date: Mar 12, 2015
//		Description: This is a junk dimension where all status/codes reside that dont logically fit into another table.
//					 All these columns are loosely related meaning each row represents a combination of values on a row.
//					 Another characteristic of a junk dimension is that all the columns that have data are part of the key,
//					 therefore there are only inserts into this table.					 
//
*/


			MERGE 
			INTO BDW.MOBILE_DEVICE_JNK_DIM SJD
			USING  (
					SELECT [DEVICE_OWNERSHIP]
						  ,[ENROLLMENT_STATUS]
						  ,[COMPLIANCE_STATUS]
						  ,[COMPROMISED_STATUS]
						  ,[APP_ISMANAGED]
					  FROM [STG].[MOBILE_DEVICE_JNK_STG]
				  ) SJS
			ON SJD.DEVICE_OWNERSHIP = SJS.DEVICE_OWNERSHIP AND
			   SJD.[ENROLLMENT_STATUS] = SJS.[ENROLLMENT_STATUS] AND
			   SJD.[COMPLIANCE_STATUS] = SJS.[COMPLIANCE_STATUS] AND
			   SJD.[COMPROMISED_STATUS] = SJS.[COMPROMISED_STATUS] AND
			   SJD.[APP_ISMANAGED] = SJS.[APP_ISMANAGED]
			WHEN NOT MATCHED THEN
				INSERT ([DEVICE_OWNERSHIP]
						,[ENROLLMENT_STATUS]
						,[COMPLIANCE_STATUS]
						,[COMPROMISED_STATUS]
						,[APP_ISMANAGED]
						,AUDIT_UPDATE_IDN
						,AUDIT_CREATE_IDN
						)
				VALUES(	SJS.[DEVICE_OWNERSHIP],
						SJS.[ENROLLMENT_STATUS], 
						SJS.[COMPLIANCE_STATUS],
						SJS.[COMPROMISED_STATUS],
						SJS.[APP_ISMANAGED],
						@AUDIT_IDN,
						@AUDIT_IDN
					   )
				  ;

		  update statistics BDW.MOBILE_DEVICE_JNK_DIM WITH SAMPLE 50 PERCENT, ALL;





GO
/****** Object:  StoredProcedure [BDW].[OFFICE_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[OFFICE_DIM_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: OFFICE_DIM_M
//		Date: Jan 21, 2015
//		Description: This is the mapping that loads the Office dimension.  The source data from the 
//					 Office staging table is inserted/updated into the office dimension. The procedure
//					 needs an integer passed to it for auditing purposes
//					 
//
*/


			/*
			Grab all the data from the office staging table.  If the office code is already in the 
			table, then update the rest of the rows.  If it is not in there, insert the row.
			*/ 
			MERGE 
				INTO BDW.OFFICE_DIM OD
				USING (SELECT [OFFICE_COD]
							  ,[OFFICE_NAM]
							  ,[REGION_COD_NEW]
							  ,[REGION_COD_OLD]
							  ,[COUNTRY_NAM]
							  ,[SYSTEM_NAM]
						  FROM [STG].[OFFICE_STG]) OS
				ON
					OD.OFFICE_COD = OS.OFFICE_COD
			WHEN MATCHED THEN
				UPDATE SET OD.OFFICE_NAM = OS.OFFICE_NAM,
						   OD.REGION_COD_NEW = OS.REGION_COD_NEW,
						   OD.REGION_COD_OLD = OS.REGION_COD_OLD,
						   OD.COUNTRY_NAM = OS.COUNTRY_NAM,
						   OD.SYSTEM_NAM = OS.SYSTEM_NAM,
						   OD.AUDIT_UPDATE_IDN = @AUDIT_IDN
			WHEN NOT MATCHED THEN
					INSERT( OFFICE_COD, 
							OFFICE_NAM, 
							REGION_COD_NEW, 
							REGION_COD_OLD, 
							COUNTRY_NAM, 
							SYSTEM_NAM, 
							AUDIT_UPDATE_IDN, 
							AUDIT_CREATE_IDN)		
					VALUES(OS.OFFICE_COD, 
							OS.OFFICE_NAM, 
							OS.REGION_COD_NEW, 
							OS.REGION_COD_OLD, 
							OS.COUNTRY_NAM, 
							OS.SYSTEM_NAM, 
							@AUDIT_IDN,
							@AUDIT_IDN)
;


  update statistics BDW.OFFICE_DIM WITH SAMPLE 50 PERCENT, ALL;


GO
/****** Object:  StoredProcedure [BDW].[PARTICIPANT_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[PARTICIPANT_DIM_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: PARTICIPANT_DIM_M
//		Date: Jan 21, 2015
//		Description: This is the mapping that loads the Participant dimension.  The source data from the 
//					 Participant staging table is inserted/updated into the participant dimension. The procedure
//					 needs an integer passed to it for auditing purposes
//					 
//
*/

			/*
				Grab all the data from the participant staging table.  Match the row on the full name and 
				email address.  If there is a match, update the rest of the columns.  If there isnt a match
				insert the entire row.
			*/

			MERGE 
				INTO BDW.PARTICIPANT_DIM PD
				USING (SELECT EMPLOYEE_ID
							  ,[FIRST_NAM]
							  ,[LAST_NAM]
							  ,[FULL_NAM]
							  ,[OFFICE_COD]
							  ,[JOB_TITLE_NAM]
							  ,[EMAIL]
							  ,[JOB_NAM]
							  ,[ASSISTANT_NAM]
							  ,[HIRE_DATE]
							  ,[TERMINATE_DATE]
							  ,[COHORT_NAM]
			--				  ,[VIDYO_INSTLD_IND]
			--				  ,[WEBEX_INSTLD_IND]
			--				  ,[LYNC_INSTLD_IND]
							  ,REC_START_DATE
							  ,REC_END_DATE
							  ,EMPLOYEE_TYPE_NAM
							  ,DEPARTMENT_NAM
						 FROM [STG].[PARTICIPANT_STG]) PS
				ON
					PD.FULL_NAM = PS.FULL_NAM AND
					PD.EMAIL = PS.EMAIL
			WHEN MATCHED THEN
				UPDATE SET PD.EMPLOYEE_ID = PS.EMPLOYEE_ID,
						   PD.FIRST_NAM = PS.FIRST_NAM,
						   PD.LAST_NAM = PS.LAST_NAM,
						   PD.OFFICE_COD = PS.OFFICE_COD,
						   PD.JOB_TITLE_NAM = PS.JOB_TITLE_NAM,
						   PD.JOB_NAM = PS.JOB_NAM,
						   PD.ASSISTANT_NAM = PS.ASSISTANT_NAM,
						   PD.HIRE_DATE = PS.HIRE_DATE,
						   PD.TERMINATE_DATE = PS.TERMINATE_DATE,
						   PD.COHORT_NAM = PS.COHORT_NAM,
						   PD.VIDYO_INSTLD_IND = 'Y',
						   PD.WEBEX_INSTLD_IND = 'Y',
						   PD.LYNC_INSTLD_IND = 'Y',
						   PD.AUDIT_UPDATE_IDN = @AUDIT_IDN,
						   PD.EMPLOYEE_TYPE_NAM = PS.EMPLOYEE_TYPE_NAM,
						   PD.DEPARTMENT_NAM = PS.EMPLOYEE_TYPE_NAM
			WHEN NOT MATCHED THEN
					INSERT( EMPLOYEE_ID, 
							FIRST_NAM, 
							LAST_NAM, 
							FULL_NAM, 
							OFFICE_COD, 
							JOB_TITLE_NAM, 
							EMAIL, 
							JOB_NAM, 
							ASSISTANT_NAM, 
							HIRE_DATE, 
							TERMINATE_DATE, 
							COHORT_NAM,
							VIDYO_INSTLD_IND,
							WEBEX_INSTLD_IND,
							LYNC_INSTLD_IND,
							AUDIT_UPDATE_IDN, 
							AUDIT_CREATE_IDN,
							REC_START_DATE,
							REC_END_DATE,
							EMPLOYEE_TYPE_NAM,
							DEPARTMENT_NAM)		
					VALUES(PS.EMPLOYEE_ID, 
							PS.FIRST_NAM, 
							PS.LAST_NAM, 
							PS.FULL_NAM, 
							PS.OFFICE_COD, 
							PS.JOB_TITLE_NAM, 
							PS.EMAIL, 
							PS.JOB_NAM, 
							PS.ASSISTANT_NAM, 
							PS.HIRE_DATE, 
							PS.TERMINATE_DATE, 
							PS.COHORT_NAM, 
							'Y',
							'Y',
							'Y',
							@AUDIT_IDN,
							@AUDIT_IDN,
							PS.REC_START_DATE,
							PS.REC_END_DATE,
							PS.EMPLOYEE_TYPE_NAM,
							PS.DEPARTMENT_NAM)
							;


		update statistics BDW.PARTICIPANT_DIM WITH SAMPLE 50 PERCENT, ALL;




GO
/****** Object:  StoredProcedure [BDW].[PUBLISHER_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[PUBLISHER_DIM_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: PUBLISHER_DIM_M
//		Date: Feb 24, 2015
//		Description: This is the mapping that loads the Publihser dimension.  The source data from the 
//					 application staging table is inserted/updated into the publihser dimension. The procedure
//					 needs an integer passed to it for auditing purposes
//					 
//
*/


			/*
			Grab all the data from the office staging table.  If the office code is already in the 
			table, then update the rest of the rows.  If it is not in there, insert the row.
			*/ 
			MERGE 
				INTO BDW.PUBLISHER_DIM PD
				USING (SELECT distinct PUBLISHER_NAM
						  FROM [STG].[APPLICATION_STG]) APS
				ON
					PD.PUBLISHER_NAM = APS.PUBLISHER_NAM
/*
			WHEN MATCHED THEN
				UPDATE SET OD.OFFICE_NAM = OS.OFFICE_NAM,
						   OD.REGION_COD_NEW = OS.REGION_COD_NEW,
						   OD.REGION_COD_OLD = OS.REGION_COD_OLD,
						   OD.COUNTRY_NAM = OS.COUNTRY_NAM,
						   OD.SYSTEM_NAM = OS.SYSTEM_NAM,
						   OD.AUDIT_UPDATE_IDN = @AUDIT_IDN
*/
			WHEN NOT MATCHED THEN
					INSERT( PUBLISHER_NAM,
							AUDIT_UPDATE_IDN, 
							AUDIT_CREATE_IDN)		
					VALUES(APS.PUBLISHER_NAM, 
							@AUDIT_IDN,
							@AUDIT_IDN)
;


  update statistics BDW.PUBLISHER_DIM WITH SAMPLE 50 PERCENT, ALL;



GO
/****** Object:  StoredProcedure [BDW].[SERVICE_USAGE_FAC_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [BDW].[SERVICE_USAGE_FAC_M](@AUDIT_IDN INT) AS

/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: SERVICE_USAGE_FAC_M
//		Date: Jan 21, 2015
//		Description: This mapping loads the fact table.  Currently it takes in the data loaded onto the
//					 the MeetMe_Webex table and VIYO_STG table.  The lines are then joined to the dimension
//					 tables to the get the primary keys (idns) and inserted into the fact table.  This fact
//					 table also contains constraints for referential integrity.  Fact tables contain numeric
//					 data that can be easly avg'ed, sum'ed etc..
//					 
*/

--INSERT VIDYO DATA

		CREATE TABLE #VIDYO_FULL_MATCH
			(CALLID		INTEGER);

		CREATE TABLE #VIDYO_EMAIL_MATCH
			(CALLID		INTEGER);

		CREATE UNIQUE CLUSTERED INDEX VFM_UNQ01 ON #VIDYO_FULL_MATCH(CALLID);
		CREATE UNIQUE CLUSTERED INDEX VEM_UNQ01 ON #VIDYO_EMAIL_MATCH(CALLID);




		--Create a temporary table to hold the CallIds that match on both full name and email.
			INSERT INTO #VIDYO_FULL_MATCH
							SELECT CALLID
							  from STG.VIDYO_STG VD
						INNER JOIN BDW.PARTICIPANT_DIM PD
								ON  vd.CALLERNAME = PD.FULL_NAM
							   AND CASE
										WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
										THEN CONVERT(VARCHAR(50),callerID)
										ELSE 'Undefined'
									END  = PD.EMAIL;

		--Create a temporary table to hold the CallIds that match only on email and are not in the full name and email dataset.
			INSERT INTO #VIDYO_EMAIL_MATCH
							SELECT distinct vd.CALLID
							  from STG.VIDYO_STG VD
						INNER JOIN BDW.PARTICIPANT_DIM PD
								ON  CASE
										WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
										THEN CONVERT(VARCHAR(50),callerID)
--										ELSE 'Undefined'
									END  = PD.EMAIL
					    LEFT JOIN #VIDYO_FULL_MATCH VFM 
							   on VD.CALLID = VFM.CALLID
							WHERE VFM.CALLID is null
							;
			

		/*
			Build and insert the Vidyo rows into the fact table that match on both full name and email.  All joins
			onto the dimension table are made against the natural key.
		*/

			INSERT INTO BDW.SERVICE_USAGE_FAC(	CONFERENCE_ID,
												START_DATETIME,
												END_DATETIME,
												DURATION,
												BRIDGE_AMT,
												TRANSPORT_AMT,
												TOTAL_AMT,
												COST_USD_AMT,
												START_DATE_IDN,
												END_DATE_IDN,
												SERVICE_VENDOR_IDN,
												PARTICIPANT_IDN,
												SERVICE_USAGE_JNK_IDN,
												AUDIT_CREATE_IDN,
												AUDIT_UPDATE_IDN,
												OFFICE_IDN)
					Select [UniqueCallId] as CONFERENCE_ID,
						   [JoinTime] as START_DATETIME,
						   [LeaveTime] as END_DATETIME,
						   DateDIFF(ss,[JoinTime],[LeaveTime]) as DURATION,
						   null as BRIDGE_AMT,
						   null as TRANSPORT_AMT,
						   null as TOTAL_AMT,
						   null as COST_USD_AMT,
						   jt.date_idn as START_DATE_IDN,
						   lt.date_idn as END_DATE_IDN,
						   sv.SERVICE_VENDOR_IDN,
						   PD.PARTICIPANT_IDN,
						   sj.SERVICE_USAGE_JNK_IDN
						   ,@AUDIT_IDN as AUDIT_CREATE_IDN 
						   ,@AUDIT_IDN as AUDIT_UPDATE_IDN
						   ,PD.OFFICE_IDN as OFFICE_IDN
					  from STG.VIDYO_STG VD
					 INNER JOIN (Select p.PARTICIPANT_IDN,
										p.FULL_NAM,
										p.EMAIL,
										bdw.GET_OFFICE_IDN(OFFICE_COD) as OFFICE_IDN
								  FROM BDW.PARTICIPANT_DIM P) PD
						ON vd.CALLERNAME = PD.FULL_NAM 
					   AND CASE
							 WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
							  THEN callerID
							  ELSE 'Undefined'
							END  = PD.EMAIL 
					 INNER JOIN BDW.DATE_DIM jt
						ON cast(vd.[JoinTime] as date) = jt.[date]
					 INNER JOIN BDW.DATE_DIM lt
						ON cast(vd.[LeaveTime] as date) = lt.[date]
					 INNER JOIN BDW.SERVICE_VENDOR_DIM SV
						ON COMPANY_NAME = 'Vidyo'
					 INNER JOIN BDW.SERVICE_USAGE_JNK_DIM SJ
						on 	SJ.ROLE = 'Undefined' AND
							STG.GET_LKUP_CODE('Vidyo','EndpointType',SJ.PLATFORM_NAM) = vd.EndpointType AND
							SJ.CONFERENCE_NAM = vd.[ConferenceName] AND
							STG.GET_LKUP_CODE('Vidyo','ConferenceType',SJ.COMMUNICATION_TYPE_NAM) = vd.ConferenceType AND
							coalesce(STG.GET_LKUP_CODE('Vidyo','Router',SJ.BRIDGE_ROUTER_COD),'Undefined') = coalesce(vd.RouterID,'Undefined') AND
							SJ.CALL_STATE = vd.CallState AND
							STG.GET_LKUP_CODE('Vidyo','Direction',SJ.DIRECTION) = vd.direction AND
							SJ.CHARGE_COUNTRY_COD = 'Undefined' AND
							SJ.CHARGE_COUNTRY_NAM = 'Undefined' AND
							SJ.HOST_FLG = 	CASE
				   								WHEN vd.ConferenceName like '%'+vd.CallerID+'%' AND vd.ConferenceType != 'D' THEN 'Y'
												ELSE 'N'
											END AND
							SJ.MEDIA_TYPE_NAM = 'Video' AND
							SJ.GATEWAY_ID_COD = CASE
												  WHEN vd.gwid ='' THEN 'Undefined'
												  WHEN vd.gwid is null THEN 'Undefined'
												  else vd.gwid
												 END  AND
							SJ.GATEWAY_PREFIX_COD = CASE
													WHEN vd.gwprefix = '' THEN 'Undefined'
													WHEN vd.gwprefix is null then 'Undefined'
													ELSE vd.gwprefix
													END
					 INNER JOIN #VIDYO_FULL_MATCH vfm
						ON vd.CALLID = vfm.CALLID
							;



		/*
			Build and insert the Vidyo rows into the fact table that only match on email and are not part of the other dataset.
			All joins onto the dimension table are made against the natural key.
		*/

			INSERT INTO BDW.SERVICE_USAGE_FAC(	CONFERENCE_ID,
												START_DATETIME,
												END_DATETIME,
												DURATION,
												BRIDGE_AMT,
												TRANSPORT_AMT,
												TOTAL_AMT,
												COST_USD_AMT,
												START_DATE_IDN,
												END_DATE_IDN,
												SERVICE_VENDOR_IDN,
												PARTICIPANT_IDN,
												SERVICE_USAGE_JNK_IDN,
												AUDIT_CREATE_IDN,
												AUDIT_UPDATE_IDN,
												OFFICE_IDN)
					Select [UniqueCallId] as CONFERENCE_ID,
						   [JoinTime] as START_DATETIME,
						   [LeaveTime] as END_DATETIME,
						   DateDIFF(ss,[JoinTime],[LeaveTime]) as DURATION,
						   null as BRIDGE_AMT,
						   null as TRANSPORT_AMT,
						   null as TOTAL_AMT,
						   null as COST_USD_AMT,
						   jt.date_idn as START_DATE_IDN,
						   lt.date_idn as END_DATE_IDN,
						   sv.SERVICE_VENDOR_IDN,
						   PD.PARTICIPANT_IDN,
						   sj.SERVICE_USAGE_JNK_IDN
						   ,@AUDIT_IDN as AUDIT_CREATE_IDN 
						   ,@AUDIT_IDN as AUDIT_UPDATE_IDN
						   ,PD.OFFICE_IDN as OFFICE_IDN
					  from STG.VIDYO_STG VD
					 INNER JOIN (Select p.PARTICIPANT_IDN,
										p.FULL_NAM,
										p.EMAIL,
										bdw.GET_OFFICE_IDN(OFFICE_COD) as OFFICE_IDN
								  FROM BDW.PARTICIPANT_DIM P) PD
					    ON CASE
							 WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
							  THEN callerID
							  ELSE 'Undefined'
							END  = PD.EMAIL 
					 INNER JOIN BDW.DATE_DIM jt
						ON cast(vd.[JoinTime] as date) = jt.[date]
					 INNER JOIN BDW.DATE_DIM lt
						ON cast(vd.[LeaveTime] as date) = lt.[date]
					 INNER JOIN BDW.SERVICE_VENDOR_DIM SV
						ON COMPANY_NAME = 'Vidyo'
					 INNER JOIN BDW.SERVICE_USAGE_JNK_DIM SJ
						on 	SJ.ROLE = 'Undefined' AND
							STG.GET_LKUP_CODE('Vidyo','EndpointType',SJ.PLATFORM_NAM) = vd.EndpointType AND
							SJ.CONFERENCE_NAM = vd.[ConferenceName] AND
							STG.GET_LKUP_CODE('Vidyo','ConferenceType',SJ.COMMUNICATION_TYPE_NAM) = vd.ConferenceType AND
							coalesce(STG.GET_LKUP_CODE('Vidyo','Router',SJ.BRIDGE_ROUTER_COD),'Undefined') = coalesce(vd.RouterID,'Undefined') AND
							SJ.CALL_STATE = vd.CallState AND
							STG.GET_LKUP_CODE('Vidyo','Direction',SJ.DIRECTION) = vd.direction AND
							SJ.CHARGE_COUNTRY_COD = 'Undefined' AND
							SJ.CHARGE_COUNTRY_NAM = 'Undefined' AND
							SJ.HOST_FLG = 	CASE
				   								WHEN vd.ConferenceName like '%'+vd.CallerID+'%' AND vd.ConferenceType != 'D' THEN 'Y'
												ELSE 'N'
											END AND
							SJ.MEDIA_TYPE_NAM = 'Video' AND
							SJ.GATEWAY_ID_COD = CASE
												  WHEN vd.gwid ='' THEN 'Undefined'
												  WHEN vd.gwid is null THEN 'Undefined'
												  else vd.gwid
												 END AND
							SJ.GATEWAY_PREFIX_COD = CASE
													WHEN vd.gwprefix = '' THEN 'Undefined'
													WHEN vd.gwprefix is null then 'Undefined'
													ELSE vd.gwprefix
													END
					 INNER JOIN #VIDYO_EMAIL_MATCH vem
						ON vd.CALLID = vem.CALLID	
							;


				--UPDATE THE CHAIRPERSON IDN FOR VIDYO
						UPDATE suf
						   SET CHAIRPERSON_IDN = pd.PARTICIPANT_IDN
						  from stg.VIDYO_STG v
					INNER JOIN BDW.SERVICE_USAGE_FAC suf
							on cast(suf.CONFERENCE_ID as varchar) = cast(v.uniqueCallID as varchar)
					INNER JOIN BDW.PARTICIPANT_DIM pd
							ON v.callerID = pd.email
						 where ConferenceName like '%'+CallerID+'%' 
						   AND ConferenceType != 'D' 
						   ;


---END INSERT VIDYO DATA



--INSERT MEETME/WEBEX DATA



		CREATE TABLE #MMWX_FULL_MATCH
			(NAME		NVARCHAR(65),
			 EMAIL		VARCHAR(50));

		CREATE TABLE #MMWX_EMAIL_MATCH
			(NAME		NVARCHAR(65),
			 EMAIL		VARCHAR(50));

		CREATE UNIQUE CLUSTERED INDEX MMWX_FM_UNQ01 ON #MMWX_FULL_MATCH(NAME, EMAIL);
		CREATE UNIQUE CLUSTERED INDEX MMWX_EM_UNQ01 ON #MMWX_EMAIL_MATCH(NAME, EMAIL);


		--Insert into a temporary table the names and emails that match for MeetMe/Webex data
			INSERT INTO #MMWX_FULL_MATCH 
							SELECT distinct
									FIRST_VALUE(mw.name) OVER (partition by mw.email order by [End Time] desc) as Name,
									CASE MW.email
									 WHEN '' then 'Undefined'
									 ELSE mw.email
									end as email
							  from stg.MeetMe_Webex MW
							 INNER JOIN BDW.PARTICIPANT_DIM PD
								ON mw.[NAME] = PD.FULL_NAM 
							   AND CASE MW.email
									 WHEN '' then 'Undefined'
									 ELSE mw.email
									end   = PD.EMAIL;
			/*
			Insert into a temporary table the emails that match for MeetMe/Webex data and that aren't in the other
			dataset
			*/
			INSERT INTO #MMWX_EMAIL_MATCH
				SELECT distinct
						mw.[NAME],
						mw.email
					FROM		(SELECT distinct
									FIRST_VALUE(mw.name) OVER (partition by mw.email order by [End Time] desc) as [NAME],
									CASE MW.email
									 WHEN '' then 'Undefined'
									 ELSE mw.email
									end as EMAIL
							  from stg.MeetMe_Webex mw) MW
							 INNER JOIN BDW.PARTICIPANT_DIM PD
							   ON CASE MW.email
									 WHEN '' then 'Undefined'
									 ELSE mw.email
									end   = PD.EMAIL
						LEFT JOIN #MMWX_FULL_MATCH FM
							     ON mw.name = fm.name
								and mw.email = fm.email
							WHERE fm.name is null
								;
	


			/*
			Insert into the fact table the MeetMe/Webex records that match on both email and full name.
			*/
				INSERT INTO BDW.SERVICE_USAGE_FAC
												(CONFERENCE_ID,
												START_DATETIME,
												END_DATETIME,
												DURATION,
												BRIDGE_AMT,
												TRANSPORT_AMT,
												TOTAL_AMT,
												COST_USD_AMT,
												START_DATE_IDN,
												END_DATE_IDN,
												SERVICE_VENDOR_IDN,
												PARTICIPANT_IDN,
												SERVICE_USAGE_JNK_IDN,
												AUDIT_CREATE_IDN,
												AUDIT_UPDATE_IDN,
												OFFICE_IDN,
												CHAIRPERSON_IDN)
					Select mw.[Conference Id],
						   [Start Time] as START_DATETIME,
						   [End Time] as END_DATETIME,
						   DateDIFF(ss,[Start Time],[End Time]) as DURATION,
						   [Bridge Amount] AS BRIDGE_AMT,
						   [Transport Amount] AS TRANSPORT_AMT,
						   [Total Amount] as TOTAL_AMT,
						   [Total Amount] as COST_USD_AMT,
						   jt.date_idn as START_DATE_IDN,
						   lt.date_idn as END_DATE_IDN,
						   sv.SERVICE_VENDOR_IDN,
						   PD.PARTICIPANT_IDN,
						   sj.SERVICE_USAGE_JNK_IDN,
						   @AUDIT_IDN as AUDIT_CREATE_IDN, 
						   @AUDIT_IDN as AUDIT_UPDATE_IDN,
						   PD.OFFICE_IDN as OFFICE_IDN,
						   chrperson.PARTICIPANT_IDN as CHAIRPERSON_IDN
					  from stg.MeetMe_Webex MW
					 INNER JOIN (Select p.PARTICIPANT_IDN,
										p.FULL_NAM,
										p.EMAIL,
										p.employee_id,
										bdw.GET_OFFICE_IDN(OFFICE_COD) as OFFICE_IDN
								  FROM BDW.PARTICIPANT_DIM P) PD
						ON mw.[NAME] = PD.FULL_NAM 
					   AND CASE MW.email
							 WHEN '' then 'Undefined'
							 ELSE mw.email
							end   = PD.EMAIL 
					 INNER JOIN BDW.DATE_DIM jt
						ON cast(MW.[Start Time] as date) = jt.[date]
					 INNER JOIN BDW.DATE_DIM lt
						ON cast(MW.[End Time] as date) = lt.[date]
					 INNER JOIN BDW.SERVICE_VENDOR_DIM SV
						ON COMPANY_NAME = MW.Product
					INNER JOIN #MMWX_FULL_MATCH MFM
							ON MW.Name = MFM.NAME
						   AND CASE MW.email
								 WHEN '' then 'Undefined'
								 ELSE mw.email
								end  = MFM.EMAIL
					left join STG.Conference_Chairperson_STG cc
							on mw.[conference id] = cc.[conference id]
					LEFT JOIN BDW.PARTICIPANT_DIM chrperson
							on cc.[employee id] = chrperson.EMPLOYEE_ID	
						   AND chrperson.rec_end_date = '9999-12-31'	
					 INNER JOIN BDW.SERVICE_USAGE_JNK_DIM SJ
						on 	SJ.ROLE = MW.ROLE AND
							SJ.PLATFORM_NAM = MW.[Category Name] AND
							SJ.CONFERENCE_NAM = 'Undefined' AND
							SJ.COMMUNICATION_TYPE_NAM = CASE 
															WHEN [Call Type] = '' THEN 'Undefined'
															WHEN [Call Type] is NULL THEN 'Undefined'
															ELSE [Call Type]
														 END AND
							SJ.BRIDGE_ROUTER_COD = 	CASE  
													  WHEN [Bridge Name]= '' THEN 'Undefined'
													  WHEN [Bridge Name] is NULL THEN 'Undefined'
													  ELSE [Bridge Name]
													 END AND
							SJ.CALL_STATE = 'COMPLETED' AND
							SJ.DIRECTION = 'Undefined'	AND
							SJ.CHARGE_COUNTRY_NAM = CASE
														  WHEN MW.Country = '' THEN 'Undefined'
														  WHEN MW.Country is null THEN 'Undefined'
														  ELSE MW.Country
														END  AND
							SJ.CHARGE_COUNTRY_COD = 	CASE
														  WHEN MW.[Charge Band] = '' THEN 'Undefined'
														  WHEN MW.[Charge Band] is null THEN 'Undefined'
														  ELSE MW.[Charge Band]
														END  AND
							SJ.HOST_FLG =     CASE
												--WHEN MW.Role = 'Chairperson' THEN 'Y'
												WHEN pd.employee_id = cc.[employee id] then 'Y'
												ELSE 'N'
											  END AND 
							SJ.MEDIA_TYPE_NAM =   CASE mw.Product
													WHEN 'WebEx' THEN 'Video'
													WHEN 'MeetMe' THen 'Audio'
												  END	AND
							SJ.GATEWAY_ID_COD = 'Undefined' AND
							SJ.GATEWAY_PREFIX_COD = 'Undefined'
							;

				/*
					Insert into the fact table the MeetMe/Webex records that only match on email and that haven't already been
					inserted into the table.
				*/
				INSERT INTO BDW.SERVICE_USAGE_FAC
												(CONFERENCE_ID,
												START_DATETIME,
												END_DATETIME,
												DURATION,
												BRIDGE_AMT,
												TRANSPORT_AMT,
												TOTAL_AMT,
												COST_USD_AMT,
												START_DATE_IDN,
												END_DATE_IDN,
												SERVICE_VENDOR_IDN,
												PARTICIPANT_IDN,
												SERVICE_USAGE_JNK_IDN,
												AUDIT_CREATE_IDN,
												AUDIT_UPDATE_IDN,
												OFFICE_IDN,
												CHAIRPERSON_IDN)
					Select mw.[Conference Id],
						   [Start Time] as START_DATETIME,
						   [End Time] as END_DATETIME,
						   DateDIFF(ss,[Start Time],[End Time]) as DURATION,
						   [Bridge Amount] AS BRIDGE_AMT,
						   [Transport Amount] AS TRANSPORT_AMT,
						   [Total Amount] as TOTAL_AMT,
						   [Total Amount] as COST_USD_AMT,
						   jt.date_idn as START_DATE_IDN,
						   lt.date_idn as END_DATE_IDN,
						   sv.SERVICE_VENDOR_IDN,
						   PD.PARTICIPANT_IDN,
						   sj.SERVICE_USAGE_JNK_IDN,
						   @AUDIT_IDN as AUDIT_CREATE_IDN, 
						   @AUDIT_IDN as AUDIT_UPDATE_IDN,
						   PD.OFFICE_IDN as OFFICE_IDN,
						   chrperson.PARTICIPANT_IDN as CHAIRPERSON_IDN
					  from stg.MeetMe_Webex MW
					 INNER JOIN (Select p.PARTICIPANT_IDN,
										p.FULL_NAM,
										p.EMAIL,
										p.employee_id,
										bdw.GET_OFFICE_IDN(OFFICE_COD) as OFFICE_IDN
								  FROM BDW.PARTICIPANT_DIM P) PD
						ON CASE MW.email
							 WHEN '' then 'Undefined'
							 ELSE mw.email
							end   = PD.EMAIL 
					 INNER JOIN BDW.DATE_DIM jt
						ON cast(MW.[Start Time] as date) = jt.[date]
					 INNER JOIN BDW.DATE_DIM lt
						ON cast(MW.[End Time] as date) = lt.[date]
					 INNER JOIN BDW.SERVICE_VENDOR_DIM SV
						ON COMPANY_NAME = MW.Product
					INNER JOIN #MMWX_EMAIL_MATCH MEM
							ON MW.Name = MEM.NAME
						   AND CASE MW.email
								 WHEN '' then 'Undefined'
								 ELSE mw.email
								end  = MEM.EMAIL
					left join STG.Conference_Chairperson_STG cc
						   on mw.[conference id] = cc.[conference id]
					LEFT JOIN BDW.PARTICIPANT_DIM chrperson
						   on cc.[employee id] = chrperson.EMPLOYEE_ID
						  AND chrperson.rec_end_date = '9999-12-31'	
					 INNER JOIN BDW.SERVICE_USAGE_JNK_DIM SJ
						on 	SJ.ROLE = MW.ROLE AND
							SJ.PLATFORM_NAM = MW.[Category Name] AND
							SJ.CONFERENCE_NAM = 'Undefined' AND
							SJ.COMMUNICATION_TYPE_NAM = CASE 
															WHEN [Call Type] = '' THEN 'Undefined'
															WHEN [Call Type] is NULL THEN 'Undefined'
															ELSE [Call Type]
														 END AND
							SJ.BRIDGE_ROUTER_COD = 	CASE  
													  WHEN [Bridge Name]= '' THEN 'Undefined'
													  WHEN [Bridge Name] is NULL THEN 'Undefined'
													  ELSE [Bridge Name]
													 END AND
							SJ.CALL_STATE = 'COMPLETED' AND
							SJ.DIRECTION = 'Undefined'	AND
							SJ.CHARGE_COUNTRY_NAM = CASE
														  WHEN MW.Country = '' THEN 'Undefined'
														  WHEN MW.Country is null THEN 'Undefined'
														  ELSE MW.Country
														END  AND
							SJ.CHARGE_COUNTRY_COD = 	CASE
														  WHEN MW.[Charge Band] = '' THEN 'Undefined'
														  WHEN MW.[Charge Band] is null THEN 'Undefined'
														  ELSE MW.[Charge Band]
														END  AND
							SJ.HOST_FLG =     CASE
												--WHEN MW.Role = 'Chairperson' THEN 'Y'
												WHEN pd.employee_id = cc.[employee id] then 'Y'
												ELSE 'N'
											  END  AND
						   SJ.MEDIA_TYPE_NAM = 	CASE mw.Product
												 WHEN 'WebEx' THEN 'Video'
												 WHEN 'MeetMe' THen 'Audio'
												END	AND
							SJ.GATEWAY_ID_COD = 'Undefined' AND
							SJ.GATEWAY_PREFIX_COD = 'Undefined'																	  																			
							;

			-- INSERT LYNC DATA
				WITH LYNC AS 
						(Select FromUri,
								ToUri,
								DialogId,
								 SessionIdTime as START_DATETIME,
								 EndTime as END_DATETIME,
								 DateDIFF(ss,SessionIdTime,EndTime) as DURATION,
								jt.DATE_IDN as START_DATE_IDN,
								lt.DATE_IDN as END_DATE_IDN,
								sv.SERVICE_VENDOR_IDN,
--								suj.SERVICE_USAGE_JNK_IDN as SERVICE_USAGE_JNK_IDN,
								@AUDIT_IDN as AUDIT_CREATE_IDN,
								@AUDIT_IDN as AUDIT_UPDATE_IDN,
								coalesce(chrperson.PARTICIPANT_IDN,0) as CHAIRPERSON_IDN,
								sd.MediaTypes
						  from STG.SessionDetailsView_STG sd
					LEFT join BDW.PARTICIPANT_DIM chrperson
							on sd.FromUri = chrperson.email
					INNER JOIN BDW.DATE_DIM jt
							ON cast(sd.SessionIdTime as date) = jt.[date]
					INNER JOIN BDW.DATE_DIM lt
							ON cast(sd.EndTime as date) = lt.[date]
					INNER JOIN BDW.SERVICE_VENDOR_DIM SV
							ON sv.COMPANY_NAME = 'Lync'

				  )
				INSERT 
				  INTO BDW.SERVICE_USAGE_FAC(
											 CONFERENCE_ID,
											 START_DATETIME,
											 END_DATETIME,
											 DURATION,
											 START_DATE_IDN,
											 END_DATE_IDN,
											 SERVICE_VENDOR_IDN,
											 PARTICIPANT_IDN,
											 SERVICE_USAGE_JNK_IDN,
											 AUDIT_CREATE_IDN,
											 AUDIT_UPDATE_IDN,
											 OFFICE_IDN,
											 CHAIRPERSON_IDN)

						Select  DialogId,
								START_DATETIME,
								END_DATETIME,
								DURATION,
								START_DATE_IDN,
								END_DATE_IDN,
								SERVICE_VENDOR_IDN,
								coalesce(pd.PARTICIPANT_IDN,0) as PARTICIPANT_IDN,
								SERVICE_USAGE_JNK_IDN,
								l.AUDIT_CREATE_IDN,
								l.AUDIT_UPDATE_IDN,
								BDW.GET_OFFICE_IDN(pd.OFFICE_COD) as OFFICE_IDN,
								CHAIRPERSON_IDN
						  from LYNC l
					LEFT join BDW.PARTICIPANT_DIM pd
							on FromUri = pd.email
					INNER JOIN BDW.SERVICE_USAGE_JNK_DIM SUJ
							ON SUJ.ROLE = 'Undefined'
						   AND SUJ.PLATFORM_NAM = 'Undefined'
						   AND SUJ.CONFERENCE_NAM = 'Undefined'
						   AND SUJ.COMMUNICATION_TYPE_NAM = 'Undefined'
						   AND SUJ.BRIDGE_ROUTER_COD = 'Undefined'
						   AND SUJ.CALL_STATE = 'Undefined'
						   AND SUJ.DIRECTION = 'Undefined'
						   AND SUJ.CHARGE_COUNTRY_COD = 'Undefined'
						   AND SUJ.CHARGE_COUNTRY_NAM = 'Undefined'
						   AND SUJ.HOST_FLG = 'Y'
						   AND SUJ.MEDIA_TYPE_NAM = l.MediaTypes
						   AND SUJ.GATEWAY_ID_COD = 'Undefined'
						   AND SUJ.GATEWAY_PREFIX_COD = 'Undefined'
					UNION ALL
					Select  DialogId,
							START_DATETIME,
							END_DATETIME,
							DURATION,
							START_DATE_IDN,
							END_DATE_IDN,
							SERVICE_VENDOR_IDN,
							coalesce(pd.PARTICIPANT_IDN,0) as PARTICIPANT_IDN,
							suj.SERVICE_USAGE_JNK_IDN,
							l.AUDIT_CREATE_IDN,
							l.AUDIT_UPDATE_IDN,
							BDW.GET_OFFICE_IDN(pd.OFFICE_COD) as OFFICE_IDN,
							CHAIRPERSON_IDN
					  from LYNC l
					LEFT join BDW.PARTICIPANT_DIM pd
							on ToUri = pd.email
					INNER JOIN BDW.SERVICE_USAGE_JNK_DIM SUJ
							ON SUJ.ROLE = 'Undefined'
						   AND SUJ.PLATFORM_NAM = 'Undefined'
						   AND SUJ.CONFERENCE_NAM = 'Undefined'
						   AND SUJ.COMMUNICATION_TYPE_NAM = 'Undefined'
						   AND SUJ.BRIDGE_ROUTER_COD = 'Undefined'
						   AND SUJ.CALL_STATE = 'Undefined'
						   AND SUJ.DIRECTION = 'Undefined'
						   AND SUJ.CHARGE_COUNTRY_COD = 'Undefined'
						   AND SUJ.CHARGE_COUNTRY_NAM = 'Undefined'
						   AND SUJ.HOST_FLG = 'N'
						   AND SUJ.MEDIA_TYPE_NAM = l.MediaTypes
						   AND SUJ.GATEWAY_ID_COD = 'Undefined'
						   AND SUJ.GATEWAY_PREFIX_COD = 'Undefined'
						   ;


GO
/****** Object:  StoredProcedure [BDW].[SERVICE_USAGE_JNK_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[SERVICE_USAGE_JNK_DIM_M](@AUDIT_IDN INT) AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: SERVICE_USAGE_JNK_DIM_M
//		Date: Jan 21, 2015
//		Description: This is a junk dimension where all status/codes reside that dont logically fit into another table.
//					 All these columns are loosely related meaning each row represents a combination of values on a row.
//					 Another characteristic of a junk dimension is that all the columns that have data are part of the key,
//					 therefore there are only inserts into this table.					 
//
*/
			/*
			Load all the service usage jnk rows that are in the staging table.  Since the natural key
			is all of the columns, there is only an insert if there is no match.
			*/

			MERGE 
			INTO BDW.SERVICE_USAGE_JNK_DIM SJD
			USING  (SELECT 
					   [ROLE]
					  ,[PLATFORM_NAM]
					  ,[CONFERENCE_NAM]
					  ,[COMMUNICATION_TYPE_NAM]
					  ,[BRIDGE_ROUTER_COD]
					  ,[CALL_STATE]
					  ,[DIRECTION]
					  ,CHARGE_COUNTRY_COD
					  ,CHARGE_COUNTRY_NAM
					  ,HOST_FLG
					  ,MEDIA_TYPE_NAM
					  ,GATEWAY_ID_COD
					  ,GATEWAY_PREFIX_COD
			  FROM [STG].[SERVICE_USAGE_JNK_STG]
				  ) SJS
			ON SJD.ROLE = SJS.ROLE AND
			   SJD.PLATFORM_NAM = SJS.PLATFORM_NAM AND
			   SJD.CONFERENCE_NAM = SJS.CONFERENCE_NAM AND
			   SJD.COMMUNICATION_TYPE_NAM = SJS.COMMUNICATION_TYPE_NAM AND
			   SJD.BRIDGE_ROUTER_COD = SJS.BRIDGE_ROUTER_COD AND
			   SJD.CALL_STATE = SJS.CALL_STATE AND
			   SJD.DIRECTION = SJS.DIRECTION AND
			   SJD.CHARGE_COUNTRY_COD = SJS.CHARGE_COUNTRY_COD AND
			   SJD.CHARGE_COUNTRY_NAM = SJS.CHARGE_COUNTRY_NAM AND
			   SJD.HOST_FLG = SJS.HOST_FLG AND
			   SJD.MEDIA_TYPE_NAM = SJS.MEDIA_TYPE_NAM AND
			   SJD.GATEWAY_ID_COD = SJS.GATEWAY_ID_COD AND
			   SJD.GATEWAY_PREFIX_COD = SJS.GATEWAY_ID_COD
			WHEN NOT MATCHED THEN
				INSERT ([ROLE],
						[PLATFORM_NAM],
						[CONFERENCE_NAM],
						[COMMUNICATION_TYPE_NAM],
						[BRIDGE_ROUTER_COD],
						[CALL_STATE],
						[DIRECTION],
						AUDIT_UPDATE_IDN,
						AUDIT_CREATE_IDN
					   ,CHARGE_COUNTRY_COD
					   ,CHARGE_COUNTRY_NAM
					   ,HOST_FLG
					   ,MEDIA_TYPE_NAM
					   ,GATEWAY_ID_COD
					   ,GATEWAY_PREFIX_COD)
				VALUES(	SJS.ROLE,
						SJS.PLATFORM_NAM, 
						SJS.CONFERENCE_NAM,
						SJS.COMMUNICATION_TYPE_NAM,
						SJS.BRIDGE_ROUTER_COD,
						SJS.CALL_STATE, 
						SJS.DIRECTION,
						@AUDIT_IDN,
						@AUDIT_IDN
					   ,CHARGE_COUNTRY_COD
					   ,CHARGE_COUNTRY_NAM
					   ,SJS.HOST_FLG
					   ,SJS.MEDIA_TYPE_NAM
					   ,SJS.GATEWAY_ID_COD
					   ,SJS.GATEWAY_PREFIX_COD
					   )
				  ;

		  update statistics BDW.SERVICE_USAGE_JNK_DIM WITH SAMPLE 50 PERCENT, ALL;




GO
/****** Object:  StoredProcedure [BDW].[SERVICE_VENDOR_DIM_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [BDW].[SERVICE_VENDOR_DIM_M](@AUDIT_IDN INT) AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: SERVICE_VENDOR_DIM_M
//		Date: Jan 21, 2015
//		Description: This table contains all the details around the service that produces that data. The Vidyo row
//					 was manually inserted into the table via an insert SQL.
//
*/

			/*
			Insert all new rows from the service vendor staging table.  If there is a match on the company 
			name then update the rest of the columns.  If there is no match, insert the row.
			*/
			MERGE 
			INTO BDW.SERVICE_VENDOR_DIM SVD
			USING  (SELECT 
							COMPANY_NAME,
							[TYPE]
			  FROM [STG].SERVICE_VENDOR_STG
				  ) SVS
			ON SVD.COMPANY_NAME = SVS.COMPANY_NAME AND
			   SVD.TYPE = SVS.TYPE
			WHEN NOT MATCHED THEN
				INSERT (COMPANY_NAME,
						TYPE,
						AUDIT_UPDATE_IDN,
						AUDIT_CREATE_IDN)
				VALUES(	SVS.COMPANY_NAME,
						SVS.TYPE,
						@AUDIT_IDN,
						@AUDIT_IDN)
				  ;

			update statistics BDW.SERVICE_VENDOR_DIM WITH SAMPLE 50 PERCENT, ALL;


GO
/****** Object:  StoredProcedure [dbo].[AirWatchImportRecord]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AirWatchImportRecord]
(
	@DeviceID int,
	@SerialNumber nvarchar(60),
	@MacAddress nvarchar(60),
	@Imei nvarchar(60),
	@AssetNumber nvarchar(60),
	@DeviceFriendlyName nvarchar(60),
	@UserName nvarchar(60),
	@UserEmailAddress nvarchar(60),
	@Ownership nvarchar(60),
	@Platform nvarchar(60),
	@PlatformId int,
	@Model nvarchar(60),
	@ModelId int,
	@OperatingSystem nvarchar(60),
	@PhoneNumber nvarchar(60),
	@LastSeen datetime,
	@EnrollmentStatus nvarchar(60),
	@ComplianceStatus nvarchar(60),
	@CompromisedStatus bit,
	@LastEnrolledOn datetime,
	@LastComplianceCheckOn datetime,
	@LastCompromisedCheckOn datetime,
	@SIMMCC int,
	@CurrentMCC int,
	@CapturedDate datetime
)
AS
	SET NOCOUNT OFF;
INSERT INTO [dbo].[AirWatchImport] ([DeviceID], [SerialNumber], [MacAddress], [Imei], [AssetNumber], [DeviceFriendlyName], [UserName], [UserEmailAddress], [Ownership], [Platform], [PlatformId], [Model], [ModelId], [OperatingSystem], [PhoneNumber], [LastSeen], [EnrollmentStatus], [ComplianceStatus], [CompromisedStatus], [LastEnrolledOn], [LastComplianceCheckOn], [LastCompromisedCheckOn], [SIMMCC], [CurrentMCC], [CapturedDate]) VALUES (@DeviceID, @SerialNumber, @MacAddress, @Imei, @AssetNumber, @DeviceFriendlyName, @UserName, @UserEmailAddress, @Ownership, @Platform, @PlatformId, @Model, @ModelId, @OperatingSystem, @PhoneNumber, @LastSeen, @EnrollmentStatus, @ComplianceStatus, @CompromisedStatus, @LastEnrolledOn, @LastComplianceCheckOn, @LastCompromisedCheckOn, @SIMMCC, @CurrentMCC, @CapturedDate)

GO
/****** Object:  StoredProcedure [dbo].[BCG_SCCM_AddRemoveImportRecord]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[BCG_SCCM_AddRemoveImportRecord]
(
	@ResourceID int,
	@TimeStamp datetime,
	@ProductID nvarchar(255),
	@DisplayName nvarchar(255),
	@InstallDate date,
	@Publisher nvarchar(255),
	@Version nvarchar(255),
	@ComputerName nvarchar(255),
	@CapturedDate datetime
)
AS
	SET NOCOUNT OFF;
INSERT INTO [dbo].[BCG_SCCM_AddRemove] ([ResourceID], [TimeStamp], [ProductID], [DisplayName], [InstallDate], [Publisher], [Version], [ComputerName], [CapturedDate]) VALUES (@ResourceID, @TimeStamp, @ProductID, @DisplayName, @InstallDate, @Publisher, @Version, @ComputerName, @CapturedDate);
	
SELECT Id, ResourceID, TimeStamp, ProductID, DisplayName, InstallDate, Publisher, Version, ComputerName, CapturedDate FROM BCG_SCCM_AddRemove WHERE (Id = SCOPE_IDENTITY())

GO
/****** Object:  StoredProcedure [STG].[APPLICATION_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[APPLICATION_STG_M] as
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: APPLICATION_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes application data from a flat file and creates a record that resembles one that is
//					 inserted/updated into the application dimension.
//
*/

	--Remove all the existing rows in the staging table
	TRUNCATE TABLE STG.APPLICATION_STG;

	/*
	Create the computer dataset by taking unique combinations of columns on the tb_CHACHE_Daily_Details table
	*/
  INSERT INTO STG.APPLICATION_STG(	APPLICATION_NAM,
									PUBLISHER_NAM,
									VERSION_COD)
				(SELECT DisplayName,
						Publisher,
						coalesce([Version],'Undefined')
				  FROM
						STG.BCG_SCCM_STG 
				UNION ALL
				 SELECT distinct
						  ApplicationName,
						 'Undefined' as PUBLISHER_NAM,
						 coalesce(Version,'Undefined')
				from dbo.AirwatchApps
				where applicationName is not null
				)
		EXCEPT
				SELECT APPLICATION_NAM,
						PUBLISHER_NAM,
						VERSION_COD
					FROM [BDW].[APPLICATION_DIM] AD
				INNER JOIN BDW.PUBLISHER_DIM PD
				    ON PD.PUBLISHER_IDN = PD.PUBLISHER_IDN




GO
/****** Object:  StoredProcedure [STG].[COMP_HARDWARE_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[COMP_HARDWARE_STG_M] as
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: COMP_HARDWARE_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes data from the systemcheck database and creates a record that resembles one that is
//					 inserted/updated into the computer dimension.
//
*/

	--Remove all the existing rows in the staging table
	TRUNCATE TABLE STG.COMP_HARDWARE_STG;

	/*
	Create the computer dataset by taking unique combinations of columns on the tb_CHACHE_Daily_Details table
	*/
  INSERT INTO STG.COMP_HARDWARE_STG
			Select distinct 
				coalesce(build_image_model,'Undefined'),
				coalesce(Build_hard_drive_model,'Undefined'),
				coalesce(asset_computer_model,'Undefined'),
				coalesce(build_windows_architecture,'Undefined'),
				coalesce(asset_processor_model,'Undefined'),
				coalesce(asset_video_model,'Undefined'),
				coalesce(asset_wired_nic_model,'Undefined'),
				coalesce(asset_wireless_nic_model,'Undefined'),
				build_computer_name,
				build_status
			FROM STG.[tb_CACHE_Daily_Details_STG]
		UNION
			Select distinct 
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					ams.[NetBIOS Name],
					'Undefined'
			   from stg.App_Metering_STG ams
			LEFT JOIN stg.tb_cache_daily_details_stg dd
			  on ams.[NetBIOS Name] = dd.build_computer_name
			where dd.build_computer_name is null 
		UNION 
			SELECT 'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined'			
		EXCEPT
		SELECT [IMAGE_MODEL_DESC]
			  ,[HD_MODEL_DESC]
			  ,[COMP_MODEL_DESC]
			  ,[WINDOWS_ARCH_DESC]
			  ,[PROCESSOR_MODEL_DESC]
			  ,[VIDEO_MODEL_DESC]
			  ,[WIRED_NIC_MODEL_DESC]
			  ,[WIRELESS_NIC_MODEL_DESC]
			  ,NAME
			  ,[STATUS]
		  FROM [BDW].[COMP_HARDWARE_DIM];




GO
/****** Object:  StoredProcedure [STG].[MOBILE_DEVICE_JNK_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[MOBILE_DEVICE_JNK_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: MOBILE_DEVICE_JNK_STG_M
//		Date: March 11, 2015
//		Description: This proc takes all the codes and status info from the airwatch devices and inserts it into this staging table
//					 if the record doesn't currently exist in the dimension table.
//
*/

	--Purge all rows from the staging table
	TRUNCATE TABLE STG.MOBILE_DEVICE_JNK_STG;

		INSERT INTO STG.MOBILE_DEVICE_JNK_STG(	DEVICE_OWNERSHIP,
												ENROLLMENT_STATUS,
												COMPLIANCE_STATUS,
												COMPROMISED_STATUS,
												APP_ISMANAGED)
					Select  distinct
							a.Ownership,
							a.EnrollmentStatus,
							a.ComplianceStatus,
							a.CompromisedStatus,
							coalesce(aa.IsManaged,'Undefined') as isManaged
							--Add isManaged from AirwatchApps
					  from STG.Airwatch_STG a 
				 LEFT JOIN dbo.airwatchApps aa
						on a.udid = aa.udid
		except
				SELECT
					   [DEVICE_OWNERSHIP]
					  ,[ENROLLMENT_STATUS]
					  ,[COMPLIANCE_STATUS]
					  ,[COMPROMISED_STATUS]
					  ,[APP_ISMANAGED]
				  FROM [BDW].[MOBILE_DEVICE_JNK_DIM]


GO
/****** Object:  StoredProcedure [STG].[MOBILE_DEVICE_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[MOBILE_DEVICE_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: MOBILE_DEVICE_STG
//		Date: March 11, 2015
//		Description: This proc takes all the info from the airwatch devices and inserts it into this staging table
//					 if the record doesn't currently exist in the dimension table.
//
*/

	--Purge all rows from the staging table
	TRUNCATE TABLE STG.MOBILE_DEVICE_STG;
	/*
	Insert new MeetMe/Webex and Vidyo rows into the staging table that dont have a matching dimensional row
	*/
	INSERT INTO STG.MOBILE_DEVICE_STG ( PLATFORM_NAM,
										MODEL_NAM,
										DEVICE_NAM,
										OS_VERSION_NAM)
				Select  DISTINCT
						Platform,
						model,
						[device model],
						OperatingSystem
				  from STG.Airwatch_STG
		except
			SELECT [PLATFORM_NAM]
				  ,[MODEL_NAM]
				  ,[DEVICE_NAM]
				  ,[OS_VERSION_NAM]
			  FROM [BDW].[MOBILE_DEVICE_DIM]




GO
/****** Object:  StoredProcedure [STG].[OFFICE_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[OFFICE_STG_M] as
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: OFFICE_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes data from the systemcheck database and creates a record that resembles one that is
//					 inserted/updated into the office dimension.
//
*/

	--Remove all the existing rows in the staging table
	TRUNCATE TABLE STG.OFFICE_STG;

	/*
	Create the office dataset by joining office, region, new region, country and office system.
	Only insert the rows that are different from those that are in the office dimension.  Also,
	insert an undefined row that is used to tie people without a valid office.
	*/
	INSERT INTO STG.OFFICE_STG(
							OFFICE_COD, 
							OFFICE_NAM, 
							REGION_COD_NEW,
							REGION_COD_OLD,
							COUNTRY_NAM, 
							SYSTEM_NAM)
			Select ro.office_short_name,
				   coalesce(ro.office_long_name,'Undefined'),
				   coalesce(rn.new_region,'Undefined') as new_region,
				   r.region_short_name as old_region,
				   c.country,
				   coalesce(os.system_long_name,'Undefined')
			  from STG.tb_Ref_Office_STG ro
		 LEFT JOIN STG.tb_Ref_Region_New_STG rn
				on ro.region_id = rn.new_region_id
		 LEFT JOIN STG.tb_Ref_Region_STG r
				on ro.region_id = r.region_id
		 INNER JOIN STG.tb_Ref_Country_STG c
				on ro.country_id = c.country_id
		 LEFT JOIN STG.tb_Ref_Office_system_STG os
				on ro.office_system_id = os.system_id
		UNION 
			SELECT 'N/A','Undefined','Undefined','Undefined','Undefined','Undefined'
			
		EXCEPT
			SELECT 	OFFICE_COD, 
					OFFICE_NAM, 
					REGION_COD_NEW,
					REGION_COD_OLD,
					COUNTRY_NAM, 
					SYSTEM_NAM
			  FROM  BDW.OFFICE_DIM
			  ;



GO
/****** Object:  StoredProcedure [STG].[PARTICIPANT_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[PARTICIPANT_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: PARTICIPANT_STG_M
//		Date: Jan 21, 2015
//		Description: This table takes in participant data from the vidyo and MeetMe/Webex file, as well as existing
//					 user data from the systemcheck database.  Only users that have an entry in one of these files
//					 are grabbed from thte systemcheck database.
//
*/
		CREATE TABLE #LATEST_REC 
		(email_address  VARCHAR(50),
		 user_header_id BIGINT,
		 rec_date		DATE);

		CREATE TABLE #MATCHING_RECS 
		(email_address VARCHAR(50));

		--Grab the latest employee row
		INSERT 
		  INTO #LATEST_REC
	   			Select cast(email_address as varchar(50)) as email_address,
						max(user_header_id) as user_header_id,
						min(date) as rec_date
				  from STG.tb_Working_Data_User_Header_STG
				 where 1=1
				   and coalesce(employee_type,'Undef') <> 'InvalidHRID'
			  group by email_address
			  having email_address is not null;

			  print 'Latest rec populated';
	CREATE UNIQUE INDEX LATEST_REC_IDX_UNQ01 on #LATEST_REC(email_address); 
	CREATE UNIQUE INDEX LATEST_REC_IDX_UNQ02 on #LATEST_REC(email_address,user_header_id); 
	
			 print 'Lastest rec indexes created'
		INSERT
		  INTO #MATCHING_RECS
						SELECT cast(lr.email_address as varchar(50))
						  FROM 
								(		Select 
												CASE ltrim(rtrim(email)) 
												  WHEN '' then 'Undefined'
												  ELSE email
												end as EMAIL
											from STG.MeetMe_Webex
										UNION 
 										SELECT 
												cast(CASE
												  WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
													THEN CallerID 
												ELSE 'Undefined'
												END as VARCHAR(50)) as EMAIL
											FROM STG.VIDYO_STG
											) pd
						 INNER JOIN #LATEST_REC lr
								 ON pd.email = lr.email_address;				

		print 'Matching rec table loaded';

		CREATE UNIQUE INDEX MATCHING_RECS_IDX_UNQ01 on #MATCHING_RECS(email_address); 
		--Remove all old participant rows from the staging table
		TRUNCATE TABLE STG.PARTICIPANT_STG;

		/*
		Use the temporary tables that identify the latest rec and only emps with data in Vidyo and Meetme/Webex datasets.
		Create the Participant record with data from the systemcheck database if the corresponding emails match.  Only
		grab the participants rows that do not match the dimensional row
		*/	
				INSERT INTO STG.PARTICIPANT_STG
								(EMPLOYEE_ID,
								FIRST_NAM,
								LAST_NAM,
								FULL_NAM, 
								OFFICE_COD, 
								JOB_TITLE_NAM, 
								EMAIL,
								JOB_NAM,
								ASSISTANT_NAM,
								HIRE_DATE,
								TERMINATE_DATE,
								COHORT_NAM,
								VIDYO_INSTLD_IND,
								WEBEX_INSTLD_IND,
								LYNC_INSTLD_IND,
								REC_START_DATE,
								REC_END_DATE,
								EMPLOYEE_TYPE_NAM,
								DEPARTMENT_NAM)
				SELECT 
					   uh.employee_id,
					   uh.first_name,
					   uh.last_name,
					   uh.display_name,
					   uh.host_office,
					   CASE	
					     WHEN uh.title IS NULL then 'Undefined'
						 ELSE uh.title
						END as TITLE,
					   uh.email_address,
					   'Undefined',
					   CASE
							WHEN uh.assistant is null then 'Undefined'
							ELSE uh.assistant
						END as ASSISTANT,
					   coalesce(uh.start_date,'12-31-9999') as START_DATE,
					   CASE
					     WHEN uh.term_date is NULL then '12-31-9999'
						 ELSE uh.term_date
						END as TERM_DATE,
					   CASE
						WHEN c.cohort is NULL then 'Undefined'
						ELSE c.cohort
						END as COHORT,
					   '?',
					   '?',
					   '?',
					   lr.[rec_date] as EFF_START_DATE,
 					   coalesce(dateadd (day,- 1,lead (lr.[rec_date]) over ( partition by uh.employee_id order by lr.[rec_date])), '12-31-9999') as EFF_END_DATE,
					   coalesce(uh.employee_type,'Undefined') as EMPLOYEE_TYPE_NAM,
					   coalesce(uh.department ,'Undefined') as DEPARTMENT_NAM
						 FROM STG.tb_Working_Data_User_Header_STG uh 
/*				   INNER JOIN #MATCHING_RECS mr
						   ON lower(uh.email_address) = mr.email_address
*/
			       INNER JOIN #LATEST_REC lr
						   ON uh.user_header_id = lr.user_header_id
--						   ON uh.email_address = lr.email_address
--						  AND uh.[Date] = lr.LATEST_REC_DATE
--						  and coalesce(uh.employee_type,'Undef') != 'InvalidHRID'
--						  and uh.is_most_recent = 1
				    LEFT JOIN STG.tb_Ref_Cohort_STG c 
						   on uh.Department = c.Department
/*				except
					SELECT
							  EMPLOYEE_ID
							  ,[FIRST_NAM]
							  ,[LAST_NAM]
							  ,[FULL_NAM]
							  ,[OFFICE_COD]
							  ,[JOB_TITLE_NAM]
							  ,[EMAIL]
							  ,[JOB_NAM]
							  ,[ASSISTANT_NAM]
							  ,[HIRE_DATE]
							  ,[TERMINATE_DATE]
							  ,[COHORT_NAM]
							  ,[VIDYO_INSTLD_IND]
							  ,[WEBEX_INSTLD_IND]
							  ,[LYNC_INSTLD_IND]
					 FROM [BDW].[PARTICIPANT_DIM]	
					;
*/		;
			/*
			 Insert all rows that dont have a matching email in the Vidyo and Meetme/Webex dataset.  Create
			 a generic row.
			*/		
				INSERT INTO STG.PARTICIPANT_STG
								(EMPLOYEE_ID,
								FIRST_NAM,
								LAST_NAM,
								FULL_NAM, 
								OFFICE_COD, 
								JOB_TITLE_NAM, 
								EMAIL,
								JOB_NAM,
								ASSISTANT_NAM,
								HIRE_DATE,
								TERMINATE_DATE,
								COHORT_NAM,
								VIDYO_INSTLD_IND,
								WEBEX_INSTLD_IND,
								LYNC_INSTLD_IND,
								REC_START_DATE,
								REC_END_DATE,
								EMPLOYEE_TYPE_NAM,
								DEPARTMENT_NAM)
			SELECT distinct
					-1 as EMPLOYEE_ID,
					CASE
						WHEN charindex(',',CallerName) > 0 THEN SUBSTRING(CallerName,CHARINDEX(',',CallerName)+1,len(CallerName))
						ELSE CallerName
					END as FIRST_NAM,
					CASE
						WHEN charindex(',',CallerName) > 0 THEN SUBSTRING(CallerName,1,CHARINDEX(',',CallerName)-1)
						ELSE CallerName
					END as LAST_NAM,
					CallerName as FULL_NAM,
					'N/A' as OFFICE_COD,
					'Undefined' as JOB_TITLE_NAM,
					 EMail,
					'Undefined' as JOB_NAM,
					'Undefined' as ASSISTANT_NAM,
					'12-31-9999' as HIRE_DATE,
					'12-31-9999' as TERMINATE_DATE,
					'Undefined' as COHORT_NAM,
					'?' as VIDYO_INSTLD_IND,
					'?' as WEBEX_INSTLD_IND,
					'?' as LYNC_INSTLD_IND,
					'01-01-2010' as EFF_START_DATE,
					'12-31-9999' as EFF_END_DATE,
					'Undefined' as EMPLOYEE_TYPE_NAM,
					'Undefined' as DEPARTMENT_NAM		
			  FROM (	Select distinct
									   CASE
										WHEN cast(EMAIL as varchar(50)) <> 'Undefined' THEN
											FIRST_VALUE(name) OVER (partition by cast(EMAIL as varchar(50)) order by [End Time] desc)
										 ELSE Name
										END as CallerName,
									   cast(EMAIL as varchar(50)) as EMAIL
								FROM
								(		Select 
												Name,
												CASE rtrim(ltrim(email))
												  WHEN '' then 'Undefined'
												  ELSE ltrim(rtrim(email))
												end as EMAIL,
												[End Time]
											from STG.MeetMe_Webex
										UNION 
 										SELECT 
												CallerName as CallerName,
												CASE
												  WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
													THEN ltrim(rtrim(CallerID))
												ELSE 'Undefined'
												END as EMAIL,
												LeaveTime
											FROM STG.VIDYO_STG
											) b )a
      LEFT JOIN #MATCHING_RECS u
			 ON a.EMail = u.email_address
		   WHERE u.email_address is null  
/*		except
			SELECT
					  EMPLOYEE_ID
					  ,[FIRST_NAM]
					  ,[LAST_NAM]
					  ,[FULL_NAM]
					  ,[OFFICE_COD]
					  ,[JOB_TITLE_NAM]
					  ,[EMAIL]
					  ,[JOB_NAM]
					  ,[ASSISTANT_NAM]
					  ,[HIRE_DATE]
					  ,[TERMINATE_DATE]
					  ,[COHORT_NAM]
					  ,[VIDYO_INSTLD_IND]
					  ,[WEBEX_INSTLD_IND]
					  ,[LYNC_INSTLD_IND]
		     FROM [BDW].[PARTICIPANT_DIM]		 
			 ;
*/







GO
/****** Object:  StoredProcedure [STG].[PARTICIPANT_STG_M_OLD]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[PARTICIPANT_STG_M_OLD] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: PARTICIPANT_STG_M
//		Date: Jan 21, 2015
//		Description: This table takes in participant data from the vidyo and MeetMe/Webex file, as well as existing
//					 user data from the systemcheck database.  Only users that have an entry in one of these files
//					 are grabbed from thte systemcheck database.
//
*/
		CREATE TABLE #LATEST_REC 
		(email_address  VARCHAR(50),
		 LATEST_REC_DATE DATETIME);

		CREATE TABLE #MATCHING_RECS 
		(email_address VARCHAR(50));

		--Grab the latest employee row
		INSERT 
		  INTO #LATEST_REC
	   			Select lower(email_address),
						max(date)
				  from STG.tb_Working_Data_User_Header_STG
				 where is_most_recent = 1
			  group by lower(email_address);

		--Only select for employees that have a record in Vidyo or Meetme/Webex datasets
		INSERT
		  INTO #MATCHING_RECS
						SELECT u.email_address
						  FROM 
							(Select email
							   from stg.MeetMe_Webex
							UNION
							 Select CallerID
							   from STG.VIDYO_STG) pd
						 INNER JOIN #LATEST_REC u
								 ON lower(pd.email) = u.email_address;				

		--Remove all old participant rows from the staging table
		TRUNCATE TABLE STG.PARTICIPANT_STG;

		/*
		Use the temporary tables that identify the latest rec and only emps with data in Vidyo and Meetme/Webex datasets.
		Create the Participant record with data from the systemcheck database if the corresponding emails match.  Only
		grab the participants rows that do not match the dimensional row
		*/	
				INSERT INTO STG.PARTICIPANT_STG
								(HRID,
								FIRST_NAM,
								MIDDLE_NAM,
								LAST_NAM,
								FULL_NAM, 
								COMPANY_NAM, 
								OFFICE_COD, 
								JOB_TITLE_NAM, 
								EMAIL,
								JOB_NAM,
								ASSISTANT_NAM,
								HIRE_DATE,
								TERMINATE_DATE,
								COHORT_NAM,
								VIDYO_INSTLD_IND,
								WEBEX_INSTLD_IND,
								LYNC_INSTLD_IND)
				SELECT 
					   uh.employee_id,
					   uh.first_name,
					   null,
					   uh.last_name,
					   uh.display_name,
					   '',
					   uh.host_office,
					   CASE	
					     WHEN uh.title IS NULL then 'Undefined'
						 ELSE uh.title
						END as TITLE,
					   uh.email_address,
					   '',
					   CASE
							WHEN uh.assistant is null then 'Undefined'
							ELSE uh.assistant
						END as ASSISTANT,
					   uh.start_date,
					   CASE
					     WHEN uh.term_date is NULL then '12-31-9999'
						 ELSE uh.term_date
						END as TERM_DATE,
					   CASE
						WHEN c.cohort is NULL then 'Undefined'
						ELSE c.cohort
						END as COHORT,
					   '?',
					   '?',
					   '?'
						 FROM STG.tb_Working_Data_User_Header_STG uh 
				   INNER JOIN #MATCHING_RECS mr
						   ON lower(uh.email_address) = mr.email_address
			       INNER JOIN #LATEST_REC lr
						   ON lower(uh.email_address) = lr.email_address
						  AND uh.[Date] = lr.LATEST_REC_DATE
				    LEFT JOIN STG.tb_Ref_Cohort_STG c 
						   on uh.Department = c.Department
				except
					SELECT
							  [HRID]
							  ,[FIRST_NAM]
							  ,[MIDDLE_NAM]
							  ,[LAST_NAM]
							  ,[FULL_NAM]
							  ,[COMPANY_NAM]
							  ,[OFFICE_COD]
							  ,[JOB_TITLE_NAM]
							  ,[EMAIL]
							  ,[JOB_NAM]
							  ,[ASSISTANT_NAM]
							  ,[HIRE_DATE]
							  ,[TERMINATE_DATE]
							  ,[COHORT_NAM]
							  ,[VIDYO_INSTLD_IND]
							  ,[WEBEX_INSTLD_IND]
							  ,[LYNC_INSTLD_IND]
					 FROM [BDW].[PARTICIPANT_DIM]	
					;

			/*
			 Insert all rows that dont have a matching email in the Vidyo and Meetme/Webex dataset.  Create
			 a generic row.
			*/		
				INSERT INTO STG.PARTICIPANT_STG
								(HRID,
								FIRST_NAM,
								MIDDLE_NAM,
								LAST_NAM,
								FULL_NAM, 
								COMPANY_NAM, 
								OFFICE_COD, 
								JOB_TITLE_NAM, 
								EMAIL,
								JOB_NAM,
								ASSISTANT_NAM,
								HIRE_DATE,
								TERMINATE_DATE,
								COHORT_NAM,
								VIDYO_INSTLD_IND,
								WEBEX_INSTLD_IND,
								LYNC_INSTLD_IND)
			SELECT distinct
					0 as HRID,
					CASE
						WHEN charindex(',',CallerName) > 0 THEN SUBSTRING(CallerName,CHARINDEX(',',CallerName)+1,len(CallerName))
						ELSE CallerName
					END as FIRST_NAM,
					null as MIDDLE_NAM,
					CASE
						WHEN charindex(',',CallerName) > 0 THEN SUBSTRING(CallerName,1,CHARINDEX(',',CallerName)-1)
						ELSE CallerName
					END as LAST_NAM,
					CallerName as FULL_NAM,
					'Undefined' as COMPANY_NAM,
					'N/A' as OFFICE_COD,
					'Undefined' as JOB_TITLE_NAM,
					 EMail,
					'Undefined' as JOB_NAM,
					'Undefined' as ASSISTANT_NAM,
					'12-31-9999' as HIRE_DATE,
					'12-31-9999' as TERMINATE_DATE,
					'Undefined' as COHORT_NAM,
					'?' as VIDYO_INSTLD_IND,
					'?' as WEBEX_INSTLD_IND,
					'?' as LYNC_INSTLD_IND		
			  FROM (		Select 
								  name as CallerName,
								  CASE rtrim(ltrim(email))
									WHEN '' then 'Undefined'
									ELSE email
								  end as EMAIL
							 from STG.MeetMe_Webex
							UNION
 							SELECT 
									CallerName as CallerName,
								  CASE
									WHEN charindex('@',CallerID)>0 AND charindex('@',CallerID) is not null
										--THEN coalesce(FIRST_VALUE(callerID) over(partition by CallerName order by convert(datetime,jointime) desc),'Undefined')
										THEN CallerID
									ELSE 'Undefined'
									END 	    as EMAIL
							 FROM STG.VIDYO_STG) a
      LEFT JOIN #MATCHING_RECS u
			 ON a.EMail = u.email_address
		  WHERE u.email_address is null 
		except
			SELECT
					  [HRID]
					  ,[FIRST_NAM]
					  ,[MIDDLE_NAM]
					  ,[LAST_NAM]
					  ,[FULL_NAM]
					  ,[COMPANY_NAM]
					  ,[OFFICE_COD]
					  ,[JOB_TITLE_NAM]
					  ,[EMAIL]
					  ,[JOB_NAM]
					  ,[ASSISTANT_NAM]
					  ,[HIRE_DATE]
					  ,[TERMINATE_DATE]
					  ,[COHORT_NAM]
					  ,[VIDYO_INSTLD_IND]
					  ,[WEBEX_INSTLD_IND]
					  ,[LYNC_INSTLD_IND]
		     FROM [BDW].[PARTICIPANT_DIM]		 
			 ;







GO
/****** Object:  StoredProcedure [STG].[RESET_ENV]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[RESET_ENV]
as
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: RESET_ENV
//		Date: Jan 21, 2015
//		Description: This proc is NOT part of the stardard run.  It should ONLY be used for clearing out all the data
//					 in the solution.  It is useful for testing when code changes are made and want the whole solution
//					 rebuilt.
//
*/

		/*
			This proc is used to clear out all data and reset all sequences for testing purposes.  On a deployment
			to Production this would never want to be run.
		*/

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK];
	

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK;

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK];

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] 
			DROP CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_CHAIRPERSON_FK];

			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC
			DROP CONSTRAINT COMP_APP_FAC_PARTICIPANT_USER_FK;

			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC
			DROP CONSTRAINT [COMP_APP_FAC_COMP_HRDWRE_DIM_FK];

			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC
			DROP CONSTRAINT COMP_APP_FAC_APP_DIM_FK;

			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC
			DROP CONSTRAINT [COMP_APP_FAC_OFFICE_DIM_FK];

			ALTER TABLE BDW.APP_METERING_FAC
			DROP CONSTRAINT [APP_MET_FAC_MONTH_DIM_FK];

			ALTER TABLE BDW.APP_METERING_FAC
			DROP CONSTRAINT APP_MET_FAC_COMP_HRDWRE_DIM_FK;

			ALTER TABLE BDW.APP_METERING_FAC
			DROP CONSTRAINT APP_MET_FAC_PARTICIPANT_DIM_FK;

			ALTER TABLE BDW.APP_METERING_FAC
			DROP CONSTRAINT APP_MET_FAC_OFFICE_DIM_FK;

			ALTER TABLE BDW.APP_METERING_FAC
			DROP CONSTRAINT [APP_MET_FAC_PUBLISHER_DIM_FK];

			ALTER TABLE BDW.APP_METERING_FAC
			DROP CONSTRAINT [APP_MET_FAC_LSTUSG_DATE_DIM_FK];

			ALTER TABLE BDW.APPLICATION_DIM
			DROP CONSTRAINT [APP_DIM_PUBLISHER_DIM_FK];

			ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC 
			DROP CONSTRAINT MOBILE_DEV_APP_FAC_MOB_DEV_DIM_FK;

			ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC 
			DROP CONSTRAINT MOBILE_DEV_APP_FAC_MOB_DEV_JNK_DIM_FK;

			ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC 
			DROP CONSTRAINT MOBILE_DEV_APP_FAC_PARTICIPANT_DIM_FK;

			ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC 
			DROP CONSTRAINT MOBILE_DEV_APP_FAC_APPLICATION_DIM_FK;




			TRUNCATE TABLE BDW.MOBILE_DEVICE_DIM;
			TRUNCATE table bdw.MOBILE_DEVICE_JNK_DIM;
			TRUNCATE TABLE BDW.APPLICATION_DIM;
			TRUNCATE TABLE BDW.PUBLISHER_DIM;
			TRUNCATE TABLE BDW.COMP_HARDWARE_DIM;
			TRUNCATE TABLE BDW.PARTICIPANT_DIM;
			TRUNCATE TABLE BDW.SERVICE_USAGE_JNK_DIM;
			TRUNCATE TABLE BDW.SERVICE_USAGE_FAC;
			TRUNCATE TABLE BDW.COMPUTER_APPLICATION_FAC;
			TRUNCATE TABLE BDW.APP_METERING_FAC;
			TRUNCATE TABLE BDW.MOBILE_DEVICE_APP_FAC;
			TRUNCATE TABLE BDW.RUN_AUDIT;
			TRUNCATE TABLE BDW.OFFICE_DIM;

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK] FOREIGN KEY([PARTICIPANT_IDN])
			REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
			
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK]

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK] FOREIGN KEY([SERVICE_USAGE_JNK_IDN])
			REFERENCES [BDW].[SERVICE_USAGE_JNK_DIM] ([SERVICE_USAGE_JNK_IDN])
			
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK]
			
			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK] FOREIGN KEY([OFFICE_IDN])
			REFERENCES [BDW].OFFICE_DIM (OFFICE_IDN)

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK];


			ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT SERVICE_USAGE_FAC_PARTICIPANT_CHAIRPERSON_FK FOREIGN KEY(CHAIRPERSON_IDN)
			REFERENCES [BDW].PARTICIPANT_DIM (PARTICIPANT_IDN)

			ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT SERVICE_USAGE_FAC_PARTICIPANT_CHAIRPERSON_FK;

------  COMPUTER_APPLICATION_FAC

			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC WITH CHECK ADD CONSTRAINT COMP_APP_FAC_PARTICIPANT_USER_FK FOREIGN KEY(USER_IDN)
			REFERENCES BDW.PARTICIPANT_DIM(PARTICIPANT_IDN);

			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC CHECK CONSTRAINT COMP_APP_FAC_PARTICIPANT_USER_FK;

			ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_COMP_HRDWRE_DIM_FK] FOREIGN KEY([COMP_HARDWARE_IDN])
			REFERENCES [BDW].[COMP_HARDWARE_DIM] ([COMP_HARDWARE_IDN])

			ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_COMP_HRDWRE_DIM_FK]


			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC WITH CHECK ADD CONSTRAINT COMP_APP_FAC_APP_DIM_FK FOREIGN KEY(APPLICATION_IDN)
			REFERENCES BDW.APPLICATION_DIM(APPLICATION_IDN);
			
			ALTER TABLE BDW.COMPUTER_APPLICATION_FAC CHECK CONSTRAINT COMP_APP_FAC_APP_DIM_FK;

			ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_OFFICE_DIM_FK] FOREIGN KEY([OFFICE_IDN])
			REFERENCES [BDW].[OFFICE_DIM] ([OFFICE_IDN])
			
			ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_OFFICE_DIM_FK]

			ALTER TABLE [BDW].APPLICATION_DIM WITH CHECK ADD  CONSTRAINT [APP_DIM_PUBLISHER_DIM_FK] FOREIGN KEY(PUBLISHER_IDN)
			REFERENCES [BDW].PUBLISHER_DIM([PUBLISHER_IDN]);
			
			ALTER TABLE [BDW].APPLICATION_DIM CHECK CONSTRAINT [APP_DIM_PUBLISHER_DIM_FK];
			
--			APP_METERING_FAC

				ALTER TABLE [BDW].APP_METERING_FAC  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_MONTH_DIM_FK] FOREIGN KEY(MONTH_IDN)
				REFERENCES [BDW].MONTH_DIM (MONTH_IDN)
				
				ALTER TABLE [BDW].APP_METERING_FAC CHECK CONSTRAINT [APP_MET_FAC_MONTH_DIM_FK]
				

				ALTER TABLE [BDW].APP_METERING_FAC  WITH CHECK ADD  CONSTRAINT APP_MET_FAC_COMP_HRDWRE_DIM_FK FOREIGN KEY(COMP_HARDWARE_IDN)
				REFERENCES [BDW].COMP_HARDWARE_DIM (COMP_HARDWARE_IDN)
				
				ALTER TABLE [BDW].APP_METERING_FAC CHECK CONSTRAINT APP_MET_FAC_COMP_HRDWRE_DIM_FK
				

				ALTER TABLE [BDW].APP_METERING_FAC  WITH CHECK ADD  CONSTRAINT APP_MET_FAC_PARTICIPANT_DIM_FK FOREIGN KEY(PARTICIPANT_IDN)
				REFERENCES [BDW].PARTICIPANT_DIM (PARTICIPANT_IDN)
				
				ALTER TABLE [BDW].APP_METERING_FAC CHECK CONSTRAINT APP_MET_FAC_PARTICIPANT_DIM_FK
				

				ALTER TABLE [BDW].APP_METERING_FAC  WITH CHECK ADD  CONSTRAINT APP_MET_FAC_OFFICE_DIM_FK FOREIGN KEY(OFFICE_IDN)
				REFERENCES [BDW].OFFICE_DIM ([OFFICE_IDN])				

				ALTER TABLE [BDW].APP_METERING_FAC CHECK CONSTRAINT APP_MET_FAC_OFFICE_DIM_FK


				ALTER TABLE [BDW].APP_METERING_FAC  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_PUBLISHER_DIM_FK] FOREIGN KEY(PUBLISHER_IDN)
				REFERENCES [BDW].PUBLISHER_DIM ([PUBLISHER_IDN])

				ALTER TABLE [BDW].APP_METERING_FAC CHECK CONSTRAINT [APP_MET_FAC_PUBLISHER_DIM_FK]


				ALTER TABLE [BDW].APP_METERING_FAC  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_LSTUSG_DATE_DIM_FK] FOREIGN KEY(LAST_USAGE_DATE_IDN)
				REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])

				ALTER TABLE [BDW].APP_METERING_FAC CHECK CONSTRAINT [APP_MET_FAC_LSTUSG_DATE_DIM_FK]
----

				ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC WITH CHECK ADD CONSTRAINT MOBILE_DEV_APP_FAC_MOB_DEV_DIM_FK FOREIGN KEY(MOBILE_DEVICE_IDN) 
					REFERENCES BDW.MOBILE_DEVICE_DIM(MOBILE_DEVICE_IDN); 

				ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC WITH CHECK ADD CONSTRAINT MOBILE_DEV_APP_FAC_MOB_DEV_JNK_DIM_FK FOREIGN KEY(MOBILE_DEVICE_JNK_IDN) 
					REFERENCES BDW.MOBILE_DEVICE_JNK_DIM(MOBILE_DEVICE_JNK_IDN); 

				ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC WITH CHECK ADD CONSTRAINT MOBILE_DEV_APP_FAC_PARTICIPANT_DIM_FK FOREIGN KEY(EMPLOYEE_IDN) 
					REFERENCES BDW.PARTICIPANT_DIM(PARTICIPANT_IDN); 

				ALTER TABLE BDW.MOBILE_DEVICE_APP_FAC WITH CHECK ADD CONSTRAINT MOBILE_DEV_APP_FAC_APPLICATION_DIM_FK FOREIGN KEY(APPLICATION_IDN) 
					REFERENCES BDW.APPLICATION_DIM(APPLICATION_IDN); 

--
			ALTER SEQUENCE BDW.MOBILE_DEVICE_IDN_S
			RESTART;
			
			ALTER SEQUENCE BDW.MOBILE_DEVICE_JNK_IDN_S
			RESTART;
			
			ALTER SEQUENCE [BDW].[RUN_AUDIT_IDN_S] 
			 RESTART
			
			ALTER SEQUENCE [BDW].PARTICIPANT_IDN_S
			 RESTART
			
			ALTER SEQUENCE [BDW].SERVICE_USAGE_JNK_IDN_S
			 RESTART

			ALTER SEQUENCE BDW.OFFICE_IDN_S
			  RESTART

			ALTER SEQUENCE BDW.COMP_HARDWARE_IDN_S
			 RESTART;

			ALTER SEQUENCE BDW.APPLICATION_IDN_S
			 RESTART;

			ALTER SEQUENCE BDW.PUBLISHER_IDN_S
			 RESTART;

			INSERT INTO BDW.PARTICIPANT_DIM
			VALUES(0,0,'Undefined','Undefined','Undefined','N/A','Undefined','Undefined','Undefined','Undefined','12-31-9999','12-31-9999',
					'Undefined','?','?','?',0,0,'01-01-2010','12-31-9999','Undefined','Undefined');

			INSERT 
			  INTO BDW.SERVICE_USAGE_JNK_DIM
			  values(0,'Undefined','Undefined','Undefined',0,0,'Undefined','Undefined','Undefined','Undefined','Undefined','Undefined','?','Undefined','Undefined','Undefined');

			INSERT
			  INTO BDW.PUBLISHER_DIM(PUBLISHER_IDN,
									 PUBLISHER_NAM,
									 AUDIT_CREATE_IDN,
									 AUDIT_UPDATE_IDN)
							VALUES(0,
									'Undefined',
									0,
									0);


			  INSERT
			    INTO BDW.APPLICATION_DIM(APPLICATION_IDN, 
										APPLICATION_NAM,
										AUDIT_CREATE_IDN,
										AUDIT_UPDATE_IDN, 
										VERSION_COD,
										PUBLISHER_IDN)
							VALUES( 0,
									'No Application',
									0,
									0,
									'Undefined',
									0);



GO
/****** Object:  StoredProcedure [STG].[SERVICE_USAGE_JNK_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[SERVICE_USAGE_JNK_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: SERVICE_USAGE_JNK_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes all codes/status from the vidyo and Meetme/Webex datasets and enters them into
//					 this table if they dont currently exist in the dimension table.
//
*/

	--Purge all rows from the staging table
	TRUNCATE TABLE STG.SERVICE_USAGE_JNK_STG;
	/*
	Insert new MeetMe/Webex and Vidyo rows into the staging table that dont have a matching dimensional row
	*/
	INSERT INTO STG.SERVICE_USAGE_JNK_STG (
								[ROLE],
								CALL_STATE, 
								CONFERENCE_NAM,
								COMMUNICATION_TYPE_NAM,
								PLATFORM_NAM,
								DIRECTION,
								BRIDGE_ROUTER_COD,
								CHARGE_COUNTRY_COD,
								CHARGE_COUNTRY_NAM,
								HOST_FLG,
								MEDIA_TYPE_NAM,
								GATEWAY_ID_COD,
								GATEWAY_PREFIX_COD
								)
			(   Select distinct
				   'Undefined' as [ROLE],
				   cd.CallState as CALL_STATE,
				   cd.[ConferenceName] as CONFERENCE_NAM,
				   conf.value as COMMUNICATION_TYPE_NAM,
				   endp.value as PLATFORM_NAM,
				   dir.value as DIRECTION,
				   CASE  
						WHEN rou.value is null then 'Undefined'
						ELSE rou.value
				   END	 as RouterID,
   				   'Undefined' as CHARGE_COUNTRY_COD,
				   'Undefined' as CHARGE_COUNTRY_NAM,
					CASE
				   		WHEN ConferenceName like '%'+CallerID+'%' AND ConferenceType != 'D' THEN 'Y'
						ELSE 'N'
					END as HOST_FLG,
					'Video' as MEDIA_TYPE_NAM,
					CASE 
					  WHEN cd.gwid ='' THEN 'Undefined'
					  WHEN cd.gwid is null THEN 'Undefined'
					  ELSE cd.gwid 
					 END as GATEWAY_ID_COD,
					CASE
					 WHEN cd.gwprefix = '' THEN 'Undefined'
					 WHEN cd.gwprefix is null THEN 'Undefined'
					 ELSE cd.gwprefix
					END AS GATEWAY_PREFIX_COD
				  FROM STG.VIDYO_STG cd
				 INNER JOIN STG.CODE_LKUP conf
					on cd.ConferenceType = conf.code
				   and conf.Source = 'Vidyo'
				   and conf.[Column] = 'ConferenceType'
				 INNER JOIN STG.CODE_LKUP endp
					on cd.EndpointType = endp.CODE
				   and endp.SOURCE = 'Vidyo'
				   and endp.[Column] = 'EndpointType'
				 INNER JOIN STG.CODE_LKUP dir
					on cd.Direction = dir.code
				   and dir.SOURCE = 'Vidyo'
				   and dir.[Column] = 'Direction'
				LEFT JOIN STG.CODE_LKUP rou
				   ON cd.RouterID = rou.code
				  and rou.SOURCE = 'Vidyo'
				  and rou.[Column] = 'Router'	   
			UNION
			SELECT
			    [ROLE] as [ROLE],
				'COMPLETED' as CALL_STATE,
				'Undefined' as CONFERENCE_NAM,
				CASE [Call Type] 
					WHEN '' THEN 'Undefined'
					WHEN NULL THEN 'Undefined'
					ELSE [Call Type]
				 END  as COMMUNICATION_TYPE_NAM,
				[Category Name] as PLATFORM_NAM,
				'Undefined' as DIRECTION,
				CASE  [Bridge Name]
				  WHEN '' THEN 'Undefined'
				  WHEN NULL THEN 'Undefined'
				  ELSE [Bridge Name]
				 END  as BRIDGE_ROUTER_COD,
				CASE
				  WHEN [Charge Band] = '' THEN 'Undefined'
				  WHEN [Charge Band] is null THEN 'Undefined' 
				  ELSE [Charge Band]
				 END as CHARGE_COUNTRY_COD,
				CASE
				  WHEN ltrim(rtrim(Country)) = '' THEN 'Undefined'
				  WHEN ltrim(rtrim(Country)) is null THEN 'Undefined'
				  ELSE Country
				END as CHARGE_COUNTRY_NAM,
            	CASE
				 WHEN Role = 'Chairperson' THEN 'Y'
				 ELSE 'N'
				END as HOST_FLAG,
				CASE Product
				 WHEN 'WebEx' THEN 'Video'
				 WHEN 'MeetMe' THen 'Audio'
				 END as MEDIA_TYPE_NAM,
				 'Undefined' as GATEWAY_ID_COD,
				 'Undefined' as GATEWAY_PREFIX_COD
			from stg.MeetMe_Webex
		UNION
			Select  distinct
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Y',
					MediaTypes,
					'Undefined' as GATEWAY_ID_COD,
					'Undefined' as GATEWAY_PREFIX_COD
			  FROM STG.SessionDetailsView_STG
		UNION
			Select  distinct
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'Undefined',
					'N',
					MediaTypes,
					'Undefined' as GATEWAY_ID_COD,
					'Undefined' as GATEWAY_PREFIX_COD
			  FROM STG.SessionDetailsView_STG
			)			
		except
		SELECT 
				[ROLE]
				,[CALL_STATE]
				,[CONFERENCE_NAM]				
				,[COMMUNICATION_TYPE_NAM]
				,[PLATFORM_NAM]
				,[DIRECTION]
				,[BRIDGE_ROUTER_COD]
				,CHARGE_COUNTRY_COD
				,CHARGE_COUNTRY_NAM
				,HOST_FLG
				,MEDIA_TYPE_NAM
				,GATEWAY_ID_COD
				,GATEWAY_PREFIX_COD
			FROM [BDW].[SERVICE_USAGE_JNK_DIM]
			;










GO
/****** Object:  StoredProcedure [STG].[SERVICE_VENDOR_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[SERVICE_VENDOR_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: SERVICE_VENDOR_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the product from the MeetMe/Webex dataset and inserts it if it is not already in
//					 the dimension table.  The type of data is defaulted as "Meeting Software".
//
*/
		--Remove all previous staging entries
		TRUNCATE TABLE STG.SERVICE_VENDOR_STG;

		/*
			Grab all the Product info from the MeetMe Webex data.  The Vidyo data
			was initially seeded by an insert statement.
		*/
		INSERT INTO STG.SERVICE_VENDOR_STG (
							COMPANY_NAME,
							[TYPE])
			Select 
					Product as COMPANY_NAME,
					'Meeting Software' AS [TYPE]
			from stg.MeetMe_Webex
			except
			SELECT 
				   COMPANY_NAME,
					[TYPE]
			  FROM BDW.SERVICE_VENDOR_DIM
			;



GO
/****** Object:  StoredProcedure [STG].[SessionDetailsView_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[SessionDetailsView_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: SessionDetailsView_STG_M
//		Date: Feb 9, 2015
//		Description: This proc takes the data from the SessionDetailsView table in the Lync database and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old staging data
	TRUNCATE TABLE STG.SessionDetailsView_STG;


	--Insert the latest country data from the systemcheck database
		INSERT 
		  INTO STG.SessionDetailsView_STG
		  SELECT [SessionIdTime]
		  ,[SessionIdSeq]
		  ,[InviteTime]
		  ,[FromUri]
		  ,[ToUri]
		  ,[FromUriType]
		  ,[ToUriType]
		  ,[FromTenant]
		  ,[ToTenant]
		  ,[FromEndpointId]
		  ,[ToEndpointId]
		  ,[EndTime]
		  ,[FromMessageCount]
		  ,[ToMessageCount]
		  ,[FromClientVersion]
		  ,[FromClientType]
		  ,[FromClientCategory]
		  ,[ToClientVersion]
		  ,[ToClientType]
		  ,[ToClientCategory]
		  ,[TargetUri]
		  ,[TargetUriType]
		  ,[TargetTenant]
		  ,[OnBehalfOfUri]
		  ,[OnBehalfOfUriType]
		  ,[OnBehalfOfTenant]
		  ,[ReferredByUri]
		  ,[ReferredByUriType]
		  ,[ReferredByTenant]
		  ,[DialogId]
		  ,[CorrelationId]
		  ,[ReplacesDialogIdTime]
		  ,[ReplacesDialogIdSeq]
		  ,[ReplacesDialogId]
		  ,[ResponseTime]
		  ,[ResponseCode]
		  ,[DiagnosticId]
		  ,[ContentType]
		  ,[FrontEnd]
		  ,[Pool]
		  ,[FromEdgeServer]
		  ,[ToEdgeServer]
		  ,[IsFromInternal]
		  ,[IsToInternal]
		  ,[CallPriority]
		  ,CASE MediaTypes
		      WHEN 1 THEN 'IM'
			  WHEN 16 THEN 'Audio'
			  WHEN 32 THEN 'Video'
			END as MediaTypes
		  ,[FromUserFlag]
		  ,[ToUserFlag]
		  ,[CallFlag]
		  ,[Location]
	   FROM [ADCLCSQL01_LCSCDR].[LcsCDR].[dbo].[SessionDetailsView]
	  where mediatypes in ( 1, 16 ,32 )
		and SessionIdTime >'2014-11-01';

	  update statistics STG.SessionDetailsView_STG WITH SAMPLE 50 PERCENT, ALL;




GO
/****** Object:  StoredProcedure [STG].[tb_CACHE_Daily_Details_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [STG].[tb_CACHE_Daily_Details_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_CACHE_Daily_Details_STG_M
//		Date: Feb 16, 2015
//		Description: This proc takes the data from the tb_CACHE_Daily_Details table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/
	--Remove old user data from stating table 
	TRUNCATE TABLE STG.tb_CACHE_Daily_Details_STG;

	--Grab the latest user data from systemcheck
	INSERT INTO stg.tb_CACHE_Daily_Details_STG
				 Select 	   [detail_id]
							  ,[build_id]
							  ,[user_id]
							  ,[asset_id]
							  ,[last_updated]
							  ,[build_status]
							  ,[user_employee_id]
							  ,[user_employee_type]
							  ,[user_display_name]
							  ,[user_first_name]
							  ,[user_last_name]
							  ,[user_logon_name]
							  ,[user_department]
							  ,[user_title]
							  ,[user_host_office]
							  ,[user_region]
							  ,[user_new_region]
							  ,[user_office_system]
							  ,[user_mobile_number]
							  ,[user_phone_number]
							  ,[user_assistant]
							  ,[user_start_date]
							  ,[user_term_date]
							  ,[user_user_creation_date]
							  ,[user_user_retirement_date]
							  ,[user_user_ou]
							  ,[user_email_address]
							  ,[user_email_server]
							  ,[user_exchange_server_version]
							  ,[user_default_limits_used]
							  ,[user_limit_send]
							  ,[user_limit_send_receive]
							  ,[user_warning_limit]
							  ,[user_calendar_item_count]
							  ,[user_calendar_size]
							  ,[user_contacts_item_count]
							  ,[user_contacts_size]
							  ,[user_exchange_data_reported]
							  ,[user_inbox_item_count]
							  ,[user_inbox_size]
							  ,[user_mailbox_item_count]
							  ,[user_mailbox_size]
							  ,[user_sent_item_count]
							  ,[user_sent_size]
							  ,[user_user_last_logon]
							  ,[build_computer_name]
							  ,[build_computer_creation_date]
							  ,[build_computer_retirement_date]
							  ,[build_computer_office_ou]
							  ,[build_computer_ou]
							  ,[build_computer_status]
							  ,[build_operating_system]
							  ,[build_ie_full_version]
							  ,[build_ie_version]
							  ,[build_pagefile]
							  ,[build_crashplan_first_backup]
							  ,[build_crashplan_guid]
							  ,[build_encryption_status]
							  ,[build_bcg_build_date]
							  ,[build_bcg_build_version]
							  ,[build_image_model]
							  ,[build_image_part_number]
							  ,[build_image_serial]
							  ,[build_image_task_id]
							  ,[build_image_task_version]
							  ,[build_image_technician]
							  ,[build_image_user]
							  ,[build_hard_drive_firmware]
							  ,[build_hard_drive_model]
							  ,[build_hard_drive_size]
							  ,[build_clone_utility_completed]
							  ,[build_clone_utility_in_progress]
							  ,[build_date_cloned]
							  ,[build_hard_drive_cloned]
							  ,[build_ms_office_version]
							  ,[build_outlook_caching]
							  ,[build_calendar_caching]
							  ,[build_public_folders_caching]
							  ,[build_imd_auto_enable]
							  ,[build_imd_disabled]
							  ,[build_imd_load_behavior]
							  ,[build_imd_version]
							  ,[build_video_driver]
							  ,[build_wired_nic_driver]
							  ,[build_wireless_nic_driver]
							  ,[build_imd_load_behavior_hkcu]
							  ,[build_image_reason]
							  ,[build_training_date]
							  ,[build_handover_date]
							  ,[build_agent_version]
							  ,[build_agent_last_run]
							  ,[build_outlook_scan_last_run]
							  ,[build_computer_last_logon]
							  ,[build_last_restart]
							  ,[build_crashplan_last_activity]
							  ,[build_crashplan_last_backup]
							  ,[build_hard_drive_available]
							  ,[build_hard_drive_free]
							  ,[build_windows_stability_index]
							  ,[build_registry_corrupt]
							  ,[build_last_registry_corrupt_date]
							  ,[build_colligo_version]
							  ,[build_icloud_folder_size]
							  ,[build_sccm_installed]
							  ,[build_active_power_scheme]
							  ,[build_crashplan_backup_size]
							  ,[build_crashplan_backup_remaining]
							  ,[build_crashplan_version]
							  ,[build_crashplan_install_date]
							  ,[build_crashplan_office]
							  ,[build_laptop_resolution]
							  ,[build_DCOM_enabled]
							  ,[build_windows_architecture]
							  ,[build_disk_controller_driver]
							  ,[build_disk_status]
							  ,[asset_model_number]
							  ,[asset_serial_number]
							  ,[asset_computer_model]
							  ,[asset_purchase_date]
							  ,[asset_warranty_expiration]
							  ,[asset_memory]
							  ,[asset_bios_release_date]
							  ,[asset_bios_version]
							  ,[asset_processor_model]
							  ,[asset_video_model]
							  ,[asset_wired_nic_model]
							  ,[asset_wireless_nic_model]
							  ,[asset_chassis_type]
							  ,[asset_battery_capacity]
							  ,[asset_battery_capacity_2]
							  ,[asset_battery_1_name]
							  ,[asset_battery_1_serial]
							  ,[asset_battery_2_name]
							  ,[asset_battery_2_serial]
							  ,[asset_sim_card_number]
							  ,[asset_disk_controller]
							  ,[total_pst_count]
							  ,[open_pst_count]
							  ,[pst_over_4gb]
							  ,[bsod_count]
							  ,[crash_count]
							  ,[excel_count]
							  ,[powerpoint_count]
							  ,[outlook_count]
							  ,[word_count]
							  ,[ie_count]
							  ,[date_inserted]
						   FROM [SYSTEMCHECK3_PROD_RO].[SystemCheck3].[dbo].[tb_CACHE_Daily_Details]

	  update statistics STG.tb_CACHE_Daily_Details_STG WITH SAMPLE 50 PERCENT, ALL;




GO
/****** Object:  StoredProcedure [STG].[tb_Ref_Cohort_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Ref_Cohort_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Ref_Cohort_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Ref_Cohort table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

--Remove old data
TRUNCATE TABLE STG.tb_Ref_Cohort_STG;


--Grab the latest data from corhort table on the systemcheck database
INSERT 
  INTO STG.tb_Ref_Cohort_STG
  SELECT *
  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Ref_Cohort;



  update statistics STG.tb_Ref_Cohort_STG WITH SAMPLE 50 PERCENT, ALL;


GO
/****** Object:  StoredProcedure [STG].[tb_Ref_Country_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Ref_Country_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Ref_Country_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Ref_Country table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old staging data
	TRUNCATE TABLE STG.tb_Ref_Country_STG;


	--Insert the latest country data from the systemcheck database
	INSERT 
	  INTO STG.tb_Ref_Country_STG
	  SELECT *
	  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Ref_Country;



	  update statistics STG.tb_Ref_Country_STG WITH SAMPLE 50 PERCENT, ALL;



GO
/****** Object:  StoredProcedure [STG].[tb_Ref_Office_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Ref_Office_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Ref_Office_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Ref_Office table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old staging data
	TRUNCATE TABLE STG.tb_Ref_Office_STG;

	--Insert the latest data into the office table from the systemcheck database
	INSERT 
	  INTO STG.tb_Ref_Office_STG
	  SELECT *
	  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Ref_Office;


	  update statistics STG.tb_Ref_Office_STG WITH SAMPLE 50 PERCENT, ALL;


GO
/****** Object:  StoredProcedure [STG].[tb_Ref_Office_system_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Ref_Office_system_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Ref_Office_system
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Ref_Office_system table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old office system data from the staging table
	TRUNCATE TABLE STG.tb_Ref_Office_system_STG;

	--Insert the newest office system rows from systemcheck into the staging table
	INSERT 
	  INTO STG.tb_Ref_Office_system_STG
	  SELECT *
	  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Ref_Office_system;




	  update statistics STG.tb_Ref_Office_system_STG WITH SAMPLE 50 PERCENT, ALL;


GO
/****** Object:  StoredProcedure [STG].[tb_Ref_Region_New_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Ref_Region_New_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Ref_Region_New
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Ref_Region_New table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old New region data
	TRUNCATE TABLE STG.tb_Ref_Region_New_STG;

	--Insert the lastest new region data from systemcheck
	INSERT 
	  INTO STG.tb_Ref_Region_New_STG
	  SELECT *
	  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Ref_Region_New;


	  update statistics STG.tb_Ref_Region_New_STG WITH SAMPLE 50 PERCENT, ALL;


GO
/****** Object:  StoredProcedure [STG].[tb_Ref_Region_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Ref_Region_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Ref_Region_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Ref_Region table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old region data from the staging area
	TRUNCATE TABLE STG.tb_Ref_Region_STG;

	--Load the latest region data from the systemcheck database
	INSERT 
	  INTO STG.tb_Ref_Region_STG
	  SELECT *
	  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Ref_Region;


	  update statistics STG.tb_Ref_Region_STG WITH SAMPLE 50 PERCENT, ALL;




GO
/****** Object:  StoredProcedure [STG].[tb_Working_Data_User_Header_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[tb_Working_Data_User_Header_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: tb_Working_Data_User_Header_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the tb_Working_Data_User_Header table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/
	--Remove old user data from stating table 
	TRUNCATE TABLE STG.tb_Working_Data_User_Header_STG;

	--Grab the latest user data from systemcheck
	INSERT 
	  INTO STG.tb_Working_Data_User_Header_STG
	  SELECT *
	  FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.dbo.tb_Working_Data_User_Header
	  ;

	  update statistics STG.tb_Working_Data_User_Header_STG WITH SAMPLE 50 PERCENT, ALL;



GO
/****** Object:  StoredProcedure [STG].[vw_BCG_Tableau_Data_STG_M]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [STG].[vw_BCG_Tableau_Data_STG_M] AS
/*
//		Author: Jacob Leon - Slalom Consulting
//		Procedure: vw_BCG_Tableau_Data_STG_M
//		Date: Jan 21, 2015
//		Description: This proc takes the data from the vw_BCG_Tableau_Data_STG_M table on system check and makes a 
//					 local copy.  This is for performance reasons.
//
*/

	--Remove old New region data
	TRUNCATE TABLE STG.vw_BCG_Tableau_Data_STG;

	--Insert the lastest new region data from systemcheck
	INSERT 
	  INTO STG.vw_BCG_Tableau_Data_STG
	  SELECT *
	    FROM SYSTEMCHECK3_PROD_RO.SystemCheck3.[dbo].[vw_BCG_Tableau_Data];


	  update statistics STG.vw_BCG_Tableau_Data_STG WITH SAMPLE 50 PERCENT, ALL;



GO
/****** Object:  Table [BDW].[DATE_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[DATE_DIM](
	[DATE_IDN] [int] NOT NULL,
	[DATE_NUM] [int] NOT NULL,
	[DATE] [datetime] NULL,
	[FULL_DATE_UK] [char](10) NULL,
	[FULL_DATE_USA] [char](10) NULL,
	[DAY_OF_MONTH] [varchar](2) NULL,
	[DAY_SUFFIX] [varchar](4) NULL,
	[DAY_NAME] [varchar](9) NULL,
	[DAY_OF_WEEK_USA] [char](1) NULL,
	[DAY_OF_WEEK_UK] [char](1) NULL,
	[DAY_OF_WEEK_IN_MONTH] [varchar](2) NULL,
	[DAY_OF_WEEK_IN_YEAR] [varchar](2) NULL,
	[DAY_OF_QUARTER] [varchar](3) NULL,
	[DAY_OF_YEAR] [varchar](3) NULL,
	[WEEK_OF_MONTH] [varchar](1) NULL,
	[WEEK_OF_QUARTER] [varchar](2) NULL,
	[WEEK_OF_YEAR] [varchar](2) NULL,
	[MONTH_NUM] [int] NULL,
	[MONTH_NAME] [varchar](9) NULL,
	[MONTH_OF_QUARTER] [varchar](2) NULL,
	[QUARTER_NUM] [int] NULL,
	[QUARTER_NAME] [varchar](9) NULL,
	[YEAR] [char](4) NULL,
	[YEAR_NAME] [varchar](9) NULL,
	[MONTH_YEAR] [char](10) NULL,
	[MMYYYY] [char](6) NULL,
	[FIRST_DAY_OF_MONTH] [date] NULL,
	[LAST_DAY_OF_MONTH] [date] NULL,
	[FIRST_DAY_OF_QUARTER] [date] NULL,
	[LAST_DAY_OF_QUARTER] [date] NULL,
	[FIRST_DAY_OF_YEAR] [date] NULL,
	[LAST_DAY_OF_YEAR] [date] NULL,
	[IS_HOLIDAY_USA] [char](1) NULL,
	[IS_WEEKDAY] [char](1) NULL,
	[HOLIDAY_USA] [varchar](50) NULL,
	[IS_HOLIDAY_UK] [char](1) NULL,
	[HOLIDAY_UK] [varchar](50) NULL,
 CONSTRAINT [MEETING_USAGE_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[DATE_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[PARTICIPANT_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[PARTICIPANT_DIM](
	[PARTICIPANT_IDN] [int] NOT NULL,
	[EMPLOYEE_ID] [bigint] NULL,
	[FIRST_NAM] [nvarchar](65) NULL,
	[LAST_NAM] [nvarchar](65) NULL,
	[FULL_NAM] [nvarchar](65) NULL,
	[OFFICE_COD] [char](3) NOT NULL,
	[JOB_TITLE_NAM] [varchar](100) NOT NULL,
	[EMAIL] [varchar](50) NOT NULL,
	[JOB_NAM] [varchar](30) NOT NULL,
	[ASSISTANT_NAM] [varchar](80) NOT NULL,
	[HIRE_DATE] [date] NOT NULL,
	[TERMINATE_DATE] [date] NOT NULL,
	[COHORT_NAM] [varchar](50) NOT NULL,
	[VIDYO_INSTLD_IND] [char](1) NOT NULL,
	[WEBEX_INSTLD_IND] [char](1) NOT NULL,
	[LYNC_INSTLD_IND] [char](1) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NULL,
	[AUDIT_UPDATE_IDN] [int] NULL,
	[REC_START_DATE] [date] NOT NULL,
	[REC_END_DATE] [date] NOT NULL,
	[EMPLOYEE_TYPE_NAM] [varchar](40) NOT NULL,
	[DEPARTMENT_NAM] [varchar](40) NOT NULL,
 CONSTRAINT [PARTICIPANT_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[PARTICIPANT_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [PARTICIPANT_DIM_IDX_UNQ01] UNIQUE NONCLUSTERED 
(
	[FULL_NAM] ASC,
	[EMAIL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [BDW].[vw_OFFICE_COHORT_HEADCOUNT]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [BDW].[vw_OFFICE_COHORT_HEADCOUNT] as
Select eomonth(date) as LAST_DAY_MONTH,
		pd.cohort_nam as Cohort,
		pd.office_cod as Office,
		pd.employee_type_nam as Employee_Type,
		count(*) as Num_Employees
  from bdw.date_dim dd
inner join bdw.participant_dim pd
on date between HIRE_DATE and TERMINATE_DATE
where pd.rec_end_date = '12-31-9999'
  and eomonth(dd.date) between '10-01-2014'and '03-01-2015'
  AND pd.employee_type_nam in ('Regular - Active','Contract - Active')
  and dd.date = eomonth(dd.date)
group by date,
		pd.cohort_nam,
		pd.office_cod,
		pd.employee_type_nam ;

GO
/****** Object:  Table [BDW].[APPLICATION_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[APPLICATION_DIM](
	[APPLICATION_IDN] [int] NOT NULL,
	[APPLICATION_NAM] [varchar](260) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NULL,
	[AUDIT_UPDATE_IDN] [int] NULL,
	[VERSION_COD] [varchar](100) NOT NULL,
	[PUBLISHER_IDN] [int] NULL,
 CONSTRAINT [APPLICATION_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[APPLICATION_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[MOBILE_DEVICE_APP_FAC]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[MOBILE_DEVICE_APP_FAC](
	[MOBILE_DEVICE_IDN] [int] NOT NULL,
	[MOBILE_DEVICE_JNK_IDN] [int] NOT NULL,
	[EMPLOYEE_IDN] [int] NOT NULL,
	[APPLICATION_IDN] [int] NOT NULL,
	[DEVICE_LAST_SEEN_DATE] [datetime] NOT NULL,
	[DEVICE_LAST_ENROLLED_DATE] [datetime] NOT NULL,
	[DEVICE_LAST_COMPLIANCE_CHK_DATE] [datetime] NOT NULL,
	[DEVICE_LAST_COMPROMISED_CHK_DATE] [datetime] NOT NULL,
	[SNPSHOT_CAPTURE_DATE] [date] NOT NULL,
	[MAC_ADDRESS] [varchar](20) NOT NULL,
	[IMEI] [varchar](40) NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[MOBILE_DEVICE_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[MOBILE_DEVICE_DIM](
	[MOBILE_DEVICE_IDN] [int] NOT NULL,
	[PLATFORM_NAM] [varchar](20) NOT NULL,
	[MODEL_NAM] [varchar](60) NOT NULL,
	[DEVICE_NAM] [varchar](20) NOT NULL,
	[OS_VERSION_NAM] [varchar](20) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL,
 CONSTRAINT [MOBILE_DEVICE_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[MOBILE_DEVICE_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[MOBILE_DEVICE_JNK_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[MOBILE_DEVICE_JNK_DIM](
	[MOBILE_DEVICE_JNK_IDN] [int] NOT NULL,
	[DEVICE_OWNERSHIP] [varchar](10) NOT NULL,
	[ENROLLMENT_STATUS] [varchar](30) NOT NULL,
	[COMPLIANCE_STATUS] [varchar](40) NOT NULL,
	[COMPROMISED_STATUS] [varchar](10) NOT NULL,
	[APP_ISMANAGED] [varchar](10) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL,
 CONSTRAINT [MOBILE_DEVICE_JNK_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[MOBILE_DEVICE_JNK_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [BDW].[vw_MOBLIE_DEVICE_APP_DETAILS]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [BDW].[vw_MOBLIE_DEVICE_APP_DETAILS] AS
	Select  
			mdf.MAC_ADDRESS,
			mdf.IMEI,
			mdf.DEVICE_LAST_SEEN_DATE,
			mdf.DEVICE_LAST_ENROLLED_DATE,
			mdf.DEVICE_LAST_COMPLIANCE_CHK_DATE,
			mdf.DEVICE_LAST_COMPROMISED_CHK_DATE,
			mdf.SNPSHOT_CAPTURE_DATE,
			mdd.DEVICE_NAM,
			mdd.MODEL_NAM,
			mdd.OS_VERSION_NAM,
			mdd.PLATFORM_NAM,
			mdjd.DEVICE_OWNERSHIP,
			mdjd.ENROLLMENT_STATUS,
			mdjd.COMPLIANCE_STATUS,
			mdjd.COMPROMISED_STATUS,
			mdjd.APP_ISMANAGED,
			pd.FULL_NAM,
			pd.JOB_TITLE_NAM,
			pd.EMAIL,
			pd.OFFICE_COD,
			pd.COHORT_NAM,
			pd.EMPLOYEE_TYPE_NAM,
			pd.DEPARTMENT_NAM,
			ad.APPLICATION_NAM,
			ad.VERSION_COD
	  from bdw.mobile_device_app_fac mdf
inner join bdw.MOBILE_DEVICE_DIM mdd
		on mdf.mobile_device_idn = mdd.mobile_device_idn
inner join bdw.MOBILE_DEVICE_JNK_DIM mdjd
		on mdf.mobile_device_jnk_idn = mdjd.mobile_device_jnk_idn
inner join bdw.participant_dim pd
		on mdf.EMPLOYEE_IDN = pd.participant_idn
inner join bdw.application_dim ad
		on mdf.APPLICATION_IDN = ad.APPLICATION_IDN

GO
/****** Object:  Table [BDW].[OFFICE_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[OFFICE_DIM](
	[OFFICE_IDN] [int] NOT NULL,
	[OFFICE_COD] [char](3) NULL,
	[OFFICE_NAM] [varchar](40) NOT NULL,
	[REGION_COD_NEW] [varchar](10) NOT NULL,
	[REGION_COD_OLD] [varchar](10) NOT NULL,
	[COUNTRY_NAM] [varchar](40) NOT NULL,
	[SYSTEM_NAM] [varchar](40) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL,
 CONSTRAINT [OFFICE_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[OFFICE_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[SERVICE_USAGE_FAC]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[SERVICE_USAGE_FAC](
	[CONFERENCE_ID] [varchar](100) NULL,
	[START_DATETIME] [datetime] NOT NULL,
	[END_DATETIME] [datetime] NOT NULL,
	[DURATION] [int] NOT NULL,
	[BRIDGE_AMT] [decimal](8, 2) NULL,
	[TRANSPORT_AMT] [decimal](8, 2) NULL,
	[TOTAL_AMT] [decimal](8, 2) NULL,
	[COST_USD_AMT] [decimal](8, 2) NULL,
	[START_DATE_IDN] [int] NOT NULL,
	[END_DATE_IDN] [int] NOT NULL,
	[SERVICE_VENDOR_IDN] [int] NOT NULL,
	[PARTICIPANT_IDN] [int] NOT NULL,
	[SERVICE_USAGE_JNK_IDN] [int] NOT NULL,
	[AUDIT_CREATE_IDN] [int] NULL,
	[AUDIT_UPDATE_IDN] [int] NULL,
	[OFFICE_IDN] [int] NOT NULL,
	[CHAIRPERSON_IDN] [int] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[SERVICE_USAGE_JNK_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[SERVICE_USAGE_JNK_DIM](
	[SERVICE_USAGE_JNK_IDN] [int] NOT NULL,
	[ROLE] [varchar](15) NOT NULL,
	[PLATFORM_NAM] [varchar](40) NULL,
	[CONFERENCE_NAM] [varchar](80) NULL,
	[AUDIT_CREATE_IDN] [int] NULL,
	[AUDIT_UPDATE_IDN] [int] NULL,
	[COMMUNICATION_TYPE_NAM] [varchar](50) NULL,
	[BRIDGE_ROUTER_COD] [varchar](20) NOT NULL,
	[CALL_STATE] [varchar](20) NOT NULL,
	[DIRECTION] [varchar](20) NOT NULL,
	[CHARGE_COUNTRY_COD] [varchar](9) NULL,
	[CHARGE_COUNTRY_NAM] [varchar](30) NULL,
	[HOST_FLG] [char](1) NULL,
	[MEDIA_TYPE_NAM] [varchar](10) NULL,
	[GATEWAY_ID_COD] [varchar](20) NULL,
	[GATEWAY_PREFIX_COD] [varchar](20) NULL,
 CONSTRAINT [SERVICE_USAGE_JNK_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[SERVICE_USAGE_JNK_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[SERVICE_VENDOR_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[SERVICE_VENDOR_DIM](
	[SERVICE_VENDOR_IDN] [int] NOT NULL,
	[COMPANY_NAME] [varchar](20) NULL,
	[TYPE] [varchar](40) NULL,
	[AUDIT_CREATE_IDN] [int] NULL,
	[AUDIT_UPDATE_IDN] [int] NULL,
 CONSTRAINT [SERVICE_VENDOR_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[SERVICE_VENDOR_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [BDW].[vw_SERVICE_USAGE_DETAILS]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [BDW].[vw_SERVICE_USAGE_DETAILS] AS
	Select USF.CONFERENCE_ID,
		   USF.START_DATETIME,
		   USF.END_DATETIME,
		   USF.DURATION as DURATION_SEC,
		   SVD.COMPANY_NAME AS SERVICE_NAME,
		   par.FULL_NAM as FULL_NAME,
		   par.JOB_TITLE_NAM as JOB_TITLE_NAME,
		   par.EMAIL as EMAIL_ADDRESS,
		   par.TERMINATE_DATE,
		   par.COHORT_NAM as COHORT_NAME,
		   od.OFFICE_COD as OFFICE_CODE,
		   od.OFFICE_NAM as OFFICE_NAME,
		   od.REGION_COD_NEW,
		   od.REGION_COD_OLD,
		   od.COUNTRY_NAM as COUNTRY_NAME,
		   suj.MEDIA_TYPE_NAM as MEDIA_TYPE_NAME,
		   suj.host_flg as CHAIRPERSON_FLAG
	  FROM BDW.SERVICE_USAGE_FAC USF
inner join bdw.service_vendor_dim svd
		on usf.service_vendor_idn = svd.service_vendor_idn
inner join bdw.participant_dim par
		on usf.PARTICIPANT_IDN = par.PARTICIPANT_IDN
inner join bdw.office_dim od
		ON usf.OFFICE_idn = OD.office_idn
Inner join bdw.service_usage_jnk_dim suj
		on usf.SERVICE_USAGE_JNK_IDN = suj.SERVICE_USAGE_JNK_IDN
INNER JOIN bdw.participant_dim chr
		on usf.chairperson_idn = chr.participant_idn
where USF.START_DATETIME < '03-01-2015'
  and (chr.email like '%@bcg.com'
		or svd.COMPANY_NAME ='MeetMe')
		;



GO
/****** Object:  Table [BDW].[COMP_HARDWARE_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[COMP_HARDWARE_DIM](
	[COMP_HARDWARE_IDN] [int] NOT NULL,
	[IMAGE_MODEL_DESC] [varchar](40) NOT NULL,
	[HD_MODEL_DESC] [varchar](80) NOT NULL,
	[COMP_MODEL_DESC] [varchar](40) NOT NULL,
	[WINDOWS_ARCH_DESC] [varchar](10) NOT NULL,
	[PROCESSOR_MODEL_DESC] [varchar](60) NOT NULL,
	[VIDEO_MODEL_DESC] [varchar](80) NOT NULL,
	[WIRED_NIC_MODEL_DESC] [varchar](60) NOT NULL,
	[WIRELESS_NIC_MODEL_DESC] [varchar](60) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL,
	[NAME] [varchar](20) NULL,
	[STATUS] [varchar](20) NULL,
 CONSTRAINT [COMP_HARDWARE_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[COMP_HARDWARE_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [BDW].[COMPUTER_APPLICATION_FAC]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BDW].[COMPUTER_APPLICATION_FAC](
	[APPLICATION_IDN] [int] NOT NULL,
	[COMP_HARDWARE_IDN] [int] NOT NULL,
	[INSTALL_DATE_IDN] [int] NOT NULL,
	[USER_IDN] [int] NOT NULL,
	[BUILD_DATE_IDN] [int] NOT NULL,
	[PURCHASE_DATE_IDN] [int] NOT NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL,
	[OFFICE_IDN] [int] NOT NULL,
	[SCCM_CAPTURED_DATE] [date] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [BDW].[PUBLISHER_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[PUBLISHER_DIM](
	[PUBLISHER_IDN] [int] NOT NULL,
	[PUBLISHER_NAM] [varchar](40) NOT NULL,
	[AUDIT_CREATE_IDN] [int] NOT NULL,
	[AUDIT_UPDATE_IDN] [int] NOT NULL,
 CONSTRAINT [PUBLISHER_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[PUBLISHER_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [BDW].[vw_SCCM_Dashboard]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [BDW].[vw_SCCM_Dashboard] AS
			Select 
					cd.NAME as [Computer Name],
					cd.STATUS as [Computer Status],
					PD.DEPARTMENT_NAM as Department,
					PD.EMPLOYEE_TYPE_NAM as [Employee Type],
					PD.FULL_NAM as [Employee Name],
					PD.OFFICE_COD as [Host Office],
					PD.Cohort_Nam	as Cohort,
					instl.DATE as [Install Date],
					OD.REGION_COD_NEW as [New Region],
					OD.SYSTEM_NAM as [Office System],
					PUBD.PUBLISHER_NAM as Publisher,
					AD.APPLICATION_NAM as Application,
					BLD.DATE as [Build Date],
					PUR.DATE as [Purchase Date],
					CAF.SCCM_CAPTURED_DATE as [Captured Date]
			  from bdw.COMPUTER_APPLICATION_FAC CAF
		INNER JOIN bdw.APPLICATION_DIM AD
		 	    ON caf.APPLICATION_idn = ad.application_idn
		INNER JOIN BDW.PUBLISHER_DIM PUBD
				ON AD.PUBLISHER_IDN = PUBD.PUBLISHER_IDN
		INNER JOIN bdw.PARTICIPANT_DIM PD
				ON CAF.USER_IDN = PD.PARTICIPANT_IDN
		INNER JOIN bdw.COMP_HARDWARE_DIM CD
				ON CAF.COMP_HARDWARE_IDN = CD.COMP_HARDWARE_IDN
		INNER JOIN BDW.DATE_DIM instl
				ON CAF.INSTALL_DATE_IDN = instl.DATE_IDN
		INNER JOIN BDW.DATE_DIM BLD
				ON CAF.BUILD_DATE_IDN = BLD.DATE_IDN
		INNER JOIN BDW.DATE_DIM PUR
				ON CAF.PURCHASE_DATE_IDN = PUR.DATE_IDN
		INNER JOIN BDW.OFFICE_DIM OD
				ON CAF.OFFICE_IDN = OD.OFFICE_IDN
			 WHERE Application_Nam <> 'No Applications'
			;



GO
/****** Object:  Table [STG].[vw_BCG_Tableau_Data_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[vw_BCG_Tableau_Data_STG](
	[header_id] [bigint] NULL,
	[display_name] [nvarchar](255) NULL,
	[employee_id] [int] NULL,
	[region] [varchar](30) NULL,
	[office_system] [varchar](150) NULL,
	[host_office] [varchar](30) NULL,
	[cohort] [nvarchar](150) NULL,
	[department] [nvarchar](150) NULL,
	[title] [nvarchar](255) NULL,
	[start_date] [date] NULL,
	[term_date] [date] NULL,
	[employee_type] [varchar](100) NULL,
	[hard_drive_free] [float] NULL,
	[hard_drive_available] [int] NULL,
	[hard_drive_size] [int] NULL,
	[hard_drive_firmware] [varchar](255) NULL,
	[hard_drive_model] [varchar](255) NULL,
	[computer_status] [varchar](150) NULL,
	[computer_name] [varchar](150) NULL,
	[computer_type] [varchar](100) NULL,
	[computer_model] [varchar](100) NULL,
	[bcg_build_date] [date] NULL,
	[agent_last_run] [datetime] NULL,
	[purchase_date] [date] NULL,
	[bcg_build_version] [varchar](100) NULL,
	[BSODs] [int] NULL,
	[System Crashes] [int] NULL,
	[Excel] [int] NULL,
	[PowerPoint] [int] NULL,
	[Outlook] [int] NULL,
	[Word] [int] NULL,
	[IE] [int] NULL,
	[build_id] [bigint] NULL,
	[asset_id] [bigint] NULL,
	[user_id] [bigint] NULL,
	[DATE] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [BDW].[vw_BCG_Tableau_Data]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [BDW].[vw_BCG_Tableau_Data] AS 
	  SELECT *
	    FROM STG.[vw_BCG_Tableau_Data_STG];



GO
/****** Object:  Table [BDW].[APP_METERING_FAC]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BDW].[APP_METERING_FAC](
	[MONTH_IDN] [int] NULL,
	[COMP_HARDWARE_IDN] [int] NULL,
	[PARTICIPANT_IDN] [int] NULL,
	[OFFICE_IDN] [int] NULL,
	[PUBLISHER_IDN] [int] NULL,
	[LAST_USAGE_DATE_IDN] [int] NULL,
	[TOTAL_USAGES] [int] NULL,
	[AVG_DAILY_USAGES] [decimal](8, 2) NULL,
	[TOTAL_DURATION_MIN] [decimal](10, 2) NULL,
	[AVG_DURATION_MIN] [decimal](8, 2) NULL,
	[AVG_DAILY_DURATION_MIN] [decimal](8, 2) NULL,
	[LAST_USAGE_DATETIME] [datetime] NULL,
	[AUDIT_CREATE_IDN] [int] NULL,
	[AUDIT_UPDATE_IDN] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [BDW].[MONTH_DIM]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[MONTH_DIM](
	[MONTH_IDN] [int] NOT NULL,
	[MONTH_YEAR] [varchar](10) NULL,
	[MMYYYY] [char](6) NULL,
	[MONTH_NUM] [int] NULL,
	[MONTH_NAME] [varchar](9) NULL,
	[MONTH_OF_QUARTER] [varchar](2) NULL,
	[QUARTER_NUM] [int] NULL,
	[QUARTER_NAME] [varchar](9) NULL,
	[YEAR] [char](4) NULL,
	[YEAR_NAME] [varchar](9) NULL,
 CONSTRAINT [MONTH_DIM_PK] PRIMARY KEY CLUSTERED 
(
	[MONTH_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [BDW].[vw_APP_METERING_DETAILS]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [BDW].[vw_APP_METERING_DETAILS] AS
		Select MD.Month_Year,
			   CH.NAME as COMPUTER_NAME,
			   CH.STATUS as COMPUTER_STATUS,
			   PD.FULL_NAM,
			   PD.OFFICE_COD,
			   PD.DEPARTMENT_NAM,
			   PD.EMPLOYEE_TYPE_NAM,
			   OD.OFFICE_NAM,
			   OD.REGION_COD_NEW,
			   OD.COUNTRY_NAM,
			   OD.SYSTEM_NAM,
			   PUB.PUBLISHER_NAM,
			   AMF.TOTAL_USAGES,
			   AMF.AVG_DAILY_USAGES,
			   AMF.TOTAL_DURATION_MIN,
			   AMF.AVG_DURATION_MIN,
			   AMF.AVG_DAILY_DURATION_MIN,
			   AMF.LAST_USAGE_DATETIME
		  from bdw.APP_METERING_FAC AMF
    INNER JOIN BDW.COMP_HARDWARE_DIM CH
			ON AMF.COMP_HARDWARE_IDN = CH.COMP_HARDWARE_IDN
	INNER JOIN BDW.PARTICIPANT_DIM PD
			ON AMF.PARTICIPANT_IDN = PD.PARTICIPANT_IDN
	INNER JOIN BDW.OFFICE_DIM OD
			ON AMF.OFFICE_IDN = OD.OFFICE_IDN
	INNER JOIN BDW.PUBLISHER_DIM PUB
			ON AMF.PUBLISHER_IDN = PUB.PUBLISHER_IDN
	INNER JOIN BDW.MONTH_DIM MD
			ON AMF.MONTH_IDN = MD.MONTH_IDN			    
		   ;

GO
/****** Object:  UserDefinedFunction [BDW].[GET_OFFICE_IDN]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [BDW].[GET_OFFICE_IDN](@OFFICE_COD CHAR(3))
/*
//		Author: Jacob Leon - Slalom Consulting
//		Function: GET_OFFICE_IDN
//		Date: Jan 21, 2015
//		Description: This proc takes in an Office code and retuns the corresponding OFFICE_IDN.
//
*/
RETURNS INTEGER
WITH SCHEMABINDING AS

 BEGIN
	DECLARE @V_CODE INTEGER;

	SELECT @V_CODE = OFFICE_IDN
	  FROM BDW.OFFICE_DIM OD
	 WHERE OFFICE_COD = @OFFICE_COD
	 ;

	 IF @V_CODE is null 	
		SELECT @V_CODE = OFFICE_IDN
		  FROM BDW.OFFICE_DIM OD
		 WHERE OFFICE_COD = 'N/A';


	RETURN @V_CODE
 END;


GO
/****** Object:  View [BDW].[vw_SERVICE_USAGE_COUNTS]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [BDW].[vw_SERVICE_USAGE_COUNTS] AS
		Select 
		   SVD.COMPANY_NAME AS SERVICE_NAME,
		   chr.COHORT_NAM as COHORT_NAME,
		   od.OFFICE_COD as OFFICE_CODE,
		   od.OFFICE_NAM as OFFICE_NAME,
		   od.REGION_COD_NEW as REGION_COD_NEW,
		   od.REGION_COD_OLD as REGION_COD_OLD,
		   od.COUNTRY_NAM as COUNTRY_NAME,
		   suj.MEDIA_TYPE_NAM as MEDIA_TYPE_NAME,
		   st.MONTH_YEAR as CONFERENCE_MONTH,
		   count(usf.chairperson_Idn) as TOTAL_PARTICIPANTS,
   		   count(distinct USF.CONFERENCE_ID) as CONFERENCE_CNT,
		   SUM(DURATION) as DURATION_SEC,
		   count(distinct usf.chairperson_idn) as CHAIR_PART_CNT
	  FROM BDW.SERVICE_USAGE_FAC USF
inner join bdw.service_vendor_dim svd
		on usf.service_vendor_idn = svd.service_vendor_idn
Inner join bdw.service_usage_jnk_dim suj
		on usf.SERVICE_USAGE_JNK_IDN = suj.SERVICE_USAGE_JNK_IDN
inner join bdw.DATE_DIM st
		on usf.START_DATE_IDN = st.DATE_IDN
INNER JOIN bdw.participant_dim chr
		on usf.chairperson_idn = chr.participant_idn
inner join bdw.office_dim od
		ON BDW.GET_OFFICE_IDN(chr.OFFICE_COD) = OD.office_idn
where st.date  < '03-01-2015'
	  and svd.company_name = 'MeetMe'
	group by 
			 SVD.COMPANY_NAME,
		     chr.COHORT_NAM,
			 od.OFFICE_COD,
		     od.OFFICE_NAM,
		     od.REGION_COD_NEW,
		     od.REGION_COD_OLD,
		     od.COUNTRY_NAM,
		     suj.MEDIA_TYPE_NAM,
		     st.MONTH_YEAR
UNION ALL
	Select 
		   SVD.COMPANY_NAME AS SERVICE_NAME,
		   par.COHORT_NAM as COHORT_NAME,
		   od.OFFICE_COD as OFFICE_CODE,
		   od.OFFICE_NAM as OFFICE_NAME,
		   od.REGION_COD_NEW as REGION_COD_NEW,
		   od.REGION_COD_OLD as REGION_COD_OLD,
		   od.COUNTRY_NAM as COUNTRY_NAME,
		   suj.MEDIA_TYPE_NAM as MEDIA_TYPE_NAME,
		   st.MONTH_YEAR as CONFERENCE_MONTH,
		   count(usf.chairperson_Idn) as TOTAL_PARTICIPANTS,
   		   count(distinct USF.CONFERENCE_ID) as CONFERENCE_CNT,
		   SUM(DURATION) as DURATION_SEC,
		   count(distinct usf.participant_idn) as CHAIR_PART_CNT
	  FROM BDW.SERVICE_USAGE_FAC USF
inner join bdw.service_vendor_dim svd
		on usf.service_vendor_idn = svd.service_vendor_idn
Inner join bdw.service_usage_jnk_dim suj
		on usf.SERVICE_USAGE_JNK_IDN = suj.SERVICE_USAGE_JNK_IDN
inner join bdw.DATE_DIM st
		on usf.START_DATE_IDN = st.DATE_IDN
INNER JOIN bdw.participant_dim par
		on usf.participant_idn = par.participant_idn
inner join bdw.office_dim od
		ON USF.OFFICE_IDN = OD.office_idn
where st.date < '03-01-2015'
	  and svd.company_name <> 'MeetMe'
	group by 
			 SVD.COMPANY_NAME,
		     par.COHORT_NAM,
			 od.OFFICE_COD,
		     od.OFFICE_NAM,
		     od.REGION_COD_NEW,
		     od.REGION_COD_OLD,
		     od.COUNTRY_NAM,
		     suj.MEDIA_TYPE_NAM,
		     st.MONTH_YEAR;
GO
/****** Object:  View [BDW].[vw_VIDYO_SERVICE_DETAILS]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [BDW].[vw_VIDYO_SERVICE_DETAILS] AS
	Select USF.CONFERENCE_ID,
		   SUJ.CONFERENCE_NAM,
		   USF.START_DATETIME,
		   USF.END_DATETIME,
		   USF.DURATION as DURATION_SEC,
		   USF.TOTAL_AMT,
		   SVD.COMPANY_NAME AS SERVICE_NAME,
		   par.FULL_NAM as FULL_NAME,
		   par.JOB_TITLE_NAM as JOB_TITLE_NAME,
		   par.EMAIL as EMAIL_ADDRESS,
		   par.COHORT_NAM as BCG_COHORT,
		   od.OFFICE_COD as OFFICE_CODE,
		   od.OFFICE_NAM as OFFICE_NAME,
		   od.REGION_COD_NEW,
		   od.REGION_COD_OLD,
		   od.COUNTRY_NAM as COUNTRY_NAME,
		   suj.MEDIA_TYPE_NAM as MEDIA_TYPE_NAME,
		   suj.host_flg as CHAIRPERSON_FLAG,
		   CASE
		   WHEN par.FULL_NAM like '%iRoom%' THEN 'Y'
		    ELSE 'N'
			END as iRoom_Flag,
			suj.BRIDGE_ROUTER_COD as ROUTER_LOCATION,
--			suj.ROLE as ATTENDEE_TYPE,
		  CASE
			WHEN par.email like'%@bcg.com%'  THEN 'BCGer'
			WHEN par.full_nam like '%room%'  THEN 'iRoom'
			WHEN par.full_nam like '%guest%'  THEN 'Guest'
			ELSE 'Other'
		   END as BCG_ATTENDEE_TYPE,
		   CASE
			WHEN par.FULL_NAM = 'Guest' Then 'Guest'
			When par.email like '%@bcg.com%' then 'Employee'
			ELSE 'Unkown'
		    END as GUEST_EMPLOYEE_FLAG,
			od.COUNTRY_NAM + '-' + od.OFFICE_NAM as BCG_Country_Office,
			suj.COMMUNICATION_TYPE_NAM,
			DIRECTION,
			CASE PLATFORM_NAM
			 WHEN 'Call Recorded via VidyoReplay/Recorder' THEN 'Y'
			 ELSE 'N'
			 END as RECORDED_FLAG,
			 suj.GATEWAY_ID_COD,
			 suj.GATEWAY_PREFIX_COD,
			 1 as PARTICIPANT_CNT
	  FROM BDW.SERVICE_USAGE_FAC USF
inner join bdw.service_vendor_dim svd
		on usf.service_vendor_idn = svd.service_vendor_idn
inner join bdw.participant_dim par
		on usf.PARTICIPANT_IDN = par.PARTICIPANT_IDN
inner join bdw.office_dim od
		ON usf.OFFICE_idn = OD.office_idn
Inner join bdw.service_usage_jnk_dim suj
		on usf.SERVICE_USAGE_JNK_IDN = suj.SERVICE_USAGE_JNK_IDN
INNER JOIN bdw.participant_dim chr
		on usf.chairperson_idn = chr.participant_idn
	 where USF.START_DATETIME < '03-01-2015'
	   AND svd.COMPANY_NAME ='Vidyo'
		;

GO
/****** Object:  View [BDW].[vw_SERVICE_USAGE_COUNTS_old]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [BDW].[vw_SERVICE_USAGE_COUNTS_old] AS
	Select 
		   USF.CONFERENCE_ID,
		   SUM(USF.DURATION) as TOTAL_DURATION_SEC,
		   SVD.COMPANY_NAME AS SERVICE_NAME,
		   chr.COHORT_NAM as CHR_COHORT_NAME,
		   od.OFFICE_COD as CHR_OFFICE_CODE,
		   od.OFFICE_NAM as OFFICE_NAME,
		   od.REGION_COD_NEW as CHR_REGION_COD_NEW,
		   od.REGION_COD_OLD as CHR_REGION_COD_OLD,
		   od.COUNTRY_NAM as CHR_COUNTRY_NAME,
		   suj.MEDIA_TYPE_NAM as MEDIA_TYPE_NAME,
		   st.date CONFERENCE_START_DATE,
		   CASE
			when SVD.COMPANY_NAME = 'MeetMe' THEN 1
			ELSE  count(*) 
			end as ATTENDEE_CNT
	  FROM BDW.SERVICE_USAGE_FAC USF
inner join bdw.service_vendor_dim svd
		on usf.service_vendor_idn = svd.service_vendor_idn
Inner join bdw.service_usage_jnk_dim suj
		on usf.SERVICE_USAGE_JNK_IDN = suj.SERVICE_USAGE_JNK_IDN
inner join bdw.DATE_DIM st
		on usf.START_DATE_IDN = st.DATE_IDN
INNER JOIN bdw.participant_dim chr
		on usf.chairperson_idn = chr.participant_idn
inner join bdw.office_dim od
		ON BDW.GET_OFFICE_IDN(chr.OFFICE_COD) = OD.office_idn
where st.date < '03-01-2015'
	group by usf.conference_ID,
			 SVD.COMPANY_NAME,
		     chr.COHORT_NAM,
			 od.OFFICE_COD,
		     od.OFFICE_NAM,
		     od.REGION_COD_NEW,
		     od.REGION_COD_OLD,
		     od.COUNTRY_NAM,
		     suj.MEDIA_TYPE_NAM,
		     st.date		
		;

GO
/****** Object:  View [BDW].[vw_OFFICE_COHORT_HEADCOUNT_OLD]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [BDW].[vw_OFFICE_COHORT_HEADCOUNT_OLD] AS
			Select 
					TOP 100 PERCENT
					office_cod as Office,
					cohort_nam as Cohort,
					count(distinct employee_id) as Num_Employees
			  from BDW.PARTICIPANT_DIM PD
			  WHERE pd.rec_end_date = '12-31-9999'
			   and pd.TERMINATE_DATE = '9999-12-31'
			group by office_cod,
					COHORT_NAM
			order by CASE 
						WHEN OFFICE_COD ='' THEN 'ZZZ'
						ELSE OFFICE_COD
					 END asc;

GO
/****** Object:  Table [STG].[CODE_LKUP]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[CODE_LKUP](
	[SOURCE] [varchar](20) NULL,
	[COLUMN] [varchar](20) NULL,
	[CODE] [varchar](60) NULL,
	[VALUE] [varchar](40) NULL,
 CONSTRAINT [CODE_LKUP_UNQ1] UNIQUE NONCLUSTERED 
(
	[SOURCE] ASC,
	[COLUMN] ASC,
	[CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [STG].[GET_LKUP_CODE]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [STG].[GET_LKUP_CODE](@P_SOURCE_APP VARCHAR(20),@P_COLUMN_NAME VARCHAR(20),@P_VALUE VARCHAR(40))
/*
//		Author: Jacob Leon - Slalom Consulting
//		Function: GET_LKUP_CODE
//		Date: Jan 21, 2015
//		Description: This function is passed a source name, a column name and a value and returns the corresponding 
//					 code from the CODE_LKUP table in the staging schema.
//
*/
RETURNS VARCHAR(60)
WITH SCHEMABINDING AS

 BEGIN
	DECLARE @V_CODE VARCHAR(60);

	SELECT @V_CODE = CODE
	  FROM STG.CODE_LKUP
	 WHERE [SOURCE] = @P_SOURCE_APP
	   AND [COLUMN] = @P_COLUMN_NAME
	   AND [VALUE]  = @P_VALUE
	 ;


	RETURN @V_CODE
 END;




GO
/****** Object:  Table [BDW].[RUN_AUDIT]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [BDW].[RUN_AUDIT](
	[RUN_AUDIT_IDN] [int] NOT NULL,
	[START_DATETIME] [datetime] NOT NULL,
	[END_DATETIME] [datetime] NULL,
	[COMMENTS] [varchar](2000) NULL,
	[STATUS] [varchar](2) NULL,
	[ERROR_MESSAGE] [varchar](2000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Airwatch]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Airwatch](
	[Udid] [varchar](255) NULL,
	[SerialNumber] [varchar](255) NULL,
	[MacAddress] [varchar](255) NULL,
	[Imei] [varchar](255) NULL,
	[AssetNumber] [varchar](255) NULL,
	[DeviceFriendlyName] [varchar](255) NULL,
	[UserName] [varchar](255) NULL,
	[UserEmailAddress] [varchar](255) NULL,
	[Ownership] [varchar](255) NULL,
	[Platform] [varchar](255) NULL,
	[PlatformId] [varchar](255) NULL,
	[Model] [varchar](255) NULL,
	[ModelId] [varchar](255) NULL,
	[Device Model] [varchar](255) NULL,
	[OperatingSystem] [varchar](255) NULL,
	[PhoneNumber] [varchar](255) NULL,
	[LastSeen] [varchar](255) NULL,
	[EnrollmentStatus] [varchar](255) NULL,
	[ComplianceStatus] [varchar](255) NULL,
	[CompromisedStatus] [varchar](255) NULL,
	[LastEnrolledOn] [varchar](255) NULL,
	[LastComplianceCheckOn] [varchar](255) NULL,
	[LastCompromisedCheckOn] [varchar](255) NULL,
	[SIMMCC] [varchar](255) NULL,
	[CurrentMCC] [varchar](255) NULL,
	[report_id] [varchar](255) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL,
	[build_id] [varchar](255) NULL,
	[asset_id] [varchar](255) NULL,
	[user_id] [varchar](255) NULL,
	[build_computer_name] [varchar](255) NULL,
	[asset_model_number] [varchar](255) NULL,
	[asset_serial_number] [varchar](255) NULL,
	[user_employee_id] [varchar](255) NULL,
	[user_employee_type] [varchar](255) NULL,
	[user_display_name] [varchar](255) NULL,
	[user_logon_name] [varchar](255) NULL,
	[user_department] [varchar](255) NULL,
	[user_title] [varchar](255) NULL,
	[user_host_office] [varchar](255) NULL,
	[user_region] [varchar](255) NULL,
	[user_new_region] [varchar](255) NULL,
	[user_office_system] [varchar](255) NULL,
	[user_mobile_number] [varchar](255) NULL,
	[user_phone_number] [varchar](255) NULL,
	[user_email_address] [varchar](255) NULL,
	[user_email_server] [varchar](255) NULL,
	[user_exchange_server_version] [varchar](255) NULL,
	[user_default_limits_used] [varchar](255) NULL,
	[user_limit_send] [varchar](255) NULL,
	[user_limit_send_receive] [varchar](255) NULL,
	[user_warning_limit] [varchar](255) NULL,
	[user_calendar_item_count] [varchar](255) NULL,
	[user_calendar_size] [varchar](255) NULL,
	[user_contacts_item_count] [varchar](255) NULL,
	[user_contacts_size] [varchar](255) NULL,
	[user_exchange_data_reported] [varchar](255) NULL,
	[user_inbox_item_count] [varchar](255) NULL,
	[user_inbox_size] [varchar](255) NULL,
	[user_mailbox_item_count] [varchar](255) NULL,
	[user_mailbox_size] [varchar](255) NULL,
	[user_sent_item_count] [varchar](255) NULL,
	[user_sent_size] [varchar](255) NULL,
	[Cohort] [varchar](255) NULL,
	[Top_Level] [varchar](255) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AirwatchApps]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AirwatchApps](
	[Id] [nvarchar](255) NULL,
	[ApplicationName] [nvarchar](255) NULL,
	[Version] [nvarchar](255) NULL,
	[BuildVersion] [nvarchar](255) NULL,
	[Status] [nvarchar](255) NULL,
	[Size] [nvarchar](255) NULL,
	[ApplicationIdentifier] [nvarchar](255) NULL,
	[AppType] [nvarchar](255) NULL,
	[IsManaged] [nvarchar](255) NULL,
	[DeviceID] [nvarchar](255) NULL,
	[Right_Id] [nvarchar](255) NULL,
	[Udid] [nvarchar](255) NULL,
	[SerialNumber] [nvarchar](255) NULL,
	[MacAddress] [nvarchar](255) NULL,
	[Imei] [nvarchar](255) NULL,
	[AssetNumber] [nvarchar](255) NULL,
	[DeviceFriendlyName] [nvarchar](255) NULL,
	[UserName] [nvarchar](255) NULL,
	[UserEmailAddress] [nvarchar](255) NULL,
	[Ownership] [nvarchar](255) NULL,
	[Platform] [nvarchar](255) NULL,
	[PlatformId] [nvarchar](255) NULL,
	[Model] [nvarchar](255) NULL,
	[ModelId] [float] NULL,
	[OperatingSystem] [nvarchar](255) NULL,
	[PhoneNumber] [nvarchar](255) NULL,
	[LastSeen] [nvarchar](255) NULL,
	[EnrollmentStatus] [nvarchar](255) NULL,
	[ComplianceStatus] [nvarchar](255) NULL,
	[CompromisedStatus] [nvarchar](255) NULL,
	[LastEnrolledOn] [nvarchar](255) NULL,
	[LastComplianceCheckOn] [nvarchar](255) NULL,
	[LastCompromisedCheckOn] [nvarchar](255) NULL,
	[SIMMCC] [nvarchar](255) NULL,
	[CurrentMCC] [nvarchar](255) NULL,
	[Right_Right_Platform] [nvarchar](255) NULL,
	[Device Model] [nvarchar](255) NULL,
	[Model ID] [nvarchar](255) NULL,
	[user_display_name] [nvarchar](255) NULL,
	[user_logon_name] [nvarchar](255) NULL,
	[user_first_name] [nvarchar](255) NULL,
	[user_last_name] [nvarchar](255) NULL,
	[user_employee_id] [nvarchar](255) NULL,
	[user_department] [nvarchar](255) NULL,
	[user_title] [nvarchar](255) NULL,
	[user_host_office] [nvarchar](255) NULL,
	[user_new_region] [nvarchar](255) NULL,
	[user_office_system] [nvarchar](255) NULL,
	[user_email_address] [nvarchar](255) NULL,
	[date_inserted] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AirWatchImport]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AirWatchImport](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DeviceID] [int] NULL,
	[SerialNumber] [nvarchar](60) NULL,
	[MacAddress] [nvarchar](60) NULL,
	[Imei] [nvarchar](60) NULL,
	[AssetNumber] [nvarchar](60) NULL,
	[DeviceFriendlyName] [nvarchar](60) NULL,
	[UserName] [nvarchar](60) NULL,
	[UserEmailAddress] [nvarchar](60) NULL,
	[Ownership] [nvarchar](60) NULL,
	[Platform] [nvarchar](60) NULL,
	[PlatformId] [int] NULL,
	[Model] [nvarchar](60) NULL,
	[ModelId] [int] NULL,
	[OperatingSystem] [nvarchar](60) NULL,
	[PhoneNumber] [nvarchar](60) NULL,
	[LastSeen] [datetime] NULL,
	[EnrollmentStatus] [nvarchar](60) NULL,
	[ComplianceStatus] [nvarchar](60) NULL,
	[CompromisedStatus] [bit] NULL,
	[LastEnrolledOn] [datetime] NULL,
	[LastComplianceCheckOn] [datetime] NULL,
	[LastCompromisedCheckOn] [datetime] NULL,
	[SIMMCC] [int] NULL,
	[CurrentMCC] [int] NULL,
	[CapturedDate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Alteryx_Metering]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Alteryx_Metering](
	[NetBIOS Name] [varchar](50) NULL,
	[Installed Site Code] [varchar](50) NULL,
	[Last Usage] [varchar](50) NULL,
	[Total Usages] [int] NULL,
	[Average Usages per Day] [decimal](10, 2) NULL,
	[Total Duration (min)] [decimal](10, 2) NULL,
	[Average Duration of Use (min)] [decimal](10, 2) NULL,
	[Average Duration per Day (min)] [decimal](10, 2) NULL,
	[Month] [varchar](50) NULL,
	[Application] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[BCG_SCCM_AddRemove]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BCG_SCCM_AddRemove](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResourceID] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[ProductID] [nvarchar](255) NULL,
	[DisplayName] [nvarchar](255) NULL,
	[InstallDate] [date] NULL,
	[Publisher] [nvarchar](255) NULL,
	[Version] [nvarchar](255) NULL,
	[ComputerName] [nvarchar](255) NULL,
	[CapturedDate] [datetime] NULL,
 CONSTRAINT [PK_BCG_SCCM_AddRemove] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ConferenceAnalysisReport_Jan_2015]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConferenceAnalysisReport_Jan_2015](
	["EB Number"] [varchar](50) NULL,
	["EB Name"] [varchar](50) NULL,
	["Account Number"] [varchar](50) NULL,
	["Account Name"] [varchar](50) NULL,
	["Conference Id"] [varchar](50) NULL,
	["Billing Reference"] [varchar](50) NULL,
	["Category Name"] [varchar](50) NULL,
	["Charge Type"] [varchar](50) NULL,
	["Product"] [varchar](50) NULL,
	["Chair Number"] [varchar](50) NULL,
	["Chair Name"] [varchar](50) NULL,
	["Conference Subject"] [varchar](50) NULL,
	["Scheduled Connections"] [varchar](50) NULL,
	["Scheduled Start Time"] [varchar](50) NULL,
	["Scheduled Duration"] [varchar](50) NULL,
	["Cancellation Time"] [varchar](50) NULL,
	["Booked Method"] [varchar](50) NULL,
	["employee id"] [varchar](50) NULL,
	["cost center"] [varchar](50) NULL,
	["department"] [varchar](50) NULL,
	["Title"] [varchar](50) NULL,
	["FlexField5"] [varchar](50) NULL,
	["FlexField6"] [varchar](50) NULL,
	["FlexField7"] [varchar](50) NULL,
	["FlexField8"] [varchar](50) NULL,
	["FlexField9"] [varchar](50) NULL,
	["Actual Start Time"] [varchar](50) NULL,
	["Actual End Time"] [varchar](50) NULL,
	["Total Billable Minutes"] [varchar](50) NULL,
	["Actual Connection"] [varchar](50) NULL,
	["Cancellation Amount"] [varchar](50) NULL,
	["Underport Amount"] [varchar](50) NULL,
	["Feature Amount"] [varchar](50) NULL,
	["Call Amount"] [varchar](50) NULL,
	["Discount Amount"] [varchar](50) NULL,
	["Currency"] [varchar](50) NULL,
	["Pre Tax Amount"] [varchar](50) NULL,
	["Tax Amount"] [varchar](50) NULL,
	["Total Amount"] [varchar](50) NULL,
	["Invoice String"] [varchar](50) NULL,
	["Pkg Conference Id"] [varchar](50) NULL,
	["Standard Product Name"] [varchar](50) NULL,
	[Column 42] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConferenceAnalysisReportFEB_2015]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConferenceAnalysisReportFEB_2015](
	[EB Number] [varchar](50) NULL,
	[EB Name] [varchar](50) NULL,
	[Account Number] [varchar](50) NULL,
	[Account Name] [varchar](50) NULL,
	[Conference Id] [varchar](50) NULL,
	[Billing Reference] [varchar](50) NULL,
	[Category Name] [varchar](50) NULL,
	[Charge Type] [varchar](50) NULL,
	[Product] [varchar](50) NULL,
	[Chair Number] [varchar](50) NULL,
	[Chair Name] [varchar](50) NULL,
	[Conference Subject] [varchar](50) NULL,
	[Scheduled Connections] [varchar](50) NULL,
	[Scheduled Start Time] [varchar](50) NULL,
	[Scheduled Duration] [varchar](50) NULL,
	[Cancellation Time] [varchar](50) NULL,
	[Booked Method] [varchar](50) NULL,
	[employee id] [varchar](50) NULL,
	[cost center] [varchar](50) NULL,
	[department] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[FlexField5] [varchar](50) NULL,
	[FlexField6] [varchar](50) NULL,
	[FlexField7] [varchar](50) NULL,
	[FlexField8] [varchar](50) NULL,
	[FlexField9] [varchar](50) NULL,
	[Actual Start Time] [varchar](50) NULL,
	[Actual End Time] [varchar](50) NULL,
	[Total Billable Minutes] [varchar](50) NULL,
	[Actual Connection] [varchar](50) NULL,
	[Cancellation Amount] [varchar](50) NULL,
	[Underport Amount] [varchar](50) NULL,
	[Feature Amount] [varchar](50) NULL,
	[Call Amount] [varchar](50) NULL,
	[Discount Amount] [varchar](50) NULL,
	[Currency] [varchar](50) NULL,
	[Pre Tax Amount] [varchar](50) NULL,
	[Tax Amount] [varchar](50) NULL,
	[Total Amount] [varchar](50) NULL,
	[Invoice String] [varchar](50) NULL,
	[Pkg Conference Id] [varchar](50) NULL,
	[Standard Product Name] [varchar](50) NULL,
	[Column 42] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MeetMe_Webex_TMP]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MeetMe_Webex_TMP](
	[EB Number] [varchar](50) NULL,
	[EB Name] [varchar](50) NULL,
	[Account Number] [varchar](50) NULL,
	[Account Name] [varchar](50) NULL,
	[Conference Id] [varchar](50) NULL,
	[Billing Reference] [varchar](50) NULL,
	[Category Name] [varchar](50) NULL,
	[Charge Type] [varchar](50) NULL,
	[Product] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Role] [varchar](50) NULL,
	[Phone Number] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Bridge Name] [varchar](50) NULL,
	[Call Type] [varchar](50) NULL,
	[DNIS Number] [varchar](50) NULL,
	[User Phone] [varchar](50) NULL,
	[Start Time] [varchar](50) NULL,
	[End Time] [varchar](50) NULL,
	[Duration] [varchar](50) NULL,
	[Country] [varchar](50) NULL,
	[Charge Band] [varchar](50) NULL,
	[Data Rate] [varchar](50) NULL,
	[Network Type] [varchar](50) NULL,
	[Video Service Type] [varchar](50) NULL,
	[Video Gateway Type] [varchar](50) NULL,
	[Video Mcu Type] [varchar](50) NULL,
	[Video Gatekeeper Type] [varchar](50) NULL,
	[Bridge Amount] [varchar](50) NULL,
	[Transport Amount] [varchar](50) NULL,
	[Total Amount] [varchar](50) NULL,
	[Currency] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tableau_Metering]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Tableau_Metering](
	[NetBIOS Name] [varchar](50) NULL,
	[Installed Site Code] [varchar](50) NULL,
	[Last Usage] [varchar](50) NULL,
	[Total Usages] [int] NULL,
	[Average Usages per Day] [decimal](10, 2) NULL,
	[Total Duration (min)] [decimal](10, 2) NULL,
	[Average Duration of Use (min)] [decimal](10, 2) NULL,
	[Average Duration per Day (min)] [decimal](10, 2) NULL,
	[Month] [varchar](50) NULL,
	[Application] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vidyo_TMP]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vidyo_TMP](
	[CallID] [float] NULL,
	[UniqueCallID] [float] NULL,
	[ConferenceName] [nvarchar](255) NULL,
	[TenantName] [nvarchar](255) NULL,
	[ConferenceType] [nvarchar](255) NULL,
	[EndpointType] [nvarchar](255) NULL,
	[CallerID] [nvarchar](255) NULL,
	[CallerName] [nvarchar](255) NULL,
	[JoinTime] [datetime] NULL,
	[LeaveTime] [datetime] NULL,
	[CallState] [nvarchar](255) NULL,
	[Direction] [nvarchar](255) NULL,
	[RouterID] [nvarchar](255) NULL,
	[GWID] [nvarchar](255) NULL,
	[GWPrefix] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[Airwatch_App_Status_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[Airwatch_App_Status_STG](
	[STATUS_ID] [int] NULL,
	[NAME] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[Airwatch_Models_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[Airwatch_Models_STG](
	[MODEL_ID] [int] NULL,
	[MODEL] [varchar](20) NULL,
	[PLATFORM] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[Airwatch_Ownership_Type_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[Airwatch_Ownership_Type_STG](
	[CODE] [char](1) NULL,
	[TYPE] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[Airwatch_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[Airwatch_STG](
	[Udid] [varchar](255) NULL,
	[SerialNumber] [varchar](255) NULL,
	[MacAddress] [varchar](255) NULL,
	[Imei] [varchar](255) NULL,
	[AssetNumber] [varchar](255) NULL,
	[DeviceFriendlyName] [varchar](255) NULL,
	[UserName] [varchar](255) NULL,
	[UserEmailAddress] [varchar](255) NULL,
	[Ownership] [varchar](255) NULL,
	[Platform] [varchar](255) NULL,
	[PlatformId] [varchar](255) NULL,
	[Model] [varchar](255) NULL,
	[ModelId] [varchar](255) NULL,
	[Device Model] [varchar](255) NULL,
	[OperatingSystem] [varchar](255) NULL,
	[PhoneNumber] [varchar](255) NULL,
	[LastSeen] [varchar](255) NULL,
	[EnrollmentStatus] [varchar](255) NULL,
	[ComplianceStatus] [varchar](255) NULL,
	[CompromisedStatus] [varchar](255) NULL,
	[LastEnrolledOn] [varchar](255) NULL,
	[LastComplianceCheckOn] [varchar](255) NULL,
	[LastCompromisedCheckOn] [varchar](255) NULL,
	[SIMMCC] [varchar](255) NULL,
	[CurrentMCC] [varchar](255) NULL,
	[start_date] [date] NULL,
	[end_date] [date] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[App_LIst_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[App_LIst_STG](
	[ResourceID] [nvarchar](255) NULL,
	[TimeStamp] [nvarchar](255) NULL,
	[ProdID0] [nvarchar](255) NULL,
	[DisplayName] [nvarchar](255) NULL,
	[InstallDate0] [nvarchar](255) NULL,
	[Publisher0] [nvarchar](255) NULL,
	[computerName] [nvarchar](255) NULL,
	[computerStatus] [nvarchar](255) NULL,
	[department] [nvarchar](255) NULL,
	[Right_displayName] [nvarchar](255) NULL,
	[employeeType] [nvarchar](255) NULL,
	[newRegion] [nvarchar](255) NULL,
	[officeSystem] [nvarchar](255) NULL,
	[hostOffice] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[App_Metering_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[App_Metering_STG](
	[NetBIOS Name] [varchar](50) NULL,
	[Installed Site Code] [varchar](50) NULL,
	[Last Usage] [varchar](50) NULL,
	[Total Usages] [int] NULL,
	[Average Usages per Day] [decimal](10, 2) NULL,
	[Total Duration (min)] [decimal](10, 2) NULL,
	[Average Duration of Use (min)] [decimal](10, 2) NULL,
	[Average Duration per Day (min)] [decimal](10, 2) NULL,
	[Month] [varchar](50) NULL,
	[Application] [varchar](50) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[APPLICATION_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[APPLICATION_STG](
	[APPLICATION_NAM] [varchar](260) NOT NULL,
	[PUBLISHER_NAM] [varchar](40) NULL,
	[VERSION_COD] [varchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[BCG_SCCM_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[BCG_SCCM_STG](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ResourceID] [int] NULL,
	[TimeStamp] [datetime] NULL,
	[ProductID] [nvarchar](255) NULL,
	[DisplayName] [nvarchar](255) NULL,
	[InstallDate] [date] NULL,
	[Publisher] [nvarchar](255) NULL,
	[Version] [nvarchar](255) NULL,
	[ComputerName] [nvarchar](255) NULL,
	[CapturedDate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[COMP_HARDWARE_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[COMP_HARDWARE_STG](
	[IMAGE_MODEL_DESC] [varchar](40) NOT NULL,
	[HD_MODEL_DESC] [varchar](80) NOT NULL,
	[COMP_MODEL_DESC] [varchar](40) NOT NULL,
	[WINDOWS_ARCH_DESC] [varchar](10) NOT NULL,
	[PROCESSOR_MODEL_DESC] [varchar](60) NOT NULL,
	[VIDEO_MODEL_DESC] [varchar](80) NOT NULL,
	[WIRED_NIC_MODEL_DESC] [varchar](60) NOT NULL,
	[WIRELESS_NIC_MODEL_DESC] [varchar](60) NOT NULL,
	[NAME] [varchar](20) NULL,
	[STATUS] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[ConfCDR]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[ConfCDR](
	[Conference ID] [nvarchar](255) NULL,
	[Conference Name] [nvarchar](255) NULL,
	[Conference] [nvarchar](255) NULL,
	[Endpoints] [float] NULL,
	[Tenant] [nvarchar](255) NULL,
	[Join Time] [datetime] NULL,
	[Leave Time] [datetime] NULL,
	[Duration] [datetime] NULL,
	[VidyoRouter] [float] NULL,
	[VidyoGateway] [float] NULL,
	[Call Recorded] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[Conference_Chairperson_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[Conference_Chairperson_STG](
	[conference id] [varchar](50) NULL,
	[employee id] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[MeetMe_Webex]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[MeetMe_Webex](
	[EB Number] [varchar](50) NULL,
	[EB Name] [varchar](50) NULL,
	[Account Number] [varchar](50) NULL,
	[Account Name] [varchar](50) NULL,
	[Conference Id] [varchar](50) NULL,
	[Billing Reference] [varchar](50) NULL,
	[Category Name] [varchar](50) NULL,
	[Charge Type] [varchar](50) NULL,
	[Product] [varchar](50) NULL,
	[Name] [nvarchar](65) NULL,
	[Role] [varchar](50) NULL,
	[Phone Number] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Bridge Name] [varchar](50) NULL,
	[Call Type] [varchar](50) NULL,
	[DNIS Number] [varchar](50) NULL,
	[User Phone] [varchar](50) NULL,
	[Start Time] [datetime] NULL,
	[End Time] [datetime] NULL,
	[Duration] [numeric](18, 0) NULL,
	[Country] [varchar](50) NULL,
	[Charge Band] [varchar](50) NULL,
	[Data Rate] [varchar](50) NULL,
	[Network Type] [varchar](50) NULL,
	[Video Service Type] [varchar](50) NULL,
	[Video Gateway Type] [varchar](50) NULL,
	[Video Mcu Type] [varchar](50) NULL,
	[Video Gatekeeper Type] [varchar](50) NULL,
	[Bridge Amount] [numeric](8, 2) NULL,
	[Transport Amount] [numeric](8, 2) NULL,
	[Total Amount] [numeric](8, 2) NULL,
	[Currency] [varchar](50) NULL,
	[StartTime_dt]  AS (CONVERT([date],[Start Time])),
	[EndTime_dt]  AS (CONVERT([date],[End Time])),
	[Call_Type_def]  AS (case when [Call Type]='' then 'Undefined' when [Call Type] IS NULL then 'Undefined' else [Call Type] end),
	[Bridge_Name_def]  AS (case when [Bridge Name]='' then 'Undefined' when [Bridge Name] IS NULL then 'Undefined' else [Bridge Name] end),
	[Country_def]  AS (case when [Country]='' then 'Undefined' when [Country] IS NULL then 'Undefined' else [Country] end),
	[Charge_Band_def]  AS (case when [Charge Band]='' then 'Undefined' when [Charge Band] IS NULL then 'Undefined' else [Charge Band] end),
	[Role_def]  AS (case when [Role]='Chairperson' then 'Y' else 'N' end),
	[Email_def]  AS (case [email] when '' then 'Undefined' else [email] end),
	[Product_def]  AS (case [Product] when 'WebEx' then 'Video' when 'MeetMe' then 'Audio'  end)
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[MOBILE_DEVICE_JNK_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[MOBILE_DEVICE_JNK_STG](
	[DEVICE_OWNERSHIP] [varchar](10) NOT NULL,
	[ENROLLMENT_STATUS] [varchar](30) NOT NULL,
	[COMPLIANCE_STATUS] [varchar](40) NOT NULL,
	[COMPROMISED_STATUS] [varchar](10) NOT NULL,
	[APP_ISMANAGED] [varchar](10) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[MOBILE_DEVICE_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[MOBILE_DEVICE_STG](
	[PLATFORM_NAM] [varchar](20) NOT NULL,
	[MODEL_NAM] [varchar](60) NOT NULL,
	[DEVICE_NAM] [varchar](20) NOT NULL,
	[OS_VERSION_NAM] [varchar](20) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[MS_Applications_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[MS_Applications_STG](
	[ResourceID] [nvarchar](255) NULL,
	[TimeStamp] [nvarchar](255) NULL,
	[DisplayName] [nvarchar](255) NULL,
	[InstallDate0] [nvarchar](255) NULL,
	[Publisher0] [nvarchar](255) NULL,
	[computerName] [nvarchar](255) NULL,
	[agentLastRun] [datetime] NULL,
	[computerStatus] [nvarchar](255) NULL,
	[department] [nvarchar](255) NULL,
	[Right_displayName] [nvarchar](255) NULL,
	[employeeType] [nvarchar](255) NULL,
	[newRegion] [nvarchar](255) NULL,
	[officeSystem] [nvarchar](255) NULL,
	[hostOffice] [nvarchar](255) NULL,
	[IEVersion] [float] NULL,
	[MSOfficeVersion] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[OctCDRCompleted]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[OctCDRCompleted](
	[CallID] [nvarchar](255) NULL,
	[UniqueCallID] [nvarchar](255) NULL,
	[ConferenceName] [nvarchar](255) NULL,
	[TenantName] [nvarchar](255) NULL,
	[ConferenceType] [nvarchar](255) NULL,
	[EndpointType] [nvarchar](255) NULL,
	[CallerID] [nvarchar](255) NULL,
	[CallerName] [nvarchar](255) NULL,
	[JoinTime] [nvarchar](255) NULL,
	[LeaveTime] [nvarchar](255) NULL,
	[Duration] [datetime] NULL,
	[CallState] [nvarchar](255) NULL,
	[Direction] [nvarchar](255) NULL,
	[RouterID] [nvarchar](255) NULL,
	[GWID] [nvarchar](255) NULL,
	[GWPrefix] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[OFFICE_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[OFFICE_STG](
	[OFFICE_COD] [char](3) NULL,
	[OFFICE_NAM] [varchar](40) NOT NULL,
	[REGION_COD_NEW] [varchar](10) NOT NULL,
	[REGION_COD_OLD] [varchar](10) NOT NULL,
	[COUNTRY_NAM] [varchar](40) NOT NULL,
	[SYSTEM_NAM] [varchar](40) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[PARTICIPANT_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[PARTICIPANT_STG](
	[EMPLOYEE_ID] [bigint] NULL,
	[FIRST_NAM] [nvarchar](65) NULL,
	[LAST_NAM] [nvarchar](65) NULL,
	[FULL_NAM] [nvarchar](65) NULL,
	[OFFICE_COD] [char](3) NOT NULL,
	[JOB_TITLE_NAM] [varchar](100) NOT NULL,
	[EMAIL] [varchar](50) NOT NULL,
	[JOB_NAM] [varchar](30) NOT NULL,
	[ASSISTANT_NAM] [varchar](80) NOT NULL,
	[HIRE_DATE] [date] NOT NULL,
	[TERMINATE_DATE] [date] NOT NULL,
	[COHORT_NAM] [varchar](50) NOT NULL,
	[VIDYO_INSTLD_IND] [char](1) NOT NULL,
	[WEBEX_INSTLD_IND] [char](1) NOT NULL,
	[LYNC_INSTLD_IND] [char](1) NOT NULL,
	[REC_START_DATE] [date] NOT NULL,
	[REC_END_DATE] [date] NOT NULL,
	[EMPLOYEE_TYPE_NAM] [varchar](40) NOT NULL,
	[DEPARTMENT_NAM] [varchar](40) NOT NULL,
 CONSTRAINT [PARTICIPANT_STG_IDX_UNQ01] UNIQUE NONCLUSTERED 
(
	[FULL_NAM] ASC,
	[EMAIL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[SERVICE_USAGE_JNK_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[SERVICE_USAGE_JNK_STG](
	[ROLE] [varchar](15) NOT NULL,
	[PLATFORM_NAM] [varchar](40) NULL,
	[CONFERENCE_NAM] [varchar](80) NULL,
	[COMMUNICATION_TYPE_NAM] [varchar](50) NULL,
	[BRIDGE_ROUTER_COD] [varchar](20) NULL,
	[CALL_STATE] [varchar](20) NULL,
	[DIRECTION] [varchar](20) NULL,
	[CHARGE_COUNTRY_COD] [varchar](9) NULL,
	[CHARGE_COUNTRY_NAM] [varchar](30) NULL,
	[HOST_FLG] [char](1) NULL,
	[MEDIA_TYPE_NAM] [varchar](10) NULL,
	[GATEWAY_ID_COD] [varchar](20) NULL,
	[GATEWAY_PREFIX_COD] [varchar](20) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[SERVICE_VENDOR_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[SERVICE_VENDOR_STG](
	[COMPANY_NAME] [varchar](20) NULL,
	[TYPE] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[SessionDetailsView_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[SessionDetailsView_STG](
	[SessionIdTime] [datetime] NOT NULL,
	[SessionIdSeq] [int] NOT NULL,
	[InviteTime] [datetime] NULL,
	[FromUri] [varchar](100) NOT NULL,
	[ToUri] [varchar](100) NOT NULL,
	[FromUriType] [nvarchar](256) NULL,
	[ToUriType] [nvarchar](256) NULL,
	[FromTenant] [uniqueidentifier] NULL,
	[ToTenant] [uniqueidentifier] NULL,
	[FromEndpointId] [uniqueidentifier] NULL,
	[ToEndpointId] [uniqueidentifier] NULL,
	[EndTime] [datetime] NULL,
	[FromMessageCount] [int] NULL,
	[ToMessageCount] [int] NULL,
	[FromClientVersion] [nvarchar](256) NULL,
	[FromClientType] [int] NULL,
	[FromClientCategory] [nvarchar](64) NULL,
	[ToClientVersion] [nvarchar](256) NULL,
	[ToClientType] [int] NULL,
	[ToClientCategory] [nvarchar](64) NULL,
	[TargetUri] [nvarchar](450) NULL,
	[TargetUriType] [nvarchar](256) NULL,
	[TargetTenant] [uniqueidentifier] NULL,
	[OnBehalfOfUri] [nvarchar](450) NULL,
	[OnBehalfOfUriType] [nvarchar](256) NULL,
	[OnBehalfOfTenant] [uniqueidentifier] NULL,
	[ReferredByUri] [nvarchar](450) NULL,
	[ReferredByUriType] [nvarchar](256) NULL,
	[ReferredByTenant] [uniqueidentifier] NULL,
	[DialogId] [varchar](max) NULL,
	[CorrelationId] [uniqueidentifier] NULL,
	[ReplacesDialogIdTime] [datetime] NULL,
	[ReplacesDialogIdSeq] [int] NULL,
	[ReplacesDialogId] [varchar](max) NULL,
	[ResponseTime] [datetime] NULL,
	[ResponseCode] [int] NULL,
	[DiagnosticId] [int] NULL,
	[ContentType] [nvarchar](256) NULL,
	[FrontEnd] [nvarchar](257) NULL,
	[Pool] [nvarchar](257) NULL,
	[FromEdgeServer] [nvarchar](257) NULL,
	[ToEdgeServer] [nvarchar](257) NULL,
	[IsFromInternal] [bit] NULL,
	[IsToInternal] [bit] NULL,
	[CallPriority] [nvarchar](256) NULL,
	[MediaTypes] [varchar](10) NULL,
	[FromUserFlag] [smallint] NOT NULL,
	[ToUserFlag] [smallint] NOT NULL,
	[CallFlag] [smallint] NOT NULL,
	[Location] [nvarchar](max) NULL,
 CONSTRAINT [SessionDetailsView_STG_PK] PRIMARY KEY CLUSTERED 
(
	[SessionIdTime] ASC,
	[SessionIdSeq] ASC,
	[FromUri] ASC,
	[ToUri] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[tb_CACHE_Daily_Details_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_CACHE_Daily_Details_STG](
	[detail_id] [bigint] NOT NULL,
	[build_id] [int] NULL,
	[user_id] [int] NULL,
	[asset_id] [int] NULL,
	[last_updated] [nvarchar](255) NULL,
	[build_status] [nvarchar](255) NULL,
	[user_employee_id] [nvarchar](255) NULL,
	[user_employee_type] [nvarchar](255) NULL,
	[user_display_name] [nvarchar](255) NULL,
	[user_first_name] [nvarchar](255) NULL,
	[user_last_name] [nvarchar](255) NULL,
	[user_logon_name] [nvarchar](255) NULL,
	[user_department] [nvarchar](255) NULL,
	[user_title] [nvarchar](255) NULL,
	[user_host_office] [nvarchar](255) NULL,
	[user_region] [nvarchar](255) NULL,
	[user_new_region] [nvarchar](255) NULL,
	[user_office_system] [nvarchar](255) NULL,
	[user_mobile_number] [nvarchar](255) NULL,
	[user_phone_number] [nvarchar](255) NULL,
	[user_assistant] [nvarchar](255) NULL,
	[user_start_date] [nvarchar](255) NULL,
	[user_term_date] [nvarchar](255) NULL,
	[user_user_creation_date] [nvarchar](255) NULL,
	[user_user_retirement_date] [nvarchar](255) NULL,
	[user_user_ou] [nvarchar](255) NULL,
	[user_email_address] [nvarchar](255) NULL,
	[user_email_server] [nvarchar](255) NULL,
	[user_exchange_server_version] [nvarchar](255) NULL,
	[user_default_limits_used] [nvarchar](255) NULL,
	[user_limit_send] [nvarchar](255) NULL,
	[user_limit_send_receive] [nvarchar](255) NULL,
	[user_warning_limit] [nvarchar](255) NULL,
	[user_calendar_item_count] [nvarchar](255) NULL,
	[user_calendar_size] [nvarchar](255) NULL,
	[user_contacts_item_count] [nvarchar](255) NULL,
	[user_contacts_size] [nvarchar](255) NULL,
	[user_exchange_data_reported] [nvarchar](255) NULL,
	[user_inbox_item_count] [nvarchar](255) NULL,
	[user_inbox_size] [nvarchar](255) NULL,
	[user_mailbox_item_count] [nvarchar](255) NULL,
	[user_mailbox_size] [nvarchar](255) NULL,
	[user_sent_item_count] [nvarchar](255) NULL,
	[user_sent_size] [nvarchar](255) NULL,
	[user_user_last_logon] [nvarchar](255) NULL,
	[build_computer_name] [nvarchar](255) NULL,
	[build_computer_creation_date] [nvarchar](255) NULL,
	[build_computer_retirement_date] [nvarchar](255) NULL,
	[build_computer_office_ou] [nvarchar](255) NULL,
	[build_computer_ou] [nvarchar](255) NULL,
	[build_computer_status] [nvarchar](255) NULL,
	[build_operating_system] [nvarchar](255) NULL,
	[build_ie_full_version] [nvarchar](255) NULL,
	[build_ie_version] [nvarchar](255) NULL,
	[build_pagefile] [nvarchar](255) NULL,
	[build_crashplan_first_backup] [nvarchar](255) NULL,
	[build_crashplan_guid] [nvarchar](255) NULL,
	[build_encryption_status] [nvarchar](255) NULL,
	[build_bcg_build_date] [nvarchar](255) NULL,
	[build_bcg_build_version] [nvarchar](255) NULL,
	[build_image_model] [nvarchar](255) NULL,
	[build_image_part_number] [nvarchar](255) NULL,
	[build_image_serial] [nvarchar](255) NULL,
	[build_image_task_id] [nvarchar](255) NULL,
	[build_image_task_version] [nvarchar](255) NULL,
	[build_image_technician] [nvarchar](255) NULL,
	[build_image_user] [nvarchar](255) NULL,
	[build_hard_drive_firmware] [nvarchar](255) NULL,
	[build_hard_drive_model] [nvarchar](255) NULL,
	[build_hard_drive_size] [nvarchar](255) NULL,
	[build_clone_utility_completed] [nvarchar](255) NULL,
	[build_clone_utility_in_progress] [nvarchar](255) NULL,
	[build_date_cloned] [nvarchar](255) NULL,
	[build_hard_drive_cloned] [nvarchar](255) NULL,
	[build_ms_office_version] [nvarchar](255) NULL,
	[build_outlook_caching] [nvarchar](255) NULL,
	[build_calendar_caching] [nvarchar](255) NULL,
	[build_public_folders_caching] [nvarchar](255) NULL,
	[build_imd_auto_enable] [nvarchar](255) NULL,
	[build_imd_disabled] [nvarchar](255) NULL,
	[build_imd_load_behavior] [nvarchar](255) NULL,
	[build_imd_version] [nvarchar](255) NULL,
	[build_video_driver] [nvarchar](255) NULL,
	[build_wired_nic_driver] [nvarchar](255) NULL,
	[build_wireless_nic_driver] [nvarchar](255) NULL,
	[build_imd_load_behavior_hkcu] [nvarchar](255) NULL,
	[build_image_reason] [nvarchar](255) NULL,
	[build_training_date] [nvarchar](255) NULL,
	[build_handover_date] [nvarchar](255) NULL,
	[build_agent_version] [nvarchar](255) NULL,
	[build_agent_last_run] [nvarchar](255) NULL,
	[build_outlook_scan_last_run] [nvarchar](255) NULL,
	[build_computer_last_logon] [nvarchar](255) NULL,
	[build_last_restart] [nvarchar](255) NULL,
	[build_crashplan_last_activity] [nvarchar](255) NULL,
	[build_crashplan_last_backup] [nvarchar](255) NULL,
	[build_hard_drive_available] [nvarchar](255) NULL,
	[build_hard_drive_free] [nvarchar](255) NULL,
	[build_windows_stability_index] [nvarchar](255) NULL,
	[build_registry_corrupt] [nvarchar](255) NULL,
	[build_last_registry_corrupt_date] [nvarchar](255) NULL,
	[build_colligo_version] [nvarchar](255) NULL,
	[build_icloud_folder_size] [nvarchar](255) NULL,
	[build_sccm_installed] [nvarchar](255) NULL,
	[build_active_power_scheme] [nvarchar](255) NULL,
	[build_crashplan_backup_size] [nvarchar](255) NULL,
	[build_crashplan_backup_remaining] [nvarchar](255) NULL,
	[build_crashplan_version] [nvarchar](255) NULL,
	[build_crashplan_install_date] [nvarchar](255) NULL,
	[build_crashplan_office] [nvarchar](255) NULL,
	[build_laptop_resolution] [nvarchar](255) NULL,
	[build_DCOM_enabled] [nvarchar](255) NULL,
	[build_windows_architecture] [nvarchar](255) NULL,
	[build_disk_controller_driver] [nvarchar](255) NULL,
	[build_disk_status] [nvarchar](255) NULL,
	[asset_model_number] [nvarchar](255) NULL,
	[asset_serial_number] [nvarchar](255) NULL,
	[asset_computer_model] [nvarchar](255) NULL,
	[asset_purchase_date] [nvarchar](255) NULL,
	[asset_warranty_expiration] [nvarchar](255) NULL,
	[asset_memory] [nvarchar](255) NULL,
	[asset_bios_release_date] [nvarchar](255) NULL,
	[asset_bios_version] [nvarchar](255) NULL,
	[asset_processor_model] [nvarchar](255) NULL,
	[asset_video_model] [nvarchar](255) NULL,
	[asset_wired_nic_model] [nvarchar](255) NULL,
	[asset_wireless_nic_model] [nvarchar](255) NULL,
	[asset_chassis_type] [nvarchar](255) NULL,
	[asset_battery_capacity] [nvarchar](255) NULL,
	[asset_battery_capacity_2] [nvarchar](255) NULL,
	[asset_battery_1_name] [nvarchar](255) NULL,
	[asset_battery_1_serial] [nvarchar](255) NULL,
	[asset_battery_2_name] [nvarchar](255) NULL,
	[asset_battery_2_serial] [nvarchar](255) NULL,
	[asset_sim_card_number] [nvarchar](255) NULL,
	[asset_disk_controller] [nvarchar](255) NULL,
	[total_pst_count] [nvarchar](255) NULL,
	[open_pst_count] [nvarchar](255) NULL,
	[pst_over_4gb] [nvarchar](255) NULL,
	[bsod_count] [nvarchar](255) NULL,
	[crash_count] [nvarchar](255) NULL,
	[excel_count] [nvarchar](255) NULL,
	[powerpoint_count] [nvarchar](255) NULL,
	[outlook_count] [nvarchar](255) NULL,
	[word_count] [nvarchar](255) NULL,
	[ie_count] [nvarchar](255) NULL,
	[date_inserted] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Ref_Cohort_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_Ref_Cohort_STG](
	[Department] [nvarchar](255) NOT NULL,
	[Cohort] [nvarchar](255) NOT NULL,
	[Top_Level] [nvarchar](5) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Ref_Country_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_Ref_Country_STG](
	[country_id] [int] NOT NULL,
	[country] [nvarchar](50) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Ref_Office_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_Ref_Office_STG](
	[office_id] [int] NOT NULL,
	[office_short_name] [nvarchar](10) NOT NULL,
	[office_long_name] [nvarchar](200) NULL,
	[region_id] [int] NOT NULL,
	[new_region_id] [int] NULL,
	[office_system_id] [int] NULL,
	[country_id] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Ref_Office_system_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_Ref_Office_system_STG](
	[system_id] [int] NOT NULL,
	[system_short_name] [nvarchar](10) NULL,
	[system_long_name] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Ref_Region_New_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_Ref_Region_New_STG](
	[new_region_id] [int] NOT NULL,
	[new_region] [nvarchar](10) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Ref_Region_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[tb_Ref_Region_STG](
	[region_id] [int] NOT NULL,
	[region_short_name] [nvarchar](10) NULL,
	[region_long_name] [nvarchar](200) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [STG].[tb_Working_Data_User_Header_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[tb_Working_Data_User_Header_STG](
	[user_header_id] [bigint] NOT NULL,
	[user_id] [int] NULL,
	[date] [date] NULL,
	[date_inserted] [datetime] NULL,
	[employee_id] [bigint] NULL,
	[employee_type] [varchar](25) NULL,
	[display_name] [nvarchar](255) NULL,
	[first_name] [nvarchar](255) NULL,
	[last_name] [nvarchar](255) NULL,
	[logon_name] [nvarchar](255) NULL,
	[department] [nvarchar](255) NULL,
	[title] [nvarchar](255) NULL,
	[host_office] [nvarchar](255) NULL,
	[region] [nvarchar](255) NULL,
	[mobile_number] [nvarchar](255) NULL,
	[phone_number] [nvarchar](255) NULL,
	[assistant] [nvarchar](255) NULL,
	[start_date] [date] NULL,
	[term_date] [date] NULL,
	[user_creation_date] [datetime] NULL,
	[user_retirement_date] [datetime] NULL,
	[user_ou] [nvarchar](255) NULL,
	[email_address] [varchar](50) NULL,
	[email_server] [nvarchar](255) NULL,
	[exchange_server_version] [int] NULL,
	[default_limits_used] [bit] NULL,
	[limit_send] [int] NULL,
	[limit_send_receive] [int] NULL,
	[warning_limit] [int] NULL,
	[is_most_recent] [bit] NULL,
	[employee_type_def]  AS (coalesce([employee_type],'Undef'))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[VIDYO_STG]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [STG].[VIDYO_STG](
	[CallID] [int] NULL,
	[UniqueCallID] [bigint] NULL,
	[ConferenceName] [nvarchar](255) NULL,
	[TenantName] [nvarchar](255) NULL,
	[ConferenceType] [nvarchar](255) NULL,
	[EndpointType] [nvarchar](255) NULL,
	[CallerID] [nvarchar](255) NULL,
	[CallerName] [nvarchar](255) NULL,
	[JoinTime] [datetime] NULL,
	[LeaveTime] [datetime] NULL,
	[CallState] [nvarchar](255) NULL,
	[Direction] [nvarchar](255) NULL,
	[RouterID] [nvarchar](255) NULL,
	[GWID] [nvarchar](255) NULL,
	[GWPrefix] [nvarchar](255) NULL,
	[JoinTime_dt]  AS (CONVERT([date],[JoinTime])),
	[LeaveTime_dt]  AS (CONVERT([date],[LeaveTime])),
	[Email_def]  AS (case when charindex('@',[CallerID])>(0) AND charindex('@',[CallerID]) IS NOT NULL then [CallerID] else 'Undefined' end),
	[CallerID_def]  AS (case when charindex('@',[CallerID])>(0) AND charindex('@',[CallerID]) IS NOT NULL then CONVERT([varchar](50),[callerID]) else 'Undefined' end) PERSISTED,
	[CallerName_def]  AS (CONVERT([nvarchar](50),[CallerName]))
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [STG].[VIDYO_STG_BKUP]    Script Date: 3/13/2015 4:53:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [STG].[VIDYO_STG_BKUP](
	[CallID] [float] NULL,
	[UniqueCallID] [bigint] NULL,
	[ConferenceName] [nvarchar](255) NULL,
	[TenantName] [nvarchar](255) NULL,
	[ConferenceType] [nvarchar](255) NULL,
	[EndpointType] [nvarchar](255) NULL,
	[CallerID] [nvarchar](255) NULL,
	[CallerName] [nvarchar](255) NULL,
	[JoinTime] [datetime] NULL,
	[LeaveTime] [datetime] NULL,
	[CallState] [nvarchar](255) NULL,
	[Direction] [nvarchar](255) NULL,
	[RouterID] [nvarchar](255) NULL,
	[GWID] [nvarchar](255) NULL,
	[GWPrefix] [nvarchar](255) NULL,
	[JoinTime_dt] [date] NULL,
	[LeaveTime_dt] [date] NULL,
	[Email_def] [nvarchar](255) NULL
) ON [PRIMARY]

GO
/****** Object:  Index [SERVICE_USAGE_FAC_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE CLUSTERED INDEX [SERVICE_USAGE_FAC_IDX_NUN01] ON [BDW].[SERVICE_USAGE_FAC]
(
	[START_DATETIME] ASC,
	[SERVICE_VENDOR_IDN] ASC,
	[PARTICIPANT_IDN] ASC,
	[SERVICE_USAGE_JNK_IDN] ASC,
	[OFFICE_IDN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [APP_METERING_STG_IDX_UNQ01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE UNIQUE CLUSTERED INDEX [APP_METERING_STG_IDX_UNQ01] ON [STG].[App_Metering_STG]
(
	[NetBIOS Name] ASC,
	[Month] ASC,
	[Application] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN07]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE CLUSTERED INDEX [MeetMe_Webex_IDX_NUN07] ON [STG].[MeetMe_Webex]
(
	[Conference Id] ASC,
	[Name] ASC,
	[Email_def] ASC,
	[StartTime_dt] ASC,
	[EndTime_dt] ASC,
	[Product] ASC,
	[Role] ASC,
	[Category Name] ASC,
	[Call_Type_def] ASC,
	[Bridge_Name_def] ASC,
	[Country_def] ASC,
	[Charge_Band_def] ASC,
	[Role_def] ASC,
	[Product_def] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Working_Data_User_Hdr_STG_IDX_UNQ01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE UNIQUE CLUSTERED INDEX [Working_Data_User_Hdr_STG_IDX_UNQ01] ON [STG].[tb_Working_Data_User_Header_STG]
(
	[employee_id] ASC,
	[date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [DATE_DIM_IDX_UNQ01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [DATE_DIM_IDX_UNQ01] ON [BDW].[DATE_DIM]
(
	[DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [PARTICIPANT_DIM_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [PARTICIPANT_DIM_IDX_NUN01] ON [BDW].[PARTICIPANT_DIM]
(
	[EMAIL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [PARTICIPANT_DIM_IDX_NUN02]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [PARTICIPANT_DIM_IDX_NUN02] ON [BDW].[PARTICIPANT_DIM]
(
	[EMPLOYEE_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SERVICE_USAGE_JNK_DIM_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [SERVICE_USAGE_JNK_DIM_IDX_NUN01] ON [BDW].[SERVICE_USAGE_JNK_DIM]
(
	[ROLE] ASC,
	[CHARGE_COUNTRY_COD] ASC,
	[CHARGE_COUNTRY_NAM] ASC,
	[MEDIA_TYPE_NAM] ASC
)
INCLUDE ( 	[SERVICE_USAGE_JNK_IDN],
	[PLATFORM_NAM],
	[CONFERENCE_NAM],
	[COMMUNICATION_TYPE_NAM],
	[BRIDGE_ROUTER_COD],
	[CALL_STATE],
	[DIRECTION],
	[HOST_FLG]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SERVICE_USAGE_JNK_DIM_IDX_NUN02]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [SERVICE_USAGE_JNK_DIM_IDX_NUN02] ON [BDW].[SERVICE_USAGE_JNK_DIM]
(
	[ROLE] ASC,
	[CHARGE_COUNTRY_COD] ASC,
	[CHARGE_COUNTRY_NAM] ASC,
	[HOST_FLG] ASC,
	[MEDIA_TYPE_NAM] ASC,
	[CONFERENCE_NAM] ASC,
	[CALL_STATE] ASC
)
INCLUDE ( 	[SERVICE_USAGE_JNK_IDN],
	[PLATFORM_NAM],
	[COMMUNICATION_TYPE_NAM],
	[BRIDGE_ROUTER_COD],
	[DIRECTION]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SERVICE_USAGE_JNK_DIM_IDX_NUN03]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [SERVICE_USAGE_JNK_DIM_IDX_NUN03] ON [BDW].[SERVICE_USAGE_JNK_DIM]
(
	[CONFERENCE_NAM] ASC,
	[CALL_STATE] ASC,
	[DIRECTION] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [SERVICE_USAGE_JNK_DIM_IDX_UNQ01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [SERVICE_USAGE_JNK_DIM_IDX_UNQ01] ON [BDW].[SERVICE_USAGE_JNK_DIM]
(
	[ROLE] ASC,
	[PLATFORM_NAM] ASC,
	[CONFERENCE_NAM] ASC,
	[COMMUNICATION_TYPE_NAM] ASC,
	[BRIDGE_ROUTER_COD] ASC,
	[CALL_STATE] ASC,
	[DIRECTION] ASC,
	[CHARGE_COUNTRY_COD] ASC,
	[CHARGE_COUNTRY_NAM] ASC,
	[HOST_FLG] ASC,
	[MEDIA_TYPE_NAM] ASC,
	[GATEWAY_ID_COD] ASC,
	[GATEWAY_PREFIX_COD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Conference_Chairperson_STG_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [Conference_Chairperson_STG_IDX_NUN01] ON [STG].[Conference_Chairperson_STG]
(
	[conference id] ASC
)
INCLUDE ( 	[employee id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [MeetMe_Webex_IDX_NUN01] ON [STG].[MeetMe_Webex]
(
	[StartTime_dt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN02]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [MeetMe_Webex_IDX_NUN02] ON [STG].[MeetMe_Webex]
(
	[EndTime_dt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN03]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [MeetMe_Webex_IDX_NUN03] ON [STG].[MeetMe_Webex]
(
	[Call_Type_def] ASC,
	[Bridge_Name_def] ASC,
	[Country_def] ASC,
	[Charge_Band_def] ASC,
	[Role_def] ASC,
	[Product_def] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN04]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [MeetMe_Webex_IDX_NUN04] ON [STG].[MeetMe_Webex]
(
	[Name] ASC,
	[Email] ASC,
	[End Time] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN05]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [MeetMe_Webex_IDX_NUN05] ON [STG].[MeetMe_Webex]
(
	[Email_def] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [MeetMe_Webex_IDX_NUN06]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [MeetMe_Webex_IDX_NUN06] ON [STG].[MeetMe_Webex]
(
	[Product_def] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Working_Data_User_Hdr_STG_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [Working_Data_User_Hdr_STG_IDX_NUN01] ON [STG].[tb_Working_Data_User_Header_STG]
(
	[email_address] ASC,
	[date] ASC,
	[employee_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Working_Data_User_Hdr_STG_IDX_NUN02]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [Working_Data_User_Hdr_STG_IDX_NUN02] ON [STG].[tb_Working_Data_User_Header_STG]
(
	[email_address] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [Working_Data_User_Hdr_STG_IDX_NUN03]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [Working_Data_User_Hdr_STG_IDX_NUN03] ON [STG].[tb_Working_Data_User_Header_STG]
(
	[employee_type_def] ASC
)
INCLUDE ( 	[email_address]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Working_Data_User_Hdr_STG_IDX_NUN04]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [Working_Data_User_Hdr_STG_IDX_NUN04] ON [STG].[tb_Working_Data_User_Header_STG]
(
	[email_address] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [VIDYO_STG_IDX_NUN01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [VIDYO_STG_IDX_NUN01] ON [STG].[VIDYO_STG]
(
	[JoinTime_dt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [VIDYO_STG_IDX_NUN02]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [VIDYO_STG_IDX_NUN02] ON [STG].[VIDYO_STG]
(
	[LeaveTime_dt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Vidyo_STG_IDX_NUN03]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [Vidyo_STG_IDX_NUN03] ON [STG].[VIDYO_STG]
(
	[CallerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ARITHABORT ON
SET CONCAT_NULL_YIELDS_NULL ON
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET NUMERIC_ROUNDABORT OFF

GO
/****** Object:  Index [VIDYO_STG_IDX_NUN04]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE NONCLUSTERED INDEX [VIDYO_STG_IDX_NUN04] ON [STG].[VIDYO_STG]
(
	[CallerName] ASC,
	[CallerID_def] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [VIDYO_STG_IDX_UNQ01]    Script Date: 3/13/2015 4:53:24 PM ******/
CREATE UNIQUE NONCLUSTERED INDEX [VIDYO_STG_IDX_UNQ01] ON [STG].[VIDYO_STG]
(
	[CallID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [BDW].[APPLICATION_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[APPLICATION_IDN_S]) FOR [APPLICATION_IDN]
GO
ALTER TABLE [BDW].[COMP_HARDWARE_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[COMP_HARDWARE_IDN_S]) FOR [COMP_HARDWARE_IDN]
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[MOBILE_DEVICE_IDN_S]) FOR [MOBILE_DEVICE_IDN]
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_JNK_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[MOBILE_DEVICE_JNK_IDN_S]) FOR [MOBILE_DEVICE_JNK_IDN]
GO
ALTER TABLE [BDW].[OFFICE_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[OFFICE_IDN_S]) FOR [OFFICE_IDN]
GO
ALTER TABLE [BDW].[PARTICIPANT_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[PARTICIPANT_IDN_S]) FOR [PARTICIPANT_IDN]
GO
ALTER TABLE [BDW].[PUBLISHER_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[PUBLISHER_IDN_S]) FOR [PUBLISHER_IDN]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] ADD  CONSTRAINT [CHAIRPERSON_DEFAULT_CONST]  DEFAULT ((0)) FOR [CHAIRPERSON_IDN]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_JNK_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[SERVICE_USAGE_JNK_IDN_S]) FOR [SERVICE_USAGE_JNK_IDN]
GO
ALTER TABLE [BDW].[SERVICE_VENDOR_DIM] ADD  DEFAULT (NEXT VALUE FOR [BDW].[SERVICE_VENDOR_IDN_S]) FOR [SERVICE_VENDOR_IDN]
GO
ALTER TABLE [BDW].[APP_METERING_FAC]  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_COMP_HRDWRE_DIM_FK] FOREIGN KEY([COMP_HARDWARE_IDN])
REFERENCES [BDW].[COMP_HARDWARE_DIM] ([COMP_HARDWARE_IDN])
GO
ALTER TABLE [BDW].[APP_METERING_FAC] CHECK CONSTRAINT [APP_MET_FAC_COMP_HRDWRE_DIM_FK]
GO
ALTER TABLE [BDW].[APP_METERING_FAC]  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_LSTUSG_DATE_DIM_FK] FOREIGN KEY([LAST_USAGE_DATE_IDN])
REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])
GO
ALTER TABLE [BDW].[APP_METERING_FAC] CHECK CONSTRAINT [APP_MET_FAC_LSTUSG_DATE_DIM_FK]
GO
ALTER TABLE [BDW].[APP_METERING_FAC]  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_MONTH_DIM_FK] FOREIGN KEY([MONTH_IDN])
REFERENCES [BDW].[MONTH_DIM] ([MONTH_IDN])
GO
ALTER TABLE [BDW].[APP_METERING_FAC] CHECK CONSTRAINT [APP_MET_FAC_MONTH_DIM_FK]
GO
ALTER TABLE [BDW].[APP_METERING_FAC]  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_OFFICE_DIM_FK] FOREIGN KEY([OFFICE_IDN])
REFERENCES [BDW].[OFFICE_DIM] ([OFFICE_IDN])
GO
ALTER TABLE [BDW].[APP_METERING_FAC] CHECK CONSTRAINT [APP_MET_FAC_OFFICE_DIM_FK]
GO
ALTER TABLE [BDW].[APP_METERING_FAC]  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_PARTICIPANT_DIM_FK] FOREIGN KEY([PARTICIPANT_IDN])
REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
GO
ALTER TABLE [BDW].[APP_METERING_FAC] CHECK CONSTRAINT [APP_MET_FAC_PARTICIPANT_DIM_FK]
GO
ALTER TABLE [BDW].[APP_METERING_FAC]  WITH CHECK ADD  CONSTRAINT [APP_MET_FAC_PUBLISHER_DIM_FK] FOREIGN KEY([PUBLISHER_IDN])
REFERENCES [BDW].[PUBLISHER_DIM] ([PUBLISHER_IDN])
GO
ALTER TABLE [BDW].[APP_METERING_FAC] CHECK CONSTRAINT [APP_MET_FAC_PUBLISHER_DIM_FK]
GO
ALTER TABLE [BDW].[APPLICATION_DIM]  WITH CHECK ADD  CONSTRAINT [APP_DIM_PUBLISHER_DIM_FK] FOREIGN KEY([PUBLISHER_IDN])
REFERENCES [BDW].[PUBLISHER_DIM] ([PUBLISHER_IDN])
GO
ALTER TABLE [BDW].[APPLICATION_DIM] CHECK CONSTRAINT [APP_DIM_PUBLISHER_DIM_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_APP_DIM_FK] FOREIGN KEY([APPLICATION_IDN])
REFERENCES [BDW].[APPLICATION_DIM] ([APPLICATION_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_APP_DIM_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_BUILD_DATE_DIM_FK] FOREIGN KEY([BUILD_DATE_IDN])
REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_BUILD_DATE_DIM_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_COMP_HRDWRE_DIM_FK] FOREIGN KEY([COMP_HARDWARE_IDN])
REFERENCES [BDW].[COMP_HARDWARE_DIM] ([COMP_HARDWARE_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_COMP_HRDWRE_DIM_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_INSTL_DATE_DIM_FK] FOREIGN KEY([INSTALL_DATE_IDN])
REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_INSTL_DATE_DIM_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_OFFICE_DIM_FK] FOREIGN KEY([OFFICE_IDN])
REFERENCES [BDW].[OFFICE_DIM] ([OFFICE_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_OFFICE_DIM_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_PARTICIPANT_USER_FK] FOREIGN KEY([USER_IDN])
REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_PARTICIPANT_USER_FK]
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC]  WITH CHECK ADD  CONSTRAINT [COMP_APP_FAC_PUR_DATE_DIM_FK] FOREIGN KEY([PURCHASE_DATE_IDN])
REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])
GO
ALTER TABLE [BDW].[COMPUTER_APPLICATION_FAC] CHECK CONSTRAINT [COMP_APP_FAC_PUR_DATE_DIM_FK]
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC]  WITH CHECK ADD  CONSTRAINT [MOBILE_DEV_APP_FAC_APPLICATION_DIM_FK] FOREIGN KEY([APPLICATION_IDN])
REFERENCES [BDW].[APPLICATION_DIM] ([APPLICATION_IDN])
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC] CHECK CONSTRAINT [MOBILE_DEV_APP_FAC_APPLICATION_DIM_FK]
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC]  WITH CHECK ADD  CONSTRAINT [MOBILE_DEV_APP_FAC_MOB_DEV_DIM_FK] FOREIGN KEY([MOBILE_DEVICE_IDN])
REFERENCES [BDW].[MOBILE_DEVICE_DIM] ([MOBILE_DEVICE_IDN])
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC] CHECK CONSTRAINT [MOBILE_DEV_APP_FAC_MOB_DEV_DIM_FK]
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC]  WITH CHECK ADD  CONSTRAINT [MOBILE_DEV_APP_FAC_MOB_DEV_JNK_DIM_FK] FOREIGN KEY([MOBILE_DEVICE_JNK_IDN])
REFERENCES [BDW].[MOBILE_DEVICE_JNK_DIM] ([MOBILE_DEVICE_JNK_IDN])
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC] CHECK CONSTRAINT [MOBILE_DEV_APP_FAC_MOB_DEV_JNK_DIM_FK]
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC]  WITH CHECK ADD  CONSTRAINT [MOBILE_DEV_APP_FAC_PARTICIPANT_DIM_FK] FOREIGN KEY([EMPLOYEE_IDN])
REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
GO
ALTER TABLE [BDW].[MOBILE_DEVICE_APP_FAC] CHECK CONSTRAINT [MOBILE_DEV_APP_FAC_PARTICIPANT_DIM_FK]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_DATE_DIM_FK1] FOREIGN KEY([START_DATE_IDN])
REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_DATE_DIM_FK1]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_DATE_DIM_FK2] FOREIGN KEY([END_DATE_IDN])
REFERENCES [BDW].[DATE_DIM] ([DATE_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_DATE_DIM_FK2]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK] FOREIGN KEY([OFFICE_IDN])
REFERENCES [BDW].[OFFICE_DIM] ([OFFICE_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_OFFICE_DIM_FK]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_CHAIRPERSON_FK] FOREIGN KEY([CHAIRPERSON_IDN])
REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_CHAIRPERSON_FK]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK] FOREIGN KEY([PARTICIPANT_IDN])
REFERENCES [BDW].[PARTICIPANT_DIM] ([PARTICIPANT_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_PARTICIPANT_DIM_FK]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK] FOREIGN KEY([SERVICE_USAGE_JNK_IDN])
REFERENCES [BDW].[SERVICE_USAGE_JNK_DIM] ([SERVICE_USAGE_JNK_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_USAGE_JNK_DIM_FK]
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC]  WITH CHECK ADD  CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_VENDOR_DIM_FK] FOREIGN KEY([SERVICE_VENDOR_IDN])
REFERENCES [BDW].[SERVICE_VENDOR_DIM] ([SERVICE_VENDOR_IDN])
GO
ALTER TABLE [BDW].[SERVICE_USAGE_FAC] CHECK CONSTRAINT [SERVICE_USAGE_FAC_SERVICE_VENDOR_DIM_FK]
GO
EXEC [IT_Metrics].sys.sp_addextendedproperty @name=N'Owner', @value=N'<enter email address for database point of contact>' 
GO
EXEC [IT_Metrics].sys.sp_addextendedproperty @name=N'Description', @value=N'<enter any sundry information about the database e.g. Applicaiton Name, App Server>' 
GO
EXEC [IT_Metrics].sys.sp_addextendedproperty @name=N'AppType', @value=N'For example pick from: BCG; Blackberry; Case Team; MS System; Payroll; SampleDB; SharePoint; Utility' 
GO
USE [master]
GO
ALTER DATABASE [IT_Metrics] SET  READ_WRITE 
GO
